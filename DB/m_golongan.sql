/*
SQLyog Enterprise v12.5.1 (64 bit)
MySQL - 10.5.5-MariaDB : Database - sirebeka
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sirebeka` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sirebeka`;

/*Table structure for table `m_golongan` */

DROP TABLE IF EXISTS `m_golongan`;

CREATE TABLE `m_golongan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT NULL,
  `golongan` varchar(50) DEFAULT NULL,
  `persen` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `m_golongan` */

insert  into `m_golongan`(`id`,`jenis`,`golongan`,`persen`,`created_at`,`updated_at`) values 
(1,'PNS','I',15,'2020-11-25 07:37:26','2020-11-25 07:37:26'),
(2,'PNS','II',20,'2020-11-25 07:43:46','2020-11-25 07:43:46'),
(3,'PNS','III',25,'2020-11-25 07:49:17','2020-11-25 07:51:54'),
(4,'PNS','IV',30,'2020-11-25 07:52:12','2020-11-25 07:52:12'),
(5,'NON PNS','NON GOLONGAN',15,'2020-11-25 07:53:03','2020-11-25 07:53:03');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
