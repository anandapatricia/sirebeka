-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sirebeka
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `f78_nkht`
--

DROP TABLE IF EXISTS `f78_nkht`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f78_nkht` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `r` varchar(100) DEFAULT NULL,
  `k` varchar(100) DEFAULT NULL,
  `b` varchar(100) DEFAULT NULL,
  `u` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f78_nkht`
--

LOCK TABLES `f78_nkht` WRITE;
/*!40000 ALTER TABLE `f78_nkht` DISABLE KEYS */;
/*!40000 ALTER TABLE `f78_nkht` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f5_pedoman_keputusan`
--

DROP TABLE IF EXISTS `f5_pedoman_keputusan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f5_pedoman_keputusan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f5_pedoman_keputusan`
--

LOCK TABLES `f5_pedoman_keputusan` WRITE;
/*!40000 ALTER TABLE `f5_pedoman_keputusan` DISABLE KEYS */;
INSERT INTO `f5_pedoman_keputusan` VALUES (1,'BAKU, SANGAN RUTIN','A','2','2020-11-18 02:36:30','2020-11-18 02:36:30'),(2,'RUTIN','B','3','2020-11-18 02:36:48','2020-11-18 02:36:48'),(3,'SEMI RUTIN','C','5','2020-11-18 02:37:08','2020-11-18 02:37:08'),(4,'KHUSUS DAN OPERASIONAL','D','10','2020-11-18 02:37:28','2020-11-18 02:37:28'),(5,'INTEGRASI KEBIJAKAN','E','19','2020-11-18 02:37:58','2020-11-18 02:37:58'),(6,'KEBIJAKAN LUAS','F','37','2020-11-18 02:38:13','2020-11-18 02:38:13');
/*!40000 ALTER TABLE `f5_pedoman_keputusan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f1_kompetensi_teknis`
--

DROP TABLE IF EXISTS `f1_kompetensi_teknis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f1_kompetensi_teknis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f1_kompetensi_teknis`
--

LOCK TABLES `f1_kompetensi_teknis` WRITE;
/*!40000 ALTER TABLE `f1_kompetensi_teknis` DISABLE KEYS */;
INSERT INTO `f1_kompetensi_teknis` VALUES (5,'PRIMER','A','100','2020-11-17 08:12:10','2020-11-17 08:12:10'),(8,'TEKNIS DASAR','B','115','2020-11-17 08:47:08','2020-11-17 08:47:08'),(9,'TEKNIS PRAKTIS','C','138','2020-11-17 09:28:10','2020-11-17 09:28:10'),(10,'ANALISIS DASAR','D','179','2020-11-17 09:31:06','2020-11-17 09:31:06'),(11,'ANALISIS TERINTEGRASI','E','260','2020-11-17 09:31:56','2020-11-17 09:31:56'),(12,'ANALISIS KONSEPTUAL','F','429','2020-11-17 09:32:25','2020-11-17 09:32:25'),(13,'PROFESIONAL','G','773','2020-11-17 09:32:44','2020-11-17 09:32:44'),(14,'AHLI','H','1468','2020-11-17 09:33:03','2020-11-17 09:33:03'),(15,'MASTER','I','2233','2020-11-17 09:33:18','2020-11-17 09:33:18');
/*!40000 ALTER TABLE `f1_kompetensi_teknis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f6_kondisi_kerja`
--

DROP TABLE IF EXISTS `f6_kondisi_kerja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f6_kondisi_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f6_kondisi_kerja`
--

LOCK TABLES `f6_kondisi_kerja` WRITE;
/*!40000 ALTER TABLE `f6_kondisi_kerja` DISABLE KEYS */;
INSERT INTO `f6_kondisi_kerja` VALUES (1,'LINGKUNGAN FISIK','A','A1','1','2','2020-11-18 02:59:35','2020-11-18 02:59:35'),(2,'LINGKUNGAN FISIK','A','A2','2','7','2020-11-18 03:00:00','2020-11-18 03:00:00'),(3,'LINGKUNGAN FISIK','A','A3','3','13','2020-11-18 03:00:20','2020-11-18 03:00:20'),(4,'LINGKUNGAN FISIK','A','A4','4','19','2020-11-18 03:00:41','2020-11-18 03:00:41'),(5,'KONTRIBUSI FISIK','B','B1','1','2','2020-11-18 03:01:39','2020-11-18 03:01:39'),(6,'KONTRIBUSI FISIK','B','B2','2','7','2020-11-18 03:01:59','2020-11-18 03:01:59'),(7,'KONTRIBUSI FISIK','B','B3','3','13','2020-11-18 03:03:09','2020-11-18 03:03:09'),(8,'KONTRIBUSI FISIK','B','B4','4','19','2020-11-18 03:03:32','2020-11-18 03:03:32'),(9,'KOORDINASI PANCA INDERA','C','C1','1','2','2020-11-18 03:04:12','2020-11-18 03:04:12'),(10,'KOORDINASI PANCA INDERA','C','C2','2','7','2020-11-18 03:04:33','2020-11-18 03:04:33'),(11,'KOORDINASI PANCA INDERA','C','C3','3','13','2020-11-18 03:04:52','2020-11-18 03:04:52'),(12,'KOORDINASI PANCA INDERA','C','C4','4','19','2020-11-18 03:05:11','2020-11-18 03:05:11'),(13,'KONSENTRASI / KETEGANGAN MENTAL','D','D1','1','4','2020-11-18 03:06:07','2020-11-18 03:06:07'),(14,'KONSENTRASI / KETEGANGAN MENTAL','D','D2','2','14','2020-11-18 03:06:35','2020-11-18 03:06:35'),(15,'KONSENTRASI / KETEGANGAN MENTAL','D','D3','3','27','2020-11-18 03:06:59','2020-11-18 03:06:59'),(16,'KONSENTRASI / KETEGANGAN MENTAL','D','D4','4','39','2020-11-18 03:07:26','2020-11-18 03:07:26'),(17,'LAMA KONSENTRASI KETEGANGAN MENTAL','E','E1','1','4','2020-11-18 03:08:31','2020-11-18 03:08:31'),(18,'LAMA KONSENTRASI KETEGANGAN MENTAL','E','E2','2','14','2020-11-18 03:08:50','2020-11-18 03:08:50'),(19,'LAMA KONSENTRASI KETEGANGAN MENTAL','E','E3','3','27','2020-11-18 03:09:33','2020-11-18 03:09:33'),(20,'LAMA KONSENTRASI KETEGANGAN MENTAL','E','E4','4','39','2020-11-18 03:10:01','2020-11-18 03:10:01'),(21,'POLA JADUAL KERJA','F','F1','1','4','2020-11-18 03:10:38','2020-11-18 03:10:38'),(22,'POLA JADUAL KERJA','F','F2','2','14','2020-11-18 03:11:13','2020-11-18 03:11:13'),(23,'POLA JADUAL KERJA','F','F3','3','27','2020-11-18 03:11:28','2020-11-18 03:11:28'),(24,'POLA JADUAL KERJA','F','F4','4','39','2020-11-18 03:11:45','2020-11-18 03:11:45'),(25,'KETERDESAKAN TINDAKAN LAYANAN','G','G1','1','4','2020-11-18 03:12:28','2020-11-18 03:12:28'),(26,'KETERDESAKAN TINDAKAN LAYANAN','G','G2','2','14','2020-11-18 03:12:46','2020-11-18 03:12:46'),(27,'KETERDESAKAN TINDAKAN LAYANAN','G','G3','3','27','2020-11-18 03:13:05','2020-11-18 03:13:05'),(28,'KETERDESAKAN TINDAKAN LAYANAN','G','G4','4','39','2020-11-18 03:13:23','2020-11-18 03:13:23'),(29,'KETERKAITAN LAYANAN PENDIDIKAN','H','H1','1','2','2020-11-18 03:14:15','2020-11-18 03:14:15'),(30,'KETERKAITAN LAYANAN PENDIDIKAN','H','H2','2','7','2020-11-18 03:14:58','2020-11-18 03:14:58'),(31,'KETERKAITAN LAYANAN PENDIDIKAN','H','H3','3','13','2020-11-18 03:15:13','2020-11-18 03:15:13'),(32,'KETERKAITAN LAYANAN PENDIDIKAN','H','H4','4','39','2020-11-18 03:15:37','2020-11-18 03:15:37'),(33,'KOMPLEKSITAS TINDAKAN PENDIDIKAN','I','I1','1','4','2020-11-18 03:18:12','2020-11-18 03:18:12'),(34,'KOMPLEKSITAS TINDAKAN PENDIDIKAN','I','I2','2','14','2020-11-18 07:29:34','2020-11-18 07:29:34'),(35,'KOMPLEKSITAS TINDAKAN PENDIDIKAN','I','I3','3','27','2020-11-18 07:59:30','2020-11-18 07:59:30'),(36,'KOMPLEKSITAS TINDAKAN PENDIDIKAN','I','I4','4','39','2020-11-18 08:00:05','2020-11-18 08:00:05'),(37,'BRAND IMAGE PENDIDIKAN','J','J1','1','5','2020-11-18 08:00:46','2020-11-18 08:00:46'),(38,'BRAND IMAGE PENDIDIKAN','J','J2','2','15','2020-11-18 08:01:15','2020-11-18 08:01:15'),(39,'BRAND IMAGE PENDIDIKAN','J','J3','3','31','2020-11-18 08:01:38','2020-11-18 08:01:38'),(40,'BRAND IMAGE PENDIDIKAN','J','J4','4','44','2020-11-18 08:01:53','2020-11-18 08:01:53'),(41,'INFEKSI','K','K1','1','4','2020-11-18 08:02:40','2020-11-18 08:02:40'),(42,'INFEKSI','K','K2','2','14','2020-11-18 08:03:03','2020-11-18 08:03:03'),(43,'INFEKSI','K','K3','3','27','2020-11-18 08:03:21','2020-11-18 08:03:21'),(44,'INFEKSI','K','K4','4','39','2020-11-18 08:03:46','2020-11-18 08:03:46'),(45,'RADIASI','L','L1','1','4','2020-11-18 08:04:08','2020-11-18 08:04:08'),(46,'RADIASI','L','L2','2','14','2020-11-18 08:04:32','2020-11-18 08:04:32'),(47,'RADIASI','L','L3','3','27','2020-11-18 08:05:01','2020-11-18 08:05:01'),(48,'RADIASI','L','L4','4','39','2020-11-18 08:05:21','2020-11-18 08:05:21');
/*!40000 ALTER TABLE `f6_kondisi_kerja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f10_probabilitas_resiko`
--

DROP TABLE IF EXISTS `f10_probabilitas_resiko`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f10_probabilitas_resiko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f10_probabilitas_resiko`
--

LOCK TABLES `f10_probabilitas_resiko` WRITE;
/*!40000 ALTER TABLE `f10_probabilitas_resiko` DISABLE KEYS */;
INSERT INTO `f10_probabilitas_resiko` VALUES (1,'MINIMAL','I','55','2020-11-18 09:32:50','2020-11-18 09:32:50'),(2,'SEDANG','II','65','2020-11-18 09:33:05','2020-11-18 09:33:05'),(3,'MENENGAH','III','75','2020-11-18 09:33:30','2020-11-18 09:33:30'),(4,'TINGGI','IV','85','2020-11-18 09:33:43','2020-11-18 09:33:43');
/*!40000 ALTER TABLE `f10_probabilitas_resiko` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f78_nkhs`
--

DROP TABLE IF EXISTS `f78_nkhs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f78_nkhs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `r` varchar(100) DEFAULT NULL,
  `k` varchar(100) DEFAULT NULL,
  `b` varchar(100) DEFAULT NULL,
  `u` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f78_nkhs`
--

LOCK TABLES `f78_nkhs` WRITE;
/*!40000 ALTER TABLE `f78_nkhs` DISABLE KEYS */;
/*!40000 ALTER TABLE `f78_nkhs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f2_manajerial`
--

DROP TABLE IF EXISTS `f2_manajerial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f2_manajerial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `lingkup` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f2_manajerial`
--

LOCK TABLES `f2_manajerial` WRITE;
/*!40000 ALTER TABLE `f2_manajerial` DISABLE KEYS */;
INSERT INTO `f2_manajerial` VALUES (1,'DIRI SENDIRI','I','A','HUBUNGAN PENGGUNA LAYANAN','10','2020-11-17 10:42:02','2020-11-17 10:42:02'),(2,'DIRI SENDIRI','I','B','HUBUNGAN FUNGSI LAIN','11','2020-11-17 10:45:43','2020-11-17 10:45:43'),(3,'DIRI SENDIRI','I','C','HUBUNGAN MATRIX PROYEK LAYANAN PENDIDIKAN','19','2020-11-17 10:52:40','2020-11-17 10:52:40'),(4,'KOORDINATOR','II','A','KOORDINATOR TIM KERJA SHIFT','14','2020-11-17 10:57:17','2020-11-17 10:57:17'),(5,'KOORDINATOR','II','B','KOORDINATOR TIM KERJA TETAP','16','2020-11-17 15:49:49','2020-11-17 15:49:49'),(6,'SUPERVISI','III','A','MATRIKS OPERASIONAL','23','2020-11-17 15:51:21','2020-11-17 15:51:21'),(7,'SUPERVISI','III','B','MATRIKS PROYEK LAYANAN PENDIDIKAN','27','2020-11-17 15:52:48','2020-11-17 15:52:48'),(8,'SUPERVISI','III','C','LINI TEKNIS OPERASIONAL','41','2020-11-17 15:53:35','2020-11-17 15:53:35'),(9,'MANAJERIAL','IV','A','STRATEGI OPERASIONAL','64','2020-11-17 15:54:09','2020-11-17 15:54:09'),(10,'MANAJERIAL','IV','B','STRATEGI USAHA','74','2020-11-17 15:54:57','2020-11-17 15:54:57');
/*!40000 ALTER TABLE `f2_manajerial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f3_komunikasi`
--

DROP TABLE IF EXISTS `f3_komunikasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f3_komunikasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f3_komunikasi`
--

LOCK TABLES `f3_komunikasi` WRITE;
/*!40000 ALTER TABLE `f3_komunikasi` DISABLE KEYS */;
INSERT INTO `f3_komunikasi` VALUES (1,'UMUM','1','15','2020-11-17 17:08:05','2020-11-17 17:08:05'),(2,'PENTING','2','23','2020-11-17 17:10:28','2020-11-17 17:10:28'),(3,'SIGNIFIKAN','3','34','2020-11-17 17:10:46','2020-11-17 17:10:46'),(4,'KRITIS','4','69','2020-11-17 17:11:01','2020-11-17 17:11:01');
/*!40000 ALTER TABLE `f3_komunikasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f78_nkhk`
--

DROP TABLE IF EXISTS `f78_nkhk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f78_nkhk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `r` varchar(100) DEFAULT NULL,
  `k` varchar(100) DEFAULT NULL,
  `b` varchar(100) DEFAULT NULL,
  `u` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f78_nkhk`
--

LOCK TABLES `f78_nkhk` WRITE;
/*!40000 ALTER TABLE `f78_nkhk` DISABLE KEYS */;
/*!40000 ALTER TABLE `f78_nkhk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f9_determinate`
--

DROP TABLE IF EXISTS `f9_determinate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f9_determinate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f9_determinate`
--

LOCK TABLES `f9_determinate` WRITE;
/*!40000 ALTER TABLE `f9_determinate` DISABLE KEYS */;
INSERT INTO `f9_determinate` VALUES (1,'REMOTE','R','1.00','2020-11-18 08:46:31','2020-11-18 08:46:31'),(2,'KONTROL','K','1.00','2020-11-18 08:47:13','2020-11-18 08:47:13'),(3,'BERPERAN','B','1.00','2020-11-18 08:47:27','2020-11-18 08:47:27'),(4,'UTAMA','U','1.00','2020-11-18 08:49:51','2020-11-18 08:49:51');
/*!40000 ALTER TABLE `f9_determinate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f9_indeterminate`
--

DROP TABLE IF EXISTS `f9_indeterminate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f9_indeterminate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f9_indeterminate`
--

LOCK TABLES `f9_indeterminate` WRITE;
/*!40000 ALTER TABLE `f9_indeterminate` DISABLE KEYS */;
INSERT INTO `f9_indeterminate` VALUES (1,'REMOTE','R','1.00','2020-11-18 08:28:18','2020-11-18 08:28:18'),(2,'KONTROL','K','1.25','2020-11-18 08:28:48','2020-11-18 08:28:48'),(3,'BERPERAN','B','1.88','2020-11-18 08:29:16','2020-11-18 08:29:16'),(4,'UTAMA','U','2.81','2020-11-18 08:29:33','2020-11-18 08:29:33');
/*!40000 ALTER TABLE `f9_indeterminate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f78_nkhm`
--

DROP TABLE IF EXISTS `f78_nkhm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f78_nkhm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `r` varchar(100) DEFAULT NULL,
  `k` varchar(100) DEFAULT NULL,
  `b` varchar(100) DEFAULT NULL,
  `u` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f78_nkhm`
--

LOCK TABLES `f78_nkhm` WRITE;
/*!40000 ALTER TABLE `f78_nkhm` DISABLE KEYS */;
/*!40000 ALTER TABLE `f78_nkhm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f78_nkh`
--

DROP TABLE IF EXISTS `f78_nkh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f78_nkh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `umum` int(11) DEFAULT NULL,
  `standar` int(11) DEFAULT NULL,
  `menengah` int(11) DEFAULT NULL,
  `menengah tinggi` int(11) DEFAULT NULL,
  `tinggi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f78_nkh`
--

LOCK TABLES `f78_nkh` WRITE;
/*!40000 ALTER TABLE `f78_nkh` DISABLE KEYS */;
/*!40000 ALTER TABLE `f78_nkh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `f4_analisis_lingkungan_pekerjaan`
--

DROP TABLE IF EXISTS `f4_analisis_lingkungan_pekerjaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `f4_analisis_lingkungan_pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `f4_analisis_lingkungan_pekerjaan`
--

LOCK TABLES `f4_analisis_lingkungan_pekerjaan` WRITE;
/*!40000 ALTER TABLE `f4_analisis_lingkungan_pekerjaan` DISABLE KEYS */;
INSERT INTO `f4_analisis_lingkungan_pekerjaan` VALUES (1,'REPETISI','1','3','2020-11-18 02:30:16','2020-11-18 02:30:16'),(2,'BERPOLA','2','5','2020-11-18 02:30:50','2020-11-18 02:30:50'),(3,'SEMI VARIABLE','3','10','2020-11-18 02:32:00','2020-11-18 02:32:00'),(4,'VARIABLE','4','19','2020-11-18 02:33:24','2020-11-18 02:33:24'),(5,'ADAPTIF','5','34','2020-11-18 02:33:48','2020-11-18 02:33:48'),(6,'INOVATIF','6','57','2020-11-18 02:34:08','2020-11-18 02:34:08');
/*!40000 ALTER TABLE `f4_analisis_lingkungan_pekerjaan` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-18 22:42:26
