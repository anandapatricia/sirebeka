/*
SQLyog Enterprise v12.5.1 (64 bit)
MySQL - 10.5.5-MariaDB : Database - sirebeka
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sirebeka` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sirebeka`;

/*Table structure for table `datapegawai` */

DROP TABLE IF EXISTS `datapegawai`;

CREATE TABLE `datapegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(100) NOT NULL,
  `ink` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  `karpeg` varchar(20) DEFAULT NULL,
  `unit` varchar(250) DEFAULT NULL,
  `kode_unit` varchar(100) DEFAULT NULL,
  `kode_unit_lama` varchar(50) DEFAULT NULL,
  `kantor` varchar(250) DEFAULT NULL,
  `kode_kantor` varchar(100) DEFAULT NULL,
  `kode_kantor_lama` varchar(50) DEFAULT NULL,
  `subkantor` varchar(250) DEFAULT NULL,
  `kode_subkantor` varchar(100) DEFAULT NULL,
  `tmt_golongan` date DEFAULT NULL,
  `golongan` varchar(50) DEFAULT NULL,
  `pangkat` varchar(50) DEFAULT NULL,
  `kode_eselon` varchar(50) DEFAULT NULL,
  `eselon` varchar(100) DEFAULT NULL,
  `jabatan_struktular` varchar(100) DEFAULT NULL,
  `tmt_jabatan_struktural` date DEFAULT NULL,
  `jabatan_fungsional` varchar(100) DEFAULT NULL,
  `tmt_jabatan_fungsional` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telepon` varchar(20) DEFAULT NULL,
  `tmt_cpns` date DEFAULT NULL,
  `tmt_pns` date DEFAULT NULL,
  `pendidikan_terakhir` varchar(200) DEFAULT NULL,
  `jabatan_id` int(11) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`nip`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;

/*Data for the table `datapegawai` */

insert  into `datapegawai`(`id`,`nip`,`ink`,`tanggal_lahir`,`tempat_lahir`,`nama`,`jenis_kelamin`,`karpeg`,`unit`,`kode_unit`,`kode_unit_lama`,`kantor`,`kode_kantor`,`kode_kantor_lama`,`subkantor`,`kode_subkantor`,`tmt_golongan`,`golongan`,`pangkat`,`kode_eselon`,`eselon`,`jabatan_struktular`,`tmt_jabatan_struktural`,`jabatan_fungsional`,`tmt_jabatan_fungsional`,`email`,`telepon`,`tmt_cpns`,`tmt_pns`,`pendidikan_terakhir`,`jabatan_id`,`foto`,`created_at`,`updated_at`) values 
(1,'195706121982031002',NULL,'1957-06-12','Kota Agung, Lampung','Drs. RIDWAN SETIAWAN, M.Si., M.Mar.E.','L','C 054687','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2014-04-01','IV/d','Pembina Utama Madya',NULL,NULL,NULL,NULL,'LEKTOR KEPALA.','2019-05-20','ridwan.setiawan@dephub.go.id','','1982-03-01','2014-04-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812021742.png','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(2,'195805191993031001',NULL,'1958-05-19','GARUT','Capt. RUDIANA, M.M.','L','F354594','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2017-10-01','IV/d','Pembina Utama Madya',NULL,NULL,NULL,NULL,'LEKTOR KEPALA','2018-05-02','rudiana@dephub.go.id','','1993-03-01','2013-10-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195805191993031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(3,'195812291993031001',NULL,'1958-12-29','MEDAN','Dr., Ir. DESAMEN SIMATUPANG, M.M.','L','F.365394','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2012-10-01','IV/c','Pembina Utama Muda',NULL,NULL,NULL,NULL,'LEKTOR KEPALA','2001-10-22','desamen_simatupang@dephub.go.id','','1993-03-01','1994-03-01','S-3/Doktor',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195812291993031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(4,'195608121982031001',NULL,'1956-08-12','Magelang','WAHYU WIDAYAT, DRS., M.M.','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2013-04-01','IV/c','Pembina Utama Muda',NULL,NULL,NULL,NULL,'Lektor','2016-01-05','wahyu_widayat1208@dephub.go.id','','1982-03-01','2013-04-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195608121982031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(5,'195810081998081001',NULL,'1958-10-08','TANJUNG PINANG','MUHAMAD HASAN HABLI, M.M.','L','H 052866','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2016-04-01','IV/c','Pembina Utama Muda',NULL,NULL,NULL,NULL,'LEKTOR KEPALA','2012-01-30','muhamad_hasan@dephub.go.id','','1998-08-01','1999-08-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195810081998081001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(6,'196008181992031001',NULL,'1960-08-18','TAPANULI UTARA','Capt. JONGGUNG SITORUS, M.M.','L','F.233090','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2016-04-01','IV/c','Pembina Utama Muda',NULL,NULL,NULL,NULL,'ASISTEN AHLI','2018-05-02','jonggung_sitorus@dephub.go.id','','1992-03-01','2009-10-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196008181992031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(7,'196205221997031001',NULL,'1962-05-22','PEMBILAHAN,  RIAU','PANDE IRIANTO SUBANDRIO SIREGAR, M.M.','L','H 031586','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2016-04-01','IV/c','Pembina Utama Muda',NULL,NULL,NULL,NULL,'LEKTOR KEPALA','2012-01-30','pande_irianto@dephub.go.id','6263860','1997-03-01','1998-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196205221997031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(8,'195903161985031001',NULL,'1959-03-16','SURABAYA','EKA BUDI TJAHJONO, DR., S.H., M.H.','L','D160274','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','1999-10-01','IV/b','Pembina Tingkat I',NULL,NULL,NULL,NULL,'Lektor Kepala','2015-03-01','eka_budi@dephub.go.id','218803002','1985-03-01','1989-10-01','S-3/Doktor',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195903161985031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(9,'195602081990031003',NULL,'1956-02-08','Makassar','THEO JOHANNES FRANS KALANGI, IR., M.S.TR.','L','E.862043','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2008-04-01','IV/b','Pembina Tingkat I',NULL,NULL,NULL,NULL,'LEKTOR','2013-02-07','theo_johannes@dephub.go.id','','1990-03-01','1991-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195602081990031003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(10,'195809181991031002',NULL,'1958-09-18','PADANG','Ir. SUKMANOFITH DJULIS, M.M.','L','F172399','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2009-10-01','IV/b','Pembina Tingkat I',NULL,NULL,NULL,NULL,'ASISTEN AHLI','2016-09-27','sukmanofith_djulis@dephub.go.id','3100710','1991-03-01','2005-04-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195809181991031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(11,'195704071979031001',NULL,'1957-04-07','CIAMIS','Drs. WARSONO, M.M.','L','C0002238','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2012-10-01','IV/b','Pembina Tingkat I',NULL,NULL,NULL,NULL,'Lektor','2013-04-19','warsono@dephub.go.id','','1979-03-01','1980-07-03','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195704071979031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(12,'195905061988031002',NULL,'1959-05-06','Pemantang Siantar','Ir. ROBINSON, MM','L','A.67710','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2012-10-01','IV/b','Pembina Tingkat I',NULL,NULL,NULL,NULL,'ASISTEN AHLI','2017-08-01','robinson@dephub.go.id','5491908','1988-03-01','2012-10-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195905061988031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(13,'196011051985031001',NULL,'1960-11-05','AMBON','Dr. Drs BAMBANG SUMALI, M. Sc','L','D.345782','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2013-04-01','IV/b','Pembina Tingkat I',NULL,NULL,NULL,NULL,'Lektor Kepala','2009-07-01','bambang_sumali@dephub.go.id','','1985-03-01','1986-03-01','S-3/Doktor',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196011051985031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(14,'197811092005022001',NULL,'1978-11-09','MEDAN','dr. JOYCE NOVELYN SIAGIAN','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-10-01','IV/b','Pembina Tingkat I',NULL,NULL,NULL,NULL,'DOKTER MADYA','2019-03-01','joyce_novelyn@dephub.go.id','','2005-02-01','2013-10-01','Spesialis',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197811092005022001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(15,'195808281985032001',NULL,'1958-08-28','TEMANGGUNG','Dra. PUJI REKNATI, P.Si., M.Pd.','P','E372050','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2001-04-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'Lektor Kepala','2001-10-22','puji_reknati@dephub.go.id','','1985-03-01','1987-02-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195808281985032001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(16,'196008141982021001',NULL,'1960-08-14','JAKARTA','A CHALID PASYAH, DIP.TESL., M.Pd.','L','527394','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2003-10-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'Lektor Kepala','2010-01-08','a_chalid@dephub.go.id','','1982-02-01','1983-05-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196008141982021001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(17,'195703201982021001',NULL,'1957-03-20','PAHAE JULU','Drs. TIGOR SIAGIAN, M.M.','L','C0527401','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2005-10-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'Lektor','2010-01-08','tigor_siagian@dephub.go.id','','1982-02-01','1983-02-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195703201982021001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(18,'195511281977101001',NULL,'1955-11-28','PURWOREDJO','SUSILO, S.E., M.T.','L','C0217499','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2006-04-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'Lektor','2010-04-01','susilo2811@dephub.go.id','','1977-10-01','1979-01-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195511281977101001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(19,'195906121980031002',NULL,'1959-06-12','PLAJU','Drs. PURNOMO,, M.M.','L','C 016553','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2009-04-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'Lektor','2014-07-03','purnomo@dephub.go.id','218448613','1980-03-01','1981-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195906121980031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(20,'195908141983021001',NULL,'1959-08-14','LOSARI','M. NURDIN, S.E., M.M.','L','C.098703','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2009-04-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'Lektor','2014-07-03','m_nurdin@dephub.go.id','218878122','1983-02-01','1984-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195908141983021001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(21,'195909271980031002',NULL,'1959-09-27','KEBUMEN','BAGASKORO, S.Kom., M.M.','L','C 016553','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2009-10-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'Lektor','2017-10-05','bagaskoro@dephub.go.id','','1980-03-01','1981-04-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195909271980031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(22,'195702251979031001',NULL,'1957-02-25','BUKITTINGGI','ZULNASRI, S.H., M.H., MM','L','B. 92832','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2011-04-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'LEKTOR KEPALA','2011-02-01','zulnasri@dephub.go.id','','1979-03-01','1980-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195702251979031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(23,'195912121984031007',NULL,'1959-12-12','BIMA','M. YUSUF, S.E., M.M.','L','D. 12544','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2013-04-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'LEKTOR KEPALA','2012-11-22','m_yusuf@dephub.go.id','','1984-03-01','1985-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195912121984031007.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(24,'197205031998032003',NULL,'1972-05-03','JAKARTA','ROSNA YUHERLINA SIAHAAN, S.Kom., M.M.Tr.','P','H.008110','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2014-04-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'PENYUSUN PROGRAM PROSES BELAJAR MENGAJAR','2019-01-01','rosna_yuherlina@dephub.go.id','','1998-03-01','1999-05-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197205031998032003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(25,'197607071998081001',NULL,'1976-07-07','LAMONGAN','Capt. SUPENDI, M.MTr.','L','J.059487','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Bagian Administrasi Akademik Dan Ketarunaan','008017001000000','2017-10-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'Atase Perhubungan pada KBRI Malaysia','2020-08-19','supendi@dephub.go.id','','1998-08-01','2002-10-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197607071998081001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(26,'196607261998081001',NULL,'1966-07-26','SURABAYA','WINARTO EDI PURNAMA, M.M.','L','J 056914','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2018-04-01','IV/a','Pembina',NULL,NULL,NULL,NULL,'PENYUSUN RENCANA PEMELIHARAAN DAN PERBAIKAN','2019-02-01','winarto_edi@dephub.go.id','8620995','1998-08-01','2000-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196607261998081001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(27,'196207151984111001',NULL,'1962-07-15','YOGYAKARTA','Drs. SUGIYANTO, M.M.','L','D.354648','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2007-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Lektor','2005-10-28','sugiyanto1507@dephub.go.id','734446688','1984-11-01','1986-02-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196207151984111001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(28,'197002071998031002',NULL,'1970-02-07','AWOTARAE, KAB. WAJO','ASMAN ALA, S.T., M.T.','L','H031850','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2007-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Lektor','2005-10-28','asman_ala@dephub.go.id','4201357','1998-03-01','1999-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197002071998031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(29,'195810101982031004',NULL,'1958-10-10','BUKITTINGGI','EFFENDI, A.Md., S.T.','L','C.052733','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2009-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Lektor','2012-11-01','effendi@dephub.go.id','','1982-03-01','1983-05-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195810101982031004.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(30,'196603101999031002',NULL,'1966-03-10','KLATEN','HARTAYA, M.M.','L','J092991','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2011-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Lektor','2012-11-01','hartaya@dephub.go.id','','1999-03-01','2000-04-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196603101999031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(31,'195712011992031001',NULL,'1957-12-01','MALUA','FAUSIL','L','F229866','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2013-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Lektor','2013-07-01','','','1992-03-01','1993-07-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/195712011992031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(32,'197301151998031001',NULL,'1973-01-15','SIMARMATA','PANDERAJA SORITUA SIJABAT, S.Kom., M.M.Tr.','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2013-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Lektor','2010-01-08','panderaja_soritua@dephub.go.id','','1998-03-01','1999-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197301151998031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(33,'198105032002122001',NULL,'1981-05-03','JAKARTA','MEILINASARI NURHASANAH HUTAGAOL, S.Si.T., M.M.Tr.','P','L 082995','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2013-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Lektor','2019-12-23','meilinasari_nurhasanah@dephub.go.id','','2002-12-01','2003-12-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198105032002122001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(34,'197304021998081001',NULL,'1973-04-02','MAGETAN','AGUS WIDODO, M. M.','L','J.059486','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2014-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Lektor','2019-05-20','agus_widodo@dephub.go.id','','1998-08-01','2000-08-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197304021998081001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(35,'196511181988031002',NULL,'1965-11-18','BOYOLALI','RISWANTO, S.H.','L','E.620801','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Ketarunaan dan Alumni','008017001004000','2014-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'PENYUSUN PROGRAM DAN LAPORAN KEGIATAN KETARUNAAN DAN ALUMNI','2016-02-01','riswanto@dephub.go.id','','1988-03-01','2014-10-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196511181988031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(36,'198401262009122002',NULL,'1984-01-26','Sukoharjo','dr. ARIYANTI SRI SULISTYORINI','P','P206920','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Dokter Muda','2013-10-01','ariyanti_sri@dephub.go.id','','2009-12-01','2010-12-01','Spesialis',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198401262009122002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(37,'196302181983031001',NULL,'1963-02-18','BANYUMAS','SALIMUN, S.SOS.','L','C0987040','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'FASILITATOR TENAGA PENGAJAR PENDIDIKAN','2019-01-01','salimun@dephub.go.id','','1983-03-01','1984-03-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196302181983031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(38,'196501051990031001',NULL,'1965-01-05','TANGERANG','ENUNG SASTRAWIJAYA, S.Pd.','L','E 809343','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Arsiparis Penyelia','2012-05-01','enung_sastrawijaya@dephub.go.id','085217060221','1990-03-01','1991-07-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196501051990031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(39,'196703071991031001',NULL,'1967-03-07','KEMAYORAN','SUGIONO, S.Pd.','L','164353','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Arsiparis Penyelia','2012-05-01','sugiono@dephub.go.id','','1991-03-01','2015-10-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196703071991031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(40,'197301221998031001',NULL,'1973-01-22','JAKARTA','FRANS NEWTONY PADJAITAN, S.Pel.','L','J.019964','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Penyusun Program Proses Belajar Mengajar Jurusan Teknika','2020-05-01','frans_newtony@dephub.go.id','214220249','1998-03-01','1999-04-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197301221998031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(41,'197706152005022001',NULL,'1977-06-15','SLEMAN','ENY KURNIAWATI, S.Kom','P','M 036704','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Pengevaluasi Program Diklat','2020-08-01','eny_kurniawati@dephub.go.id','','2005-02-01','2006-02-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812062367.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(42,'197905172006042015',NULL,'1979-05-17','JAKARTA','DIAH ZAKIAH, S.T., M.T.','P','N. 13180','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2017-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'ASISTEN AHLI','2017-10-01','','','2006-04-01','2008-01-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197905172006042015.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(43,'197911162005021001',NULL,'1979-11-16','WAYHANDAK','MUDAKIR, S.Si.T.,M.M.','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Asisten Ahli','2019-12-23','mudakir@dephub.go.id','','2005-02-01','2006-02-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197911162005021001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(44,'198203062005021001',NULL,'1982-03-06','GROBOGAN','TITIS ARI WIBOWO, S.Si.T., M. M. Tr','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2017-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Lektor','2016-01-06','titis_ari@dephub.go.id','','2005-02-01','2006-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198203062005021001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(45,'198207172005021001',NULL,'1982-07-17','MAGETAN','JAROT DELTA SUSANTO, S.Si.T., M.M.','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'PETUGAS LEGALISASI SERTIFIKAT KEPELAUTAN','2015-03-02','jarot_delta@dephub.go.id','','2005-02-01','2006-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198207172005021001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(46,'198208012005021003',NULL,'1982-08-01','CENGKARENG','JAYA ALAMSYAH, S.Si.T., M. M. Tr.','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua III','008017005000000','2017-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Penyusun Bahan Rencana Kerja','2019-02-01','jaya_alamsyah@dephub.go.id','','2005-02-01','2006-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198208012005021003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(47,'198208262005022001',NULL,'1982-08-26','LAMONGAN','EVITA RATNA WATI, S.Si.T., M.T.','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Program dan Pelaporan','008017002002000','2017-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'PENYUSUN BAHAN EVALUASI DAN PELAPORAN','2019-01-01','evita_ratna@dephub.go.id','','2005-02-01','2006-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198208262005022001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(48,'198211242005021001',NULL,'1982-11-24','KULON PROGO','HARIS ALFENDI, S.Si.T.','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'PETUGAS LEGALISASI SERTIFIKAT KEPELAUTAN','2015-03-02','haris_alfendi@dephub.go.id','','2005-02-01','2006-02-01','Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198211242005021001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(49,'198508222010122001',NULL,'1985-08-22','Palembang','dr. RIRI ANITA AGUSTIE D','P','Q030117','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Dokter Muda','2014-09-19','riri_anita@dephub.go.id','','2010-12-01','2011-12-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198508222010122001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(50,'197006211989031001',NULL,'1970-06-21','JAKARTA','UNTUNG, S.PD.','L','E.620815','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2018-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Perwira Aktifitas','2019-01-01','untung2106@dephub.go.id','','1989-03-01','1990-04-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197006211989031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(51,'197705102006041001',NULL,'1977-05-10','KARAWANG','SUGENG SUPRIYONO, S.Psi.','L','M.259061','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua III','008017005000000','2018-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Petugas Bimbingan dan Konseling','2018-04-01','sugeng_supriyono@dephub.go.id','','2006-04-01','2007-04-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197705102006041001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(52,'196603161992031002',NULL,'1966-03-16','PURBALINGGA','IMIN SUTARTO, S.Sos.','L','F333682','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Administrasi Akademik','008017001001000','2018-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'PENGEVALUASI PROGRAM DIKLAT','2018-01-01','imin_sutarto@dephub.go.id','','1992-03-01','1993-04-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196603161992031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(53,'196808041990081001',NULL,'1968-08-04','JAKARTA','SUHANDA, S.Pd.','L','F164358','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2018-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Fasilitator Tenaga Pengajar Pendidikan','2020-09-01','suhanda0408@dephub.go.id','-','1990-08-01','1992-03-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196808041990081001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(54,'197604081998031003',NULL,'1976-04-08','JAKARTA','AGUS SURAWAN, S.Sos.','L','H.031587','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2018-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Penyusun Kebutuhan Tenaga Kependidikan','2020-08-01','agus_surawan@dephub.go.id','','1998-03-01','1999-04-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197604081998031003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(55,'196807041989032001',NULL,'1968-07-04','JAKARTA','MARPUAH','P','E.745081','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2019-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'Penata Laporan Keuangan','2017-01-02','marpuah@dephub.go.id','','1989-03-01','1990-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196807041989032001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(56,'197711222009122004',NULL,'1977-11-22','Jakarta','NAOMI LOUHENAPESSY','P','P206936','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2019-10-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'LEKTOR DENGAN TUGAS TAMBAHAN KEPALA UNIT AKREDITASI DAN SERTIFIKASI MUTU','2019-02-01','naomi_louhenapessy@dephub.go.id','','2009-12-01','2010-12-01','Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197711222009122004.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(57,'196806171992031001',NULL,'1968-06-17','CIREBON','NANO SUNARYO, S.Sos.','L','F.333684','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Rumah Tangga dan Hubungan Masyarakat','008017002003000','2020-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'PENGELOLA URUSAN KERUMAHTANGGAAN','2015-03-02','nano_sunaryo@dephub.go.id','','1992-03-01','1993-04-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196806171992031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(58,'197207051993031002',NULL,'1972-07-05','CIRACAS','ROMANTA, S.E.','L','G025820','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2020-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'PENGELOLA FASILITAS ASRAMA','2016-06-13','romanta@dephub.go.id','','1993-03-01','1994-03-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197207051993031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(59,'197606222003121002',NULL,'1976-06-22','LAHAT','R. M. YUSUF, S.T.','L','M.017525','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Administrasi Akademik','008017001001000','2020-04-01','III/d','Penata Tingkat I',NULL,NULL,NULL,NULL,'PENYUSUN KERJASAMA PENDIDIKAN','2018-08-18','r_m@dephub.go.id','','2003-12-01',NULL,'S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197606222003121002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(60,'196505261989021001',NULL,'1965-05-26','JAKARTA','HERY HIRAWAN','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2011-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Petugas Registrasi Diklat','2020-09-01','hery_hirawan@dephub.go.id','','1989-02-01','1990-06-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196505261989021001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(61,'197010101997031003',NULL,'1970-10-10','JAKARTA','BAMBANG KURNIADI, A.Md.','L','E. 41403','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2015-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Perwira Aktifitas','2018-01-01','bambang_kurniadi@dephub.go.id','','1997-03-01','2011-04-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197010101997031003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(62,'196810221990081001',NULL,'1968-10-22','JAKARTA','ASEP HADI ROSTYADI','L','F.164362','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'ARSIPARIS PELAKSANA LANJUTAN','2013-01-18','asep_hadi@dephub.go.id','','1990-08-01','1992-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196810221990081001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(63,'197005021993031001',NULL,'1970-05-02','SEMARANG','SUGITO, S.Sos., M. Si','L','G025813','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Pengelola Administrasi Penelitian','2018-01-01','sugito@dephub.go.id','','1993-03-01','1994-03-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197005021993031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(64,'197402201997031002',NULL,'1974-02-20','JAKARTA','EKO SUJARWO, S.E.','L','G433143','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-10-01','III/c','Penata',NULL,NULL,NULL,NULL,'PENGELOLA URUSAN KERUMAHTANGGAAN','2018-01-01','eko_sujarwo@dephub.go.id','9157425','1997-03-01','1998-05-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197402201997031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(65,'197406171997031001',NULL,'1974-06-17','JAKARTA','ZAENUDIN, S.Sos.','L','G.433142','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-10-01','III/c','Penata',NULL,NULL,NULL,NULL,'Penyusun Program Proses Belajar Mengajar Jurusan Nautika','2019-01-01','zaenudin@dephub.go.id','','1997-03-01','1998-05-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197406171997031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(66,'197606061998032001',NULL,'1976-06-06','DKI JAKARTA','SUSIWI RAHAYU, S.E.','P','J.056915','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Penyusun Rencana dan Anggaran','2020-08-01','susiwi_rahayu@dephub.go.id','08128725052','1998-03-01','1999-04-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197606061998032001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(67,'197906222008122003',NULL,'1979-06-22','SEMARANG','IRMA ROOSTANTI, S.Psi.','P','P 084180','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'PENGELOLA KEPEGAWAIAN','2015-03-02','irma_roostanti@dephub.go.id','','2008-12-01','2009-12-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197906222008122003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(68,'198011242008121001',NULL,'1980-11-24','Semarang','TRI BUDI PRASETYA, S.Si.T., M. M.','L','P 084186','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Unit Bimbingan Taruna','008017013000000','2017-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'PERWIRA AKTIFITAS','2016-03-21','tri_budi2411@dephub.go.id','','2008-12-01','2009-12-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198011242008121001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(69,'198305142008122001',NULL,'1983-05-14','JAKARTA','WIDIANTI LESTARI, S.Psi., M.Pd','P','P 084182','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2017-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'ASISTEN AHLI','2019-03-21','widianti_lestari@dephub.go.id','','2008-12-01','2009-12-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198305142008122001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(70,'197901031998031001',NULL,'1979-01-03','TANGERANG','MUDINI RIYANTO, S.E., M.Ak.','L','H.031590','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-10-01','III/c','Penata',NULL,NULL,NULL,NULL,'PENGUMPUL DAN PENGOLAH DATA LAPORAN','2015-05-26','mudini_riyanto@dephub.go.id','','1998-03-01','1999-04-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812064159.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(71,'198008282009121004',NULL,'1980-08-28','','SUHARSO JATI, S.T.','L','P206938','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2018-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Teknisi','2016-02-01','suharso_jati@dephub.go.id','085727280011','2009-12-01','2010-12-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812071327.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(72,'198111192009122001',NULL,'1981-11-19','Jakarta','ELISABETH NOVIANDARI, S.E.','P','P206931','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2018-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'VERIFIKATOR KEUANGAN','2019-01-01','elisabeth_noviandari@dephub.go.id','','2009-12-01','2010-12-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198111192009122001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(73,'198608142009122006',NULL,'1986-08-14','Magelang','DYAH RATNA PANDU PERTIWI, S.Si.','P','P206928','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2018-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'BENDAHARA','2019-01-01','dyah_ratna@dephub.go.id','','2009-12-01','2010-12-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198608142009122006.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(74,'197906291998031001',NULL,'1979-06-29','SUMEDANG','DUDI HERNAWAN, S.Sos.','L','H031588','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Rumah Tangga dan Hubungan Masyarakat','008017002003000','2018-10-01','III/c','Penata',NULL,NULL,NULL,NULL,'Pengelola Urusan Kerumahtanggaan','2019-01-02','dudi_hernawan@dephub.go.id','','1998-03-01','1999-04-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197906291998031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(75,'197111142010121001',NULL,'1971-11-14','Madiun','Capt. INDRA MUDA','L','Q030120','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2019-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Penata Usaha','2017-01-02','indra_muda@dephub.go.id','','2010-12-01','2012-01-01','Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197111142010121001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(76,'197711212002122001',NULL,'1977-11-21','INDRAMAYU','HENI MABRUROH','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2019-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'PETUGAS LEGALISASI SERTIFIKAT KEPELAUTAN','2016-02-05','heni_mabruroh@dephub.go.id','','2002-12-01','2003-12-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197711212002122001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(77,'197807072009121005',NULL,'1978-07-07','Jakarta','MOHAMAD RIDWAN, S.Si.T., M. M.','L','P206935','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2019-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Lektor','2019-10-01','mohamad_ridwan0707@dephub.go.id','','2009-12-01',NULL,'S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197807072009121005.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(78,'198007022002121003',NULL,'1980-07-02','KEDIRI','DIDIK SULISTYO KURNIAWAN, S.T., M. Si','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2019-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Asisten Ahli','2019-12-23','didik_sulistyo@dephub.go.id','','2002-12-01','2003-12-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198007022002121003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(79,'198101132002122002',NULL,'1981-01-13','PALEMBANG','NURUL RAHMANI ELIANA','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2019-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Pengevaluasi Program Diklat','2015-03-02','nurul_rahmani@dephub.go.id','','2002-12-01','2003-12-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198101132002122002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(80,'198110122002121002',NULL,'1981-10-12','SENTANI','MUKHLAS HAMDANI, S.T.','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Program dan Pelaporan','008017002002000','2019-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'ASISTEN AHLI','2019-03-21','mukhlas_hamdani@dephub.go.id','','2002-12-01','2003-12-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812072811.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(81,'198112082002121002',NULL,'1981-12-08','PURWOREJO','PRIMA ARKHA KHAIZAR','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2019-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Teknisi','2016-01-01','prima_arkha@dephub.go.id','','2002-12-01','2003-12-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198112082002121002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(82,'198403162010122002',NULL,'1984-03-16','JAKARTA','DERMA WATTY SIHOMBING, S.E., M. M.','P','Q030122','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Program dan Pelaporan','008017002002000','2019-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'PENYUSUN BAHAN EVALUASI DAN PELAPORAN','2015-01-02','derma_watty@dephub.go.id','','2010-12-01','2011-01-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198403162010122002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(83,'198701132010122007',NULL,'1987-01-13','Jakarta','SRI WULANDARI, S.Sos.','P','Q030119','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Rumah Tangga dan Hubungan Masyarakat','008017002003000','2019-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'PENGELOLA URUSAN KERUMAHTANGGAAN','2016-02-05','sri_wulandari@dephub.go.id','','2010-12-01','2011-12-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198701132010122007.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(84,'198708122010121002',NULL,'1987-08-12','Jakarta','WILLIYANTO, S.E.','L','Q030123','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2019-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'BENDAHARA','2019-01-01','williyanto@dephub.go.id','','2010-12-01','2011-12-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198708122010121002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(85,'197603172003122001',NULL,'1976-03-17','KLATEN','RINI SETIYAWATI, S.Kom.','P','L 200674','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2020-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Penyusun Kebutuhan Tenaga Kependidikan','2020-05-01','rini_setiyawati@dephub.go.id','','2003-12-01','2004-12-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197603172003122001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(86,'197707071998031003',NULL,'1977-07-07','JAKARTA','MUHAMAD IHSAN','L','H016001','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2020-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Petugas Legalisasi Sertifikat Kepelautan','2020-08-01','muhamad_ihsan@dephub.go.id','','1998-03-01','1999-06-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197707071998031003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(87,'198006052008121001',NULL,'1980-06-05','Jakarta','MARKUS YANDO, S.Si.T.','L','P 084185','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua II','008017004000000','2020-04-01','III/c','Penata',NULL,NULL,NULL,NULL,'Lektor','2019-04-05','markus_yando@dephub.go.id','','2008-12-01','2009-12-01','Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198006052008121001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(88,'196412161986031001',NULL,'1964-12-16','CIREBON','RUHAENO','L','E.402811','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2005-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENGELOLA ADMINISTRASI PENGABDIAN','2016-05-30','ruhaeno@dephub.go.id','','1986-03-01','1987-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196412161986031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(89,'196308211987031002',NULL,'1963-08-21','MUARA LABUH, SUMBAR','SISWANDI','L','E.595223','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2007-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENYUSUN RENCANA PEMELIHARAAN DAN PERBAIKAN','2016-03-21','siswandi@dephub.go.id','','1987-03-01','1989-02-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196308211987031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(90,'196411041988032002',NULL,'1964-11-04','KLATEN','CHRISTIANA ENDANG IRIANTI','P','E.620799','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2007-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Pengadministrasi Umum','2020-09-01','christiana_endang@dephub.go.id','6289024','1988-03-01','1989-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196411041988032002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(91,'196610201988031002',NULL,'1966-10-20','GEMPOLAN, SUMUT','DANIEL PASARIBU','L','E.620803','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2008-10-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Petugas Asrama dan Rumah Dinas','2020-09-01','daniel_pasaribu@dephub.go.id','','1988-03-01','1989-05-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196610201988031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(92,'196901021989032002',NULL,'1969-01-02','RANGKASBITUNG/ LEBAK','RASMINI','P','E.745080','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2009-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENGADMINISTRASI UMUM','2016-02-05','rasmini@dephub.go.id','','1989-03-01','1990-10-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196901021989032002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(93,'196601291989121001',NULL,'1966-01-29','JAKARTA','EMAN RESMANA','L','E.800975','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2010-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENGINVENTARIS BARANG DAN ATK','2018-01-01','eman_resmana@dephub.go.id','','1989-12-01','1991-05-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196601291989121001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(94,'196711271991031002',NULL,'1967-11-27','TEGAL','SURATNO RS','L','F189025','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2010-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Pengelola Kerumahtanggaan','2018-07-01','suratno_rs@dephub.go.id','88973382','1991-03-01','1992-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196711271991031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(95,'197105121993031002',NULL,'1971-05-12','CILACAP','SUDIR','L','G.025812','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2012-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Teknisi','2015-03-02','sudir@dephub.go.id','','1993-03-01','1994-03-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197105121993031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(96,'197107021992031002',NULL,'1971-07-02','BOGOR','SUMPENO','L','F.333683','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2012-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Penyusun Bahan Rencana, Program, Evaluasi Olah  Raga','2020-08-01','sumpeno@dephub.go.id','','1992-03-01','1993-04-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197107021992031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(97,'197208191993031003',NULL,'1972-08-19','MALANG','TEGUH SETYO WIDODO','L','G025819','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2012-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENGELOLA KETATAUSAHAAN','2016-06-13','teguh_setyo@dephub.go.id','5608865','1993-03-01','1994-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197208191993031003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(98,'197211191993031003',NULL,'1972-11-19','JAKARTA','SARIMIN','L','G025817','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2012-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENGELOLA KERUMAHTANGGAAN','2018-01-01','sarimin@dephub.go.id','','1993-03-01','1994-04-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197211191993031003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(99,'196902051993031001',NULL,'1969-02-05','GOMBONG','SURYONO','L','G.025815','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2013-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Penyusun Rencana Pemeliharaan dan Perbaikan','2020-08-01','suryono@dephub.go.id','-','1993-03-01','1994-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196902051993031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(100,'197606052002122001',NULL,'1976-06-05','JAKARTA','MIRAWATI, A.Mk.','P','L.141191','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2013-10-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Perawat Pelaksana Lanjutan','2009-04-01','mirawati@dephub.go.id','','2002-12-01','2003-12-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197606052002122001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(101,'197607092002122001',NULL,'1976-07-09','JAKARTA','SITI FAUZIAH, A.Mk.','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2013-10-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PERAWAT PELAKSANA LANJUTAN','2010-01-25','siti_fauziah@dephub.go.id','','2002-12-01','2003-12-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197607092002122001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(102,'197712162002122005',NULL,'1977-12-16','JAKARTA','URIP RAHAYU ENDI, A.Mk.','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2013-10-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PERAWAT PELAKSANA LANJUTAN','2010-01-25','urip_rahayu@dephub.go.id','','2002-12-01','2003-12-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197712162002122005.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(103,'198007272009121001',NULL,'1980-07-27','Situjuh Batur','DENNY FITRIAL, S.Si., M. T.','L','P206926','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2014-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'ASISTEN AHLI DENGAN TUGAS TAMBAHAN SEKRETARIS JURUSAN NAUTIKA','2018-07-26','denny_fitrial@dephub.go.id','','2009-12-01','2010-12-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198007272009121001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(104,'198702142009122002',NULL,'1987-02-14','Palembang','LESTARI, S.Si.T., M.M.','P','P 206942','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000','2014-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Penyusun Rencana Metode Pembelajaran Bahasa','2019-09-25','lestari@dephub.go.id','81373078474','2009-12-01','2010-12-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198702142009122002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(105,'198708062009122004',NULL,'1987-08-06','Surabaya','DIAN KARTIKA SARI, S.T.','P','P206927','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2014-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Penyusun Rencana Pemeliharaan dan Pebaikan Sarana dan Prasarana','2010-01-25','dian_kartika0608@dephub.go.id','','2009-12-01','2010-12-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198708062009122004.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(106,'198101062015032001',NULL,'1981-01-06','SEMARANG','SARI KUSUMANINGRUM, S.S., M.Hum.','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tenaga Pendidikan','008017001002000','2016-05-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'ASISTEN AHLI','2017-10-01','sari_kusumaningrum@dephub.go.id','','2015-03-01','2016-05-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198101062015032001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(107,'198811202015031001',NULL,'1988-11-20','BOJONEGORO','IMAM FAHCRUDDIN, S.Si., M.Sc.','L','571/KEP/KARPEG/2018','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Pembantu Ketua I','008017003000000','2016-05-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'ASISTEN AHLI','2017-10-01','imam_fahcruddin@dephub.go.id','085326349333','2015-03-01','2016-05-01','S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198811202015031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(108,'196901301990081001',NULL,'1969-01-30','TEGAL','DARYONO','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2016-10-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Petugas Asrama dan Rumah Dinas','2019-01-02','daryono@dephub.go.id','','1990-08-01','1992-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812043748.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(109,'196907151989031001',NULL,'1969-07-15','CIREBON','RASMAD','L','E 745085','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Rumah Tangga dan Hubungan Masyarakat','008017002003000','2016-10-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Perwira Aktifitas','2020-01-02','rasmad@dephub.go.id','','1989-03-01','1990-10-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812044261.JPG','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(110,'196309071989031002',NULL,'1963-09-07','JAKARTA','SUWARNO','L','E745082','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'SEKRETARIS PIMPINAN','2015-03-02','suwarno@dephub.go.id','','1989-03-01','1990-04-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196309071989031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(111,'196510051989031001',NULL,'1965-10-05','CIREBON','MUHAMMAD SUSWORO','L','E.745088','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENGINVENTARIS BARANG DAN ATK','2017-01-02','muhammad_susworo@dephub.go.id','-','1989-03-01','1990-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196510051989031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(112,'197009261989031001',NULL,'1970-09-26','CIREBON','SUHABUDIN','L','E.800979','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Praktek Kerja Nyata','008017001003000','2017-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Penyiap Program Praktek Kerja','2020-01-02','suhabudin@dephub.go.id','','1989-03-01','1989-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197009261989031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(113,'197302281997032003',NULL,'1973-02-28','JAKARTA','SITI ALIYAH','P','G.389280','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2017-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Penata Laporan Keuangan','2018-01-01','siti_aliyah@dephub.go.id','','1997-03-01','1998-04-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197302281997032003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(114,'197808011997031001',NULL,'1978-08-01','SUMEDANG','AGUS RUSNAWAN','L','433140','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Ketarunaan dan Alumni','008017001004000','2017-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PEMROSES DATA TARUNA DAN ALUMNI','2014-10-21','agus_rusnawan@dephub.go.id','267432580','1997-03-01','1998-05-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197808011997031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(115,'196610101989031002',NULL,'1966-10-10','CIAWI','SUHANDA','L','E.745084','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-05-26','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Pengelola Kerumahtanggaan','2019-01-02','suhanda@dephub.go.id','','1989-03-01','1990-10-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196610101989031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(116,'196711131991031001',NULL,'1967-11-13','TEGAL','SURATNO','L','F164354','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Rumah Tangga dan Hubungan Masyarakat','008017002003000','2017-10-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENGELOLA URUSAN KERUMAHTANGGAAN','2015-03-02','suratno@dephub.go.id','','1991-03-01','1992-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196711131991031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(117,'197303061998031004',NULL,'1973-03-06','GUNUNG KIDUL','TATA HERU PRABAWA, S.H.','L','H. 031593','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2018-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENGELOLA KEPEGAWAIAN','2018-01-01','tata_heru@dephub.go.id','','1998-03-01','1999-04-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197303061998031004.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(118,'197508061998031002',NULL,'1975-08-06','JAKARTA','RASYID GUNAWAN','L','H.031589','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2018-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Pengadministrasi Umum','2019-01-01','rasyid_gunawan@dephub.go.id','','1998-03-01','1999-04-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197508061998031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(119,'197611301998032001',NULL,'1976-11-30','LABUAN','ENDAH SUARNI','P','I.032010','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2018-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENYUSUN PROGRAM PROSES BELAJAR MENGAJAR','2017-05-01','endah_suarni@dephub.go.id','','1998-03-01','1999-04-01','Diploma I',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197611301998032001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(120,'197704151998032001',NULL,'1977-04-15','JAKARTA','NURITA FIRLAINA, S.E.','P','J.021471','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2018-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PEMROSES ADMINISTRASI KEUANGAN','2016-02-01','nurita_firlaina@dephub.go.id','5712982','1998-03-01','1999-04-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197704151998032001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(121,'197711161998031001',NULL,'1977-11-16','SUMEDANG','ADE KARSADI, S.E.','L','H031592','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Rumah Tangga dan Hubungan Masyarakat','008017002003000','2018-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENGELOLA URUSAN KERUMAHTANGGAAN','2017-01-02','ade_karsadi@dephub.go.id','','1998-03-01','1998-03-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197711161998031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(122,'197812251998032002',NULL,'1978-12-25','JAKARTA','RINA MARLIANA','P','I.032009','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Ketarunaan dan Alumni','008017001004000','2018-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'PENYUSUN PROGRAM DAN LAPORAN KEGIATAN KETARUNAAN','2018-01-01','rina_marliana@dephub.go.id','','1998-03-01','1999-04-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197812251998032002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(123,'198704022014021004',NULL,'1987-04-02','TEGAL','ADIN SAYEKTI, S.S.T.PEL','L','B 00000052','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2018-04-01','III/b','Penata Muda Tingkat I',NULL,NULL,NULL,NULL,'Fungsional Umum','2020-10-22','adin_sayekti@dephub.go.id','081228929977','2014-02-01',NULL,'Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198704022014021004.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(124,'197512182007101001',NULL,'1975-12-18','Jakarta','MOHAMAD ARIEF','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2016-10-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'PEMROSES ADMINISTRASI KEUANGAN','2015-03-02','mohamad_arief@dephub.go.id','','2007-10-01','2008-12-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197512182007101001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(125,'198002272009011013',NULL,'1980-02-27','Brebes','AHMAD TALHIS','L','P 084189','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Program dan Pelaporan','008017002002000','2016-10-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'Penyusun Bahan Evaluasi dan Pelaporan','2020-07-01','ahmad_talhis@dephub.go.id','','2009-01-01','2010-10-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198002272009011013.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(126,'197704301998031002',NULL,'1977-04-30','SEMARANG','ARIFIN BUDIANTO','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'PENGELOLA KEPEGAWAIAN','2012-09-01','arifin_budianto@dephub.go.id','','1998-03-01','2013-04-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812062193.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(127,'198302282008121001',NULL,'1983-02-28','JAKARTA','BOY LAKSMANA, S.E.','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'PENGELOLA KEPEGAWAIAN','2015-03-02','boy_laksmana@dephub.go.id','43900785','2008-12-01','2013-04-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198302282008121001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(128,'196809171989031003',NULL,'1968-09-17','CIAMIS','SARIP','L','E.745081','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-10-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'PENGELOLA KERUMAHTANGGAAN','2016-06-13','sarip@dephub.go.id','','1989-03-01',NULL,'SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196809171989031003.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(129,'197901232006041001',NULL,'1979-01-23','JAKARTA','HADI HIDAYAT, AmdPK','L','985/KEP/KARPEG/2007','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-10-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'PEREKAM MEDIS PELAKSANA LANJUTAN','2017-08-10','hadi_hidayat@dephub.go.id','','2006-04-01','2007-04-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812064224.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(130,'197910032010122001',NULL,'1979-10-03','DUMAI','HEINCE ERNA PELITA, A.Md.','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2019-04-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'PENGELOLA KEPEGAWAIAN','2015-03-02','heince_erna@dephub.go.id','081382650032','2010-12-01',NULL,'S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197910032010122001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(131,'198707302019021001',NULL,'1987-07-30','MEDAN','PESTA VERI AHMADI NAPITUPULU, S.ST.Pel','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2020-02-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'Perwira Aktifitas','2020-08-01','','081285858786','2019-02-01','2020-02-01','Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/default.png','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(132,'199207182019021002',NULL,'1992-07-18','KENDAL','YUSUF PRIA UTAMA, S.Tr.Pel','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2020-02-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'Penyusun Bahan Evaluasi dan Pelaporan','2020-08-01','','087700069345','2019-02-01','2020-02-01','Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1907240805.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(133,'199212182019021001',NULL,'1992-12-18','PURWAKARTA','DONY FERDINAL FRENGKI SIMARMATA, S.S.T.Pel','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2020-02-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'Penyusun Bahan Evaluasi dan Pelaporan','2020-08-01','','085215606616','2019-02-01','2020-02-01','Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1907240328.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(134,'199305072019021003',NULL,'1993-05-07','JAKARTA UTARA','NATANAEL SURANTA, S.S.T.Pel','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2020-02-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'Perwira Aktifitas','2020-08-01','','081319786453','2019-02-01','2020-02-01','Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1907240329.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(135,'199508292019021001',NULL,'1995-08-29','MATARAM','AKHMAD GIFARI MULTAZAM, S.Tr.Pel','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2020-02-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'Pengevaluasi Program Praktek Kerja','2020-08-01','','08121307412','2019-02-01','2020-02-01','Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1907240339.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(136,'199601122019021002',NULL,'1996-01-12','SUKOHARJO','RIZAL ROCHMANSYAH, S.Tr.Pel','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2020-02-01','III/a','Penata Muda',NULL,NULL,NULL,NULL,'Fasilitator Tenaga Pengajar Pendidikan','2020-08-01','','085736492153','2019-02-01','2020-02-01','Diploma IV',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/default.png','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(137,'196604281989031001',NULL,'1966-04-28','RANGKASBITUNG','MOHAMAD  PARDI','L','E745093','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','II/d','Pengatur Tingkat I',NULL,NULL,NULL,NULL,'PENGINVENTARIS BARANG DAN ATK','2015-03-02','mohamad_pardi@dephub.go.id','','1989-03-01','1990-10-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196604281989031001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(138,'197504012007101001',NULL,'1975-04-01','Kediri','MUHAMMAD MARZUKI','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Rumah Tangga dan Hubungan Masyarakat','008017002003000','2019-10-01','II/d','Pengatur Tingkat I',NULL,NULL,NULL,NULL,'PENYUSUN BAHAN PUBLIKASI DAN KEHUMASAN','2016-02-01','muhammad_marzuki@dephub.go.id','0817805362','2007-10-01','2008-12-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197504012007101001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(139,'197505092007101001',NULL,'1975-05-09','Jakarta','SUPRIYANTO','L','N164170','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Rumah Tangga dan Hubungan Masyarakat','008017002003000','2019-10-01','II/d','Pengatur Tingkat I',NULL,NULL,NULL,NULL,'PENGELOLA URUSAN KERUMAHTANGGAAN','2016-07-01','supriyanto0905@dephub.go.id','','2007-10-01','2008-12-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197505092007101001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(140,'198511012010121004',NULL,'1985-11-01','MALANG','AULIYA RACHMAN, A.Md. Kep.','L','Q030125','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2011-12-01','II/c','Pengatur',NULL,NULL,NULL,NULL,'Perawat Kesehatan','2011-03-31','auliya_rachman@dephub.go.id','','2010-12-01','2011-01-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/198511012010121004.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(141,'196307041993031002',NULL,'1963-07-04','JAKARTA','AGUS SUPRAYITNO','L','G025818','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2012-04-01','II/c','Pengatur',NULL,NULL,NULL,NULL,'Petugas Registrasi Diklat','2020-01-02','agus_suprayitno@dephub.go.id','','1993-03-01','1994-03-01','SLTP',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196307041993031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(142,'199501012016122002',NULL,'1995-01-01','GUNUNG KIDUL','ROHMAH ISBIYANTI, A.Md.','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2018-02-01','II/c','Pengatur',NULL,NULL,NULL,NULL,'PENATA LAPORAN KEUANGAN','2016-12-01','','','2016-12-01','2018-02-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/199501012016122002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(143,'197701242010121001',NULL,'1977-01-24','Purworejo','PARNEN','L','Q030128','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2019-04-01','II/c','Pengatur',NULL,NULL,NULL,NULL,'Penyusun Bahan Rencana, Program, Evaluasi Olah  Raga','2020-01-02','parnen@dephub.go.id','','2010-12-01','2011-12-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197701242010121001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(144,'197709222010121001',NULL,'1977-09-22','PASURUAN','KHOIRUL ANAM','L','Q030127','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2019-04-01','II/c','Pengatur',NULL,NULL,NULL,NULL,'Pemroses Administrasi Keuangan','2020-01-02','khoirul_anam2209@dephub.go.id','','2010-12-01','2010-12-01','S-1/Sarjana',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197709222010121001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(145,'199606082018122001',NULL,'1996-06-08','PATI','YUANISA NURFAHMI, A.Md.Ak.','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2019-12-01','II/c','Pengatur',NULL,NULL,NULL,NULL,'Pengelola Keuangan','2019-02-01','yuanisa@dephub.go.id','','2018-12-01','2019-12-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1907290150.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(146,'199707162018122002',NULL,'1997-07-16','TEGAL','TATI MAULIDIA, A.Md.Ak.','P','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Keuangan','008017002004000','2019-12-01','II/c','Pengatur',NULL,NULL,NULL,NULL,'Penata Laporan Keuangan','2020-08-01','tatimaulidia@dephub.go.id','083878954263','2018-12-01','2019-12-01','Diploma III/Sarjana Muda',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1907290151.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(147,'197205152012121005',NULL,'1972-05-15','Klaten','SRIYOTO','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Rumah Tangga dan Hubungan Masyarakat','008017002003000','2017-04-01','II/b','Pengatur Muda Tingkat I',NULL,NULL,NULL,NULL,'PENGELOLA URUSAN KERUMAHTANGGAAN','2016-02-01','sriyoto@dephub.go.id','','2012-12-01','2014-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197205152012121005.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(148,'197306062012121006',NULL,'1973-06-06','Lebak','AJAT SUDRAJAT','L','A00001295','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','II/b','Pengatur Muda Tingkat I',NULL,NULL,NULL,NULL,'Petugas Registrasi Diklat','2020-01-02','ajat_sudrajat0606@dephub.go.id','083804002908','2012-12-01','2014-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812053287.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(149,'197905172012121005',NULL,'1979-05-17','Kuningan','MULYADI','L','A 00001296','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2017-04-01','II/b','Pengatur Muda Tingkat I',NULL,NULL,NULL,NULL,'Teknisi','2015-01-02','mulyadi1705@dephub.go.id','081380549027','2012-12-01','2014-03-01','SLTA',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/1812064608.JPG','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(150,'197202052007011001',NULL,'1972-02-05','Jakarta','SATIMAN','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Rumah Tangga dan Hubungan Masyarakat','008017002003000','2019-10-01','II/b','Pengatur Muda Tingkat I',NULL,NULL,NULL,NULL,'Pramu Pustaka','2020-01-02','satiman@dephub.go.id','','2007-01-01','2008-12-01','SLTP',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/197202052007011001.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(151,'196310151989031002',NULL,'1963-10-15','DKI JAKARTA','JUMADI','L','E.745095','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2005-04-01','II/a','Pengatur Muda',NULL,NULL,NULL,NULL,'PENGADMINISTRASI UMUM','2015-03-02','jumadi@dephub.go.id','','1989-03-01','1990-10-01','Sekolah Dasar',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196310151989031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(152,'196502141989031002',NULL,'1965-02-14','KUNINGAN','UNGKANG SUKLAN','L','E 745092','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Subbagian Tata Usaha dan Kepegawaian','008017002001000','2005-04-01','II/a','Pengatur Muda',NULL,NULL,NULL,NULL,'Petugas Registrasi Diklat','2020-08-01','ungkang_suklan@dephub.go.id','','1989-03-01',NULL,'Sekolah Dasar',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/196502141989031002.jpg','2020-11-17 03:51:33','2020-11-17 03:51:33'),
(153,'196908131997031001',NULL,'1969-08-13','YOGYAKARTA','RADEN ARI WIDIANTO, SH, ME','L','','Badan Pengembangan Sumber Daya Manusia Perhubungan','008000000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'Sekolah Tinggi Ilmu Pelayaran','008017000000000',NULL,'','',NULL,NULL,NULL,NULL,'Asisten Ahli','2019-12-30','','','1997-03-01',NULL,'S-2',NULL,'https://sik.dephub.go.id/ftpsik/doc/images/users/default.png','2020-11-17 03:51:33','2020-11-17 03:51:33');

/*Table structure for table `f10_probabilitas_resiko` */

DROP TABLE IF EXISTS `f10_probabilitas_resiko`;

CREATE TABLE `f10_probabilitas_resiko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f10_probabilitas_resiko` */

insert  into `f10_probabilitas_resiko`(`id`,`jenis`,`tipe`,`nilai`,`updated_at`,`created_at`) values 
(1,'MINIMAL','I','55','2020-11-18 09:32:50','2020-11-18 09:32:50'),
(2,'SEDANG','II','65','2020-11-18 09:33:05','2020-11-18 09:33:05'),
(3,'MENENGAH','III','75','2020-11-18 09:33:30','2020-11-18 09:33:30'),
(4,'TINGGI','IV','85','2020-11-18 09:33:43','2020-11-18 09:33:43');

/*Table structure for table `f1_kompetensi_teknis` */

DROP TABLE IF EXISTS `f1_kompetensi_teknis`;

CREATE TABLE `f1_kompetensi_teknis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f1_kompetensi_teknis` */

insert  into `f1_kompetensi_teknis`(`id`,`jenis`,`tipe`,`nilai`,`updated_at`,`created_at`) values 
(5,'PRIMER','A','100','2020-11-17 08:12:10','2020-11-17 08:12:10'),
(8,'TEKNIS DASAR','B','115','2020-11-17 08:47:08','2020-11-17 08:47:08'),
(9,'TEKNIS PRAKTIS','C','138','2020-11-17 09:28:10','2020-11-17 09:28:10'),
(10,'ANALISIS DASAR','D','179','2020-11-17 09:31:06','2020-11-17 09:31:06'),
(11,'ANALISIS TERINTEGRASI','E','260','2020-11-17 09:31:56','2020-11-17 09:31:56'),
(12,'ANALISIS KONSEPTUAL','F','429','2020-11-17 09:32:25','2020-11-17 09:32:25'),
(13,'PROFESIONAL','G','773','2020-11-17 09:32:44','2020-11-17 09:32:44'),
(14,'AHLI','H','1468','2020-11-20 03:39:58','2020-11-17 09:33:03'),
(15,'MASTER','I','2233','2020-11-17 09:33:18','2020-11-17 09:33:18');

/*Table structure for table `f2_manajerial` */

DROP TABLE IF EXISTS `f2_manajerial`;

CREATE TABLE `f2_manajerial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `lingkup` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f2_manajerial` */

insert  into `f2_manajerial`(`id`,`jenis`,`kelas`,`tipe`,`lingkup`,`nilai`,`created_at`,`updated_at`) values 
(1,'DIRI SENDIRI','I','A','HUBUNGAN PENGGUNA LAYANAN','10','2020-11-17 10:42:02','2020-11-20 03:40:48'),
(2,'DIRI SENDIRI','I','B','HUBUNGAN FUNGSI LAIN','11','2020-11-17 10:45:43','2020-11-17 10:45:43'),
(3,'DIRI SENDIRI','I','C','HUBUNGAN MATRIX PROYEK LAYANAN PENDIDIKAN','19','2020-11-17 10:52:40','2020-11-17 10:52:40'),
(4,'KOORDINATOR','II','A','KOORDINATOR TIM KERJA SHIFT','14','2020-11-17 10:57:17','2020-11-17 10:57:17'),
(5,'KOORDINATOR','II','B','KOORDINATOR TIM KERJA TETAP','16','2020-11-17 15:49:49','2020-11-17 15:49:49'),
(6,'SUPERVISI','III','A','MATRIKS OPERASIONAL','23','2020-11-17 15:51:21','2020-11-17 15:51:21'),
(7,'SUPERVISI','III','B','MATRIKS PROYEK LAYANAN PENDIDIKAN','27','2020-11-17 15:52:48','2020-11-17 15:52:48'),
(8,'SUPERVISI','III','C','LINI TEKNIS OPERASIONAL','41','2020-11-17 15:53:35','2020-11-17 15:53:35'),
(9,'MANAJERIAL','IV','A','STRATEGI OPERASIONAL','64','2020-11-17 15:54:09','2020-11-17 15:54:09'),
(10,'MANAJERIAL','IV','B','STRATEGI USAHA','74','2020-11-17 15:54:57','2020-11-17 15:54:57');

/*Table structure for table `f3_komunikasi` */

DROP TABLE IF EXISTS `f3_komunikasi`;

CREATE TABLE `f3_komunikasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f3_komunikasi` */

insert  into `f3_komunikasi`(`id`,`jenis`,`kelas`,`nilai`,`created_at`,`updated_at`) values 
(1,'UMUM','1','15','2020-11-17 17:08:05','2020-11-17 17:08:05'),
(2,'PENTING','2','23','2020-11-17 17:10:28','2020-11-17 17:10:28'),
(3,'SIGNIFIKAN','3','34','2020-11-17 17:10:46','2020-11-17 17:10:46'),
(4,'KRITIS','4','69','2020-11-17 17:11:01','2020-11-20 03:42:33');

/*Table structure for table `f4_analisis_lingkungan_pekerjaan` */

DROP TABLE IF EXISTS `f4_analisis_lingkungan_pekerjaan`;

CREATE TABLE `f4_analisis_lingkungan_pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f4_analisis_lingkungan_pekerjaan` */

insert  into `f4_analisis_lingkungan_pekerjaan`(`id`,`jenis`,`kelas`,`nilai`,`created_at`,`updated_at`) values 
(1,'REPETISI','1','3','2020-11-18 02:30:16','2020-11-18 02:30:16'),
(2,'BERPOLA','2','5','2020-11-18 02:30:50','2020-11-18 02:30:50'),
(3,'SEMI VARIABLE','3','10','2020-11-18 02:32:00','2020-11-18 02:32:00'),
(4,'VARIABLE','4','19','2020-11-18 02:33:24','2020-11-18 02:33:24'),
(5,'ADAPTIF','5','34','2020-11-18 02:33:48','2020-11-18 02:33:48'),
(6,'INOVATIF','6','57','2020-11-18 02:34:08','2020-11-18 02:34:08');

/*Table structure for table `f5_pedoman_keputusan` */

DROP TABLE IF EXISTS `f5_pedoman_keputusan`;

CREATE TABLE `f5_pedoman_keputusan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f5_pedoman_keputusan` */

insert  into `f5_pedoman_keputusan`(`id`,`jenis`,`tipe`,`nilai`,`updated_at`,`created_at`) values 
(1,'BAKU, SANGAN RUTIN','A','2','2020-11-18 02:36:30','2020-11-18 02:36:30'),
(2,'RUTIN','B','3','2020-11-18 02:36:48','2020-11-18 02:36:48'),
(3,'SEMI RUTIN','C','5','2020-11-18 02:37:08','2020-11-18 02:37:08'),
(4,'KHUSUS DAN OPERASIONAL','D','10','2020-11-18 02:37:28','2020-11-18 02:37:28'),
(5,'INTEGRASI KEBIJAKAN','E','19','2020-11-18 02:37:58','2020-11-18 02:37:58'),
(6,'KEBIJAKAN LUAS','F','37','2020-11-18 02:38:13','2020-11-18 02:38:13');

/*Table structure for table `f6_kondisi_kerja` */

DROP TABLE IF EXISTS `f6_kondisi_kerja`;

CREATE TABLE `f6_kondisi_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f6_kondisi_kerja` */

insert  into `f6_kondisi_kerja`(`id`,`jenis`,`tipe`,`kode`,`kelas`,`nilai`,`created_at`,`updated_at`) values 
(1,'LINGKUNGAN FISIK','A','A1','1','2','2020-11-18 02:59:35','2020-11-18 02:59:35'),
(2,'LINGKUNGAN FISIK','A','A2','2','7','2020-11-18 03:00:00','2020-11-18 03:00:00'),
(3,'LINGKUNGAN FISIK','A','A3','3','13','2020-11-18 03:00:20','2020-11-18 03:00:20'),
(4,'LINGKUNGAN FISIK','A','A4','4','19','2020-11-18 03:00:41','2020-11-18 03:00:41'),
(5,'KONTRIBUSI FISIK','B','B1','1','2','2020-11-18 03:01:39','2020-11-18 03:01:39'),
(6,'KONTRIBUSI FISIK','B','B2','2','7','2020-11-18 03:01:59','2020-11-18 03:01:59'),
(7,'KONTRIBUSI FISIK','B','B3','3','13','2020-11-18 03:03:09','2020-11-18 03:03:09'),
(8,'KONTRIBUSI FISIK','B','B4','4','19','2020-11-18 03:03:32','2020-11-18 03:03:32'),
(9,'KOORDINASI PANCA INDERA','C','C1','1','2','2020-11-18 03:04:12','2020-11-18 03:04:12'),
(10,'KOORDINASI PANCA INDERA','C','C2','2','7','2020-11-18 03:04:33','2020-11-18 03:04:33'),
(11,'KOORDINASI PANCA INDERA','C','C3','3','13','2020-11-18 03:04:52','2020-11-18 03:04:52'),
(12,'KOORDINASI PANCA INDERA','C','C4','4','19','2020-11-18 03:05:11','2020-11-18 03:05:11'),
(13,'KONSENTRASI / KETEGANGAN MENTAL','D','D1','1','4','2020-11-18 03:06:07','2020-11-18 03:06:07'),
(14,'KONSENTRASI / KETEGANGAN MENTAL','D','D2','2','14','2020-11-18 03:06:35','2020-11-18 03:06:35'),
(15,'KONSENTRASI / KETEGANGAN MENTAL','D','D3','3','27','2020-11-18 03:06:59','2020-11-18 03:06:59'),
(16,'KONSENTRASI / KETEGANGAN MENTAL','D','D4','4','39','2020-11-18 03:07:26','2020-11-18 03:07:26'),
(17,'LAMA KONSENTRASI KETEGANGAN MENTAL','E','E1','1','4','2020-11-18 03:08:31','2020-11-18 03:08:31'),
(18,'LAMA KONSENTRASI KETEGANGAN MENTAL','E','E2','2','14','2020-11-18 03:08:50','2020-11-18 03:08:50'),
(19,'LAMA KONSENTRASI KETEGANGAN MENTAL','E','E3','3','27','2020-11-18 03:09:33','2020-11-18 03:09:33'),
(20,'LAMA KONSENTRASI KETEGANGAN MENTAL','E','E4','4','39','2020-11-18 03:10:01','2020-11-18 03:10:01'),
(21,'POLA JADUAL KERJA','F','F1','1','4','2020-11-18 03:10:38','2020-11-18 03:10:38'),
(22,'POLA JADUAL KERJA','F','F2','2','14','2020-11-18 03:11:13','2020-11-18 03:11:13'),
(23,'POLA JADUAL KERJA','F','F3','3','27','2020-11-18 03:11:28','2020-11-18 03:11:28'),
(24,'POLA JADUAL KERJA','F','F4','4','39','2020-11-18 03:11:45','2020-11-18 03:11:45'),
(25,'KETERDESAKAN TINDAKAN LAYANAN','G','G1','1','4','2020-11-18 03:12:28','2020-11-18 03:12:28'),
(26,'KETERDESAKAN TINDAKAN LAYANAN','G','G2','2','14','2020-11-18 03:12:46','2020-11-18 03:12:46'),
(27,'KETERDESAKAN TINDAKAN LAYANAN','G','G3','3','27','2020-11-18 03:13:05','2020-11-18 03:13:05'),
(28,'KETERDESAKAN TINDAKAN LAYANAN','G','G4','4','39','2020-11-18 03:13:23','2020-11-18 03:13:23'),
(29,'KETERKAITAN LAYANAN PENDIDIKAN','H','H1','1','2','2020-11-18 03:14:15','2020-11-18 03:14:15'),
(30,'KETERKAITAN LAYANAN PENDIDIKAN','H','H2','2','7','2020-11-18 03:14:58','2020-11-18 03:14:58'),
(31,'KETERKAITAN LAYANAN PENDIDIKAN','H','H3','3','13','2020-11-18 03:15:13','2020-11-18 03:15:13'),
(32,'KETERKAITAN LAYANAN PENDIDIKAN','H','H4','4','39','2020-11-18 03:15:37','2020-11-18 03:15:37'),
(33,'KOMPLEKSITAS TINDAKAN PENDIDIKAN','I','I1','1','4','2020-11-18 03:18:12','2020-11-18 03:18:12'),
(34,'KOMPLEKSITAS TINDAKAN PENDIDIKAN','I','I2','2','14','2020-11-18 07:29:34','2020-11-18 07:29:34'),
(35,'KOMPLEKSITAS TINDAKAN PENDIDIKAN','I','I3','3','27','2020-11-18 07:59:30','2020-11-18 07:59:30'),
(36,'KOMPLEKSITAS TINDAKAN PENDIDIKAN','I','I4','4','39','2020-11-18 08:00:05','2020-11-18 08:00:05'),
(37,'BRAND IMAGE PENDIDIKAN','J','J1','1','5','2020-11-18 08:00:46','2020-11-18 08:00:46'),
(38,'BRAND IMAGE PENDIDIKAN','J','J2','2','15','2020-11-18 08:01:15','2020-11-18 08:01:15'),
(39,'BRAND IMAGE PENDIDIKAN','J','J3','3','31','2020-11-18 08:01:38','2020-11-18 08:01:38'),
(40,'BRAND IMAGE PENDIDIKAN','J','J4','4','44','2020-11-18 08:01:53','2020-11-18 08:01:53'),
(41,'INFEKSI','K','K1','1','4','2020-11-18 08:02:40','2020-11-18 08:02:40'),
(42,'INFEKSI','K','K2','2','14','2020-11-18 08:03:03','2020-11-18 08:03:03'),
(43,'INFEKSI','K','K3','3','27','2020-11-18 08:03:21','2020-11-18 08:03:21'),
(44,'INFEKSI','K','K4','4','39','2020-11-18 08:03:46','2020-11-18 08:03:46'),
(45,'RADIASI','L','L1','1','4','2020-11-18 08:04:08','2020-11-18 08:04:08'),
(46,'RADIASI','L','L2','2','14','2020-11-18 08:04:32','2020-11-18 08:04:32'),
(47,'RADIASI','L','L3','3','27','2020-11-18 08:05:01','2020-11-18 08:05:01'),
(48,'RADIASI','L','L4','4','39','2020-11-18 08:05:21','2020-11-18 08:05:21');

/*Table structure for table `f78_nkh` */

DROP TABLE IF EXISTS `f78_nkh`;

CREATE TABLE `f78_nkh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `umum` int(11) DEFAULT NULL,
  `standar` int(11) DEFAULT NULL,
  `menengah` int(11) DEFAULT NULL,
  `menengah_tinggi` int(11) DEFAULT NULL,
  `tinggi` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f78_nkh` */

insert  into `f78_nkh`(`id`,`jenis`,`kelas`,`umum`,`standar`,`menengah`,`menengah_tinggi`,`tinggi`,`updated_at`,`created_at`) values 
(1,'SPESIFIK','1',30,43,62,88,'126','2020-11-19 05:10:05','2020-11-19 05:10:05'),
(2,'KONTROL','2',43,62,88,126,'158','2020-11-19 05:10:41','2020-11-19 05:10:41'),
(3,'STANDAR','3',62,88,126,158,'197','2020-11-19 06:21:39','2020-11-19 06:21:39'),
(4,'REGULASI','4',88,126,158,197,'247','2020-11-19 07:58:01','2020-11-19 07:58:01'),
(5,'REGULASI PROFESI','5',126,158,197,247,'308','2020-11-19 11:04:10','2020-11-19 11:03:17');

/*Table structure for table `f78_nkhk` */

DROP TABLE IF EXISTS `f78_nkhk`;

CREATE TABLE `f78_nkhk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `r` varchar(100) DEFAULT NULL,
  `k` varchar(100) DEFAULT NULL,
  `b` varchar(100) DEFAULT NULL,
  `u` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f78_nkhk` */

insert  into `f78_nkhk`(`id`,`jenis`,`kelas`,`r`,`k`,`b`,`u`,`updated_at`,`created_at`) values 
(1,'SPESIFIK','1','146','182','227','284','2020-11-19 06:22:22','2020-11-19 06:22:22'),
(2,'KONTROL','2','182','227','284','316','2020-11-19 10:58:30','2020-11-19 08:17:05');

/*Table structure for table `f78_nkhm` */

DROP TABLE IF EXISTS `f78_nkhm`;

CREATE TABLE `f78_nkhm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `r` varchar(100) DEFAULT NULL,
  `k` varchar(100) DEFAULT NULL,
  `b` varchar(100) DEFAULT NULL,
  `u` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f78_nkhm` */

insert  into `f78_nkhm`(`id`,`jenis`,`kelas`,`r`,`k`,`b`,`u`,`updated_at`,`created_at`) values 
(1,'SPESIFIK','1','180','225','281','351','2020-11-19 06:32:31','2020-11-19 06:32:31'),
(2,'KONTROL','2','225','281','351','390','2020-11-19 10:50:43','2020-11-19 10:50:06');

/*Table structure for table `f78_nkhs` */

DROP TABLE IF EXISTS `f78_nkhs`;

CREATE TABLE `f78_nkhs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `r` varchar(100) DEFAULT NULL,
  `k` varchar(100) DEFAULT NULL,
  `b` varchar(100) DEFAULT NULL,
  `u` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f78_nkhs` */

insert  into `f78_nkhs`(`id`,`jenis`,`kelas`,`r`,`k`,`b`,`u`,`updated_at`,`created_at`) values 
(1,'SPESIFIK','1','162','202','253','316','2020-11-19 06:24:12','2020-11-19 06:24:12'),
(2,'KONTROL','2','202','253','316','351','2020-11-19 10:43:45','2020-11-19 10:35:29');

/*Table structure for table `f78_nkht` */

DROP TABLE IF EXISTS `f78_nkht`;

CREATE TABLE `f78_nkht` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `r` varchar(100) DEFAULT NULL,
  `k` varchar(100) DEFAULT NULL,
  `b` varchar(100) DEFAULT NULL,
  `u` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f78_nkht` */

insert  into `f78_nkht`(`id`,`jenis`,`kelas`,`r`,`k`,`b`,`u`,`updated_at`,`created_at`) values 
(1,'SPESIFIK','1','200','250','312','390','2020-11-19 06:33:02','2020-11-19 06:33:02'),
(2,'KONTROL','2','250','312','390','433','2020-11-19 10:10:21','2020-11-19 10:04:52');

/*Table structure for table `f9_determinate` */

DROP TABLE IF EXISTS `f9_determinate`;

CREATE TABLE `f9_determinate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f9_determinate` */

insert  into `f9_determinate`(`id`,`jenis`,`tipe`,`nilai`,`created_at`,`updated_at`) values 
(1,'REMOTE','R','1.00','2020-11-18 08:46:31','2020-11-18 08:46:31'),
(2,'KONTROL','K','1.00','2020-11-18 08:47:13','2020-11-18 08:47:13'),
(3,'BERPERAN','B','1.00','2020-11-18 08:47:27','2020-11-19 09:48:44'),
(4,'UTAMA','U','1.00','2020-11-18 08:49:51','2020-11-18 08:49:51');

/*Table structure for table `f9_indeterminate` */

DROP TABLE IF EXISTS `f9_indeterminate`;

CREATE TABLE `f9_indeterminate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `nilai` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `f9_indeterminate` */

insert  into `f9_indeterminate`(`id`,`jenis`,`tipe`,`nilai`,`created_at`,`updated_at`) values 
(1,'REMOTE','R','1.00','2020-11-18 08:28:18','2020-11-18 08:28:18'),
(2,'KONTROL','K','1.25','2020-11-18 08:28:48','2020-11-18 08:28:48'),
(3,'BERPERAN','B','1.88','2020-11-18 08:29:16','2020-11-19 09:15:56'),
(4,'UTAMA','U','2.81','2020-11-18 08:29:33','2020-11-18 08:29:33');

/*Table structure for table `m_golongan` */

DROP TABLE IF EXISTS `m_golongan`;

CREATE TABLE `m_golongan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) DEFAULT NULL,
  `golongan` varchar(50) DEFAULT NULL,
  `persen` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `m_golongan` */

insert  into `m_golongan`(`id`,`jenis`,`golongan`,`persen`,`created_at`,`updated_at`) values 
(1,'PNS','I',15,'2020-11-25 07:37:26','2020-11-25 07:37:26'),
(2,'PNS','II',20,'2020-11-25 07:43:46','2020-11-25 07:43:46'),
(3,'PNS','III',25,'2020-11-25 07:49:17','2020-11-25 07:51:54'),
(4,'PNS','IV',30,'2020-11-25 07:52:12','2020-11-25 07:52:12'),
(5,'NON PNS','NON GOLONGAN',15,'2020-11-25 07:53:03','2020-11-25 07:53:03');

/*Table structure for table `m_grade` */

DROP TABLE IF EXISTS `m_grade`;

CREATE TABLE `m_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade` int(11) DEFAULT NULL,
  `persentase` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `m_grade` */

insert  into `m_grade`(`id`,`grade`,`persentase`,`created_at`,`updated_at`) values 
(1,1,35,'2020-11-25 05:13:13','2020-11-25 05:23:15'),
(2,2,35,'2020-11-25 05:23:26','2020-11-25 05:23:26'),
(3,3,35,'2020-11-25 12:24:10','2020-11-25 12:24:13'),
(4,4,35,'2020-11-25 12:24:15','2020-11-25 12:24:17'),
(5,5,35,'2020-11-25 12:24:19','2020-11-25 12:24:21'),
(6,6,35,'2020-11-25 12:24:23','2020-11-25 12:24:26'),
(7,7,35,'2020-11-25 12:24:28','2020-11-25 12:24:30'),
(8,8,35,'2020-11-25 12:24:32','2020-11-25 12:24:34'),
(9,9,35,'2020-11-25 12:24:36','2020-11-25 12:24:38'),
(10,10,40,'2020-11-25 12:25:38','2020-11-25 12:25:40'),
(11,11,45,'2020-11-25 12:25:42','2020-11-25 12:25:44'),
(12,12,50,'2020-11-25 12:25:47','2020-11-25 12:25:48'),
(13,13,50,'2020-11-25 12:25:51','2020-11-25 12:25:52'),
(14,14,50,'2020-11-25 12:25:55','2020-11-25 12:25:56'),
(15,15,50,'2020-11-25 12:25:58','2020-11-25 12:26:00');

/*Table structure for table `m_jabatan` */

DROP TABLE IF EXISTS `m_jabatan`;

CREATE TABLE `m_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jabatan_baru` varchar(200) NOT NULL,
  `grade_baru` int(11) NOT NULL,
  `kelas_jabatan` varchar(200) NOT NULL,
  `nilai_jabatan` int(11) NOT NULL,
  `gapok` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

/*Data for the table `m_jabatan` */

insert  into `m_jabatan`(`id`,`jabatan_baru`,`grade_baru`,`kelas_jabatan`,`nilai_jabatan`,`gapok`,`created_at`,`updated_at`) values 
(1,'KETUA',15,'STRUKTURAL ESELON II',4990,NULL,'2020-11-17 08:07:24','2020-11-17 08:07:24'),
(2,'PUKET',14,'DOSEN DENGAN TUGAS TAMBAHAN',3676,NULL,'2020-11-17 08:08:32','2020-11-17 08:08:32'),
(3,'KEPALA BAGIAN',14,'STRUKTURAL ESELON III',3637,NULL,'2020-11-17 09:29:19','2020-11-17 09:29:19'),
(4,'DIREKTURE PASCASARJANA',13,'DOSEN DENGAN TUGAS TAMBAHAN',3216,NULL,'2020-11-17 16:31:00','2020-11-17 10:13:06'),
(5,'KEPALA SUB BAGIAN I (KASUBBAG PROGRAM & PELAPORAN, KASUBBAG RT & HUMAS, KASUBBAG KEUANGAN)',12,'STRUKTURAL ESELON IV',2998,NULL,'2020-11-17 09:32:42','2020-11-17 10:28:15'),
(6,'KEPALA SUB BAGIAN II (KASUBBAG TU & KEPEGAWAIAN, KASUBBAG ADM, TENAGA KEPENDIDIKAN, AKADEMIK, KETARUNAAN & ALUMNI, PRAKTEK KERJA NYATA)',12,'STRUKTURAL ESELON IV',2873,NULL,'2020-11-17 10:34:12','2020-11-17 10:34:12'),
(7,'KEPALA PUSAT I (KEPALA PUSAT PEMBANGUNAN KARAKTER)',12,'DOSEN DENGAN TUGAS TAMBAHAN',2972,NULL,'2020-11-17 10:35:16','2020-11-17 10:35:16'),
(8,'KEPALA PUSAT II (KAPUS PENJAMINAN MUTU)',12,'DOSEN DENGAN TUGAS TAMBAHAN',2746,NULL,'2020-11-17 10:36:44','2020-11-17 10:36:44'),
(9,'WADIR PASCASARJANA',12,'DOSEN DENGAN TUGAS TAMBAHAN',2746,NULL,'2020-11-17 10:38:49','2020-11-17 10:38:49'),
(10,'KETUA JURUSAN',12,'DOSEN DENGAN TUGAS TAMBAHAN',2977,NULL,'2020-11-17 10:56:58','2020-11-17 10:56:58'),
(11,'KETUA PRODI',12,'DOSEN DENGAN TUGAS TAMBAHAN',2939,NULL,'2020-11-17 10:57:36','2020-11-17 10:57:36'),
(12,'KEPALA DIVISI PENGEMBANGAN USAHA',12,'DOSEN DENGAN TUGAS TAMBAHAN',2939,NULL,'2020-11-17 10:58:17','2020-11-17 10:58:17'),
(13,'KETUA SPI',12,'DOSEN DENGAN TUGAS TAMBAHAN',2873,NULL,'2020-11-17 10:59:16','2020-11-17 10:59:16'),
(14,'DIREKTUR SLP',12,'DOSEN DENGAN TUGAS TAMBAHAN',2846,NULL,'2020-11-17 10:59:58','2020-11-17 10:59:58'),
(15,'GURU BESAR',12,'DOSEN',2568,NULL,'2020-11-17 11:01:08','2020-11-17 11:01:08'),
(16,'KEPALA UNIT I (KANIT IJAZAH DAN SERTIFIKAT, KANIT BIMBINGAN TARUNA KANIT ASRAMA)',11,'DOSEN DENGAN TUGAS TAMBAHAN',2198,NULL,'2020-11-17 11:06:45','2020-11-17 11:06:45'),
(17,'KEPALA UNIT II',11,'DOSEN DENGAN TUGAS TAMBAHAN',2094,NULL,'2020-11-17 11:07:36','2020-11-17 11:07:36'),
(18,'FUNGSIONAL TERTENTU UTAMA',11,'FT UTAMA',2094,NULL,'2020-11-17 11:08:26','2020-11-17 11:08:26'),
(19,'LEKTOR KEPALA',11,'DOSEN',2105,NULL,'2020-11-17 11:09:50','2020-11-17 11:09:50'),
(20,'SEKRETARIS JURUSAN',10,'DOSEN DENGAN TUGAS TAMBAHAN',1902,NULL,'2020-11-17 11:11:25','2020-11-17 11:11:25'),
(21,'MANAGER LSP',10,'DOSEN DENGAN TUGAS TAMBAHAN',1834,NULL,'2020-11-17 11:11:55','2020-11-17 11:11:55'),
(22,'KEPALA SUB DIVISI PENGEMBANGAN USAHA',10,'DOSEN DENGAN TUGAS TAMBAHAN',1871,NULL,'2020-11-18 11:30:45','2020-11-18 11:30:47'),
(23,'SEKRETARIS SPI',10,'DOSEN DENGAN TUGAS TAMBAHAN',1784,NULL,'2020-11-18 11:31:26','2020-11-18 11:31:29'),
(24,'LEKTOR',10,'DOSEN',1775,NULL,'2020-11-18 11:32:34','2020-11-18 11:32:37'),
(25,'FUNGSIONAL TERTENTU MADYA',10,'FT MADYA',1775,NULL,'2020-11-18 11:33:08','2020-11-18 11:33:12'),
(26,'SEKRETARIS PRODI',10,'DOSEN DENGAN TUGAS TAMBAHAN',1753,NULL,'2020-11-18 11:33:52','2020-11-18 11:33:55'),
(27,'SEKRETARIS PENELITIAN',10,'DOSEN DENGAN TUGAS TAMBAHAN',1753,NULL,'2020-11-18 11:34:55','2020-11-18 11:34:58'),
(28,'SEKRETARIS PENGABDIAN',10,'DOSEN DENGAN TUGAS TAMBAHAN',1753,NULL,'2020-11-18 11:48:07','2020-11-18 11:48:09'),
(29,'ASISTEN AHLI',9,'DOSEN',1536,NULL,'2020-11-18 11:48:34','2020-11-18 11:48:36'),
(30,'KEPALA ULP',9,'FU1',1455,NULL,'2020-11-18 11:49:08','2020-11-18 11:49:11'),
(31,'BENDAHARA PENGELUARAN',9,'FU1',1455,NULL,'2020-11-18 11:49:58','2020-11-18 11:50:00'),
(32,'DOKTER GIGI MUDA',9,'FT MUDA',1506,NULL,'2020-11-18 11:50:26','2020-11-18 11:50:28'),
(33,'ANGGOTA SPI',8,'FU2',1391,NULL,'2020-11-18 11:50:59','2020-11-18 11:51:01'),
(34,'BENDAHARA MATERIL DAN PENGELOLA URUSAN KERUMAHTANGGAN',8,'FU2',1391,NULL,'2020-11-18 11:51:44','2020-11-18 11:51:46'),
(35,'BENDAHARA PENERIMA',8,'FU2',1391,NULL,'2020-11-18 11:56:22','2020-11-18 11:56:24'),
(36,'PENATA LAPORAN KEUANGAN I',8,'FU2',1391,NULL,'2020-11-18 11:56:56','2020-11-18 11:56:59'),
(37,'PENATA LAPORAN KEUANGAN II',8,'FU2',1391,NULL,'2020-11-18 11:57:34','2020-11-18 11:57:36'),
(38,'VERIFIKATOR KEUANGAN',8,'FU2',1391,NULL,'2020-11-18 11:58:31','2020-11-18 11:58:34'),
(39,'PENYUSUN RENCANA ANGGARAN DAN LAPORAN',8,'FU2',1391,NULL,'2020-11-18 11:59:08','2020-11-18 11:59:10'),
(40,'DOKTER PERTAMA',8,'FT PERTAMA',1293,NULL,'2020-11-18 11:59:43','2020-11-18 11:59:45'),
(41,'PENYUSUN KEBUTUHAN TENAGA PENDIDIK',7,'FU3',1194,NULL,'2020-11-18 12:00:40','2020-11-18 12:00:42'),
(42,'PENGEVALUASI PROGRAM DIKLAT',7,'FU3',1194,NULL,'2020-11-18 12:01:44','2020-11-18 12:01:47'),
(43,'PRANATA KOMPUTER',7,'FT MAHIR',1175,NULL,'2020-11-18 12:05:49','2020-11-18 12:05:51'),
(44,'PENYUSUN PROGRAM DAN LAPORAN KEGIATAN KETARUNAAN DAN ALUMNI',7,'FU3',1194,NULL,'2020-11-18 12:06:56','2020-11-18 12:06:58'),
(45,'PEMEROSES DATA TARUNA DAN ALUMNI',7,'FU3',1194,NULL,'2020-11-18 12:08:24','2020-11-18 12:08:27'),
(46,'PENGEVALUASI PROGRAM PERAKTEK KERJA',7,'FU3',1194,NULL,'2020-11-18 12:09:05','2020-11-18 12:09:07'),
(47,'PENYIAPAN PROGRAM PRAKTEK KERJA',7,'FU3',1194,NULL,'2020-11-18 12:10:14','2020-11-18 12:10:16'),
(48,'PENGELOLA KEPEGAWAIAN',7,'FU3',1194,NULL,'2020-11-18 12:10:19','2020-11-18 12:10:21'),
(49,'PENGELOLA KETATAUSAHAAN',7,'FU3',1194,NULL,'2020-11-18 12:11:03','2020-11-18 12:11:05'),
(50,'SEKRETARIS PIMPINAN I',7,'FU3',1194,NULL,'2020-11-18 12:11:36','2020-11-18 12:11:37'),
(51,'PENYUSUN BAHAN PUBLIKASI DAN KEHUMASAN',7,'FU3',1194,NULL,'2020-11-18 12:12:09','2020-11-18 12:12:11'),
(52,'PENGELOLA URUSAN KERUMAHTANGGAN',7,'FU3',1194,NULL,'2020-11-18 12:44:51','2020-11-18 12:44:53'),
(53,'PENGELOLA FASILITAS ASRAMA',7,'FU3',1194,NULL,'2020-11-18 12:45:23','2020-11-18 12:45:26'),
(54,'FASILITATOR TENAGA PENGAJAR PENDIDIKAN',7,'FU3',1194,NULL,'2020-11-18 12:45:57','2020-11-18 12:45:59'),
(55,'PENYUSUN PROGRAM PROSES BELAJAR MENGAJAR',7,'FU3',1194,NULL,'2020-11-18 12:46:42','2020-11-18 12:46:45'),
(56,'PERWIRA AKTIVITAS',7,'FU3',1194,NULL,'2020-11-18 12:47:20','2020-11-18 12:47:22'),
(57,'PENATA LAPORAN KAUANGAN III',7,'FU3',1194,NULL,'2020-11-18 12:48:05','2020-11-18 12:48:08'),
(58,'PEMEROSES ADMINISTRASI KEAUANGAN ',7,'FU3',1194,NULL,'2020-11-18 12:48:44','2020-11-18 12:48:46'),
(59,'PENYUSUN BAHAN EVALUASI DAN PELAPORAN',7,'FU3',1194,NULL,'2020-11-18 12:49:45','2020-11-18 12:49:48'),
(60,'PETUGAS LEGALISASI SERTIFIKAT KEPELAUTAN',7,'FU3',1194,NULL,'2020-11-18 12:50:34','2020-11-18 12:50:36'),
(61,'PENGEVALUASI DIKLAT',7,'FU3',1194,NULL,'2020-11-18 12:51:38','2020-11-18 12:51:40'),
(62,'PENGELOLA ADMINISTRASI PENGABDIAN',7,'FU3',1194,NULL,'2020-11-18 12:52:17','2020-11-18 12:52:19'),
(63,'PENGADMINISTRASI DATA BMN',7,'FU3',1194,NULL,'2020-11-18 12:52:55','2020-11-18 12:52:57'),
(64,'ARSIPARIS PENYELIA',7,'FT MAHIR',1175,NULL,'2020-11-18 12:55:04','2020-11-18 12:55:07'),
(65,'PEGAWAI BLU I',7,'BLU1',1151,NULL,'2020-11-18 12:56:11','2020-11-18 12:56:13'),
(66,'PENATA USAHA',6,'FU4',936,NULL,'2020-11-18 12:56:46','2020-11-18 12:56:48'),
(67,'SEKRETARIS PIMPINAN II',6,'FU4',936,NULL,'2020-11-18 12:59:07','2020-11-18 12:59:09'),
(68,'SEKRETARIS PIMPINAN III',6,'FU4',936,NULL,'2020-11-18 12:59:35','2020-11-18 12:59:38'),
(69,'PENGELOLA KERUMAHTANGAAN I',6,'FU4',936,NULL,'2020-11-18 13:00:16','2020-11-18 13:00:18'),
(70,'PENYUSUN RENCANA PEMELIHARAAN DAN PERBAIKAN',6,'FU4',936,NULL,'2020-11-18 13:00:57','2020-11-18 13:01:00'),
(71,'PENYUSUN BAHAN RENCANA PROGRAM EVALUASI OLAHRAGA',6,'FU4',936,NULL,'2020-11-18 14:28:14','2020-11-18 14:28:16'),
(72,'PENYUSUN BAHAN RENCANA PROGRAM EVALUASI SENI',6,'FU4',936,NULL,'2020-11-18 14:28:46','2020-11-18 14:28:48'),
(73,'PENYUSUN RENCANA PEMELIHARAAN DAN PERBAIKAN',6,'FU4',936,NULL,'2020-11-18 15:08:47','2020-11-18 15:08:50'),
(74,'TEKNISI RUANG PERAKTEK BAHASA',6,'FU4',936,NULL,'2020-11-18 15:09:24','2020-11-18 15:09:26'),
(75,'PERAWAT PELAKSANA LANJUTAN',6,'FT TERAMPIL',898,NULL,'2020-11-18 15:10:11','2020-11-18 15:10:13'),
(76,'PETUGAS REKAM MEDIS PELAKSANA',6,'FT PEMULA',777,NULL,'2020-11-18 15:11:03','2020-11-18 15:11:05'),
(77,'PRAWAT PELAKSANA',6,'FT PEMULA',777,NULL,'2020-11-18 15:11:32','2020-11-18 15:11:34'),
(78,'PENGINVENTARIS BARANG DAN ATK',6,'FU4',936,NULL,'2020-11-18 15:12:29','2020-11-18 15:12:32'),
(79,'PETUGAS REGISTRASI DIKLAT',6,'FU4',936,NULL,'2020-11-18 15:14:51','2020-11-18 15:14:53'),
(80,'PENGUMPUL DAN PENGOLAH DATA',6,'FU4',936,NULL,'2020-11-18 15:15:24','2020-11-18 15:15:26'),
(81,'PENYUSUN KERJASAMA',6,'FU4',936,NULL,'2020-11-18 15:15:46','2020-11-18 15:15:50'),
(82,'TEKNISI',6,'FU4',936,NULL,'2020-11-18 15:16:07','2020-11-18 15:16:09'),
(83,'PENGOLAH DATA DAN DOKUMENTASI PERPUSTAKAAN',6,'FU4',936,NULL,'2020-11-18 15:16:47','2020-11-18 15:16:50'),
(84,'PENGEVALUASI KEGIATAN TARUNA',6,'FU4',936,NULL,'2020-11-18 15:17:16','2020-11-18 15:17:17'),
(85,'PETUGAS ASRAMA DAN RUMAH DINAS',6,'FU4',936,NULL,'2020-11-18 15:17:45','2020-11-18 15:17:47'),
(86,'PETUGAS BIMBINGAN DAN KONSELING',6,'FU4',936,NULL,'2020-11-18 15:18:20','2020-11-18 15:18:23'),
(87,'PEGAWAI BLU 2',6,'BLU2',879,NULL,'2020-11-18 15:18:53','2020-11-18 15:18:55'),
(88,'PRAMU PUSTAKA',5,'FU5',771,NULL,'2020-11-18 15:19:19','2020-11-18 15:19:21'),
(89,'PENGELOLA KERUMAHTANGGAAN II',5,'FU5',771,NULL,'2020-11-18 15:20:20','2020-11-18 15:20:22'),
(90,'PENGADMINISTRASI UMUM',5,'FU5',771,NULL,'2020-11-18 15:20:49','2020-11-18 15:20:51'),
(91,'TENAGA MEDIS / DOKTER UMUM',4,'BLU4',708,NULL,'2020-11-18 15:21:34','2020-11-18 15:21:36'),
(92,'TENAGA MEDIS / APOTEKER',4,'BLU4',708,NULL,'2020-11-18 15:22:12','2020-11-18 15:22:15'),
(93,'PETUGAS K3',3,'BLU5',617,NULL,'2020-11-18 15:22:55','2020-11-18 15:22:57'),
(94,'PETUGAS KEAMANAN',3,'BLU5',617,NULL,'2020-11-18 15:23:26','2020-11-18 15:23:30'),
(95,'TENAGA TEKNISI',3,'BLU5',617,NULL,'2020-11-18 15:24:01','2020-11-18 15:24:03'),
(96,'TENAGA MEDIS / ANALIS KESEHATAN',3,'BLU5',617,NULL,'2020-11-18 15:24:46','2020-11-18 15:24:49'),
(97,'TENAGA ADMINISTRASI',2,'BLU6',572,NULL,'2020-11-18 15:25:19','2020-11-18 15:25:21'),
(98,'PEGAWAI BLU 7',1,'BLU7',415,NULL,'2020-11-18 15:25:41','2020-11-18 15:25:43');

/*Table structure for table `masa_kerja` */

DROP TABLE IF EXISTS `masa_kerja`;

CREATE TABLE `masa_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `masa_kerja` int(11) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `masa_kerja` */

insert  into `masa_kerja`(`id`,`masa_kerja`,`nilai`,`created_at`,`updated_at`) values 
(1,10,25,'2020-11-25 14:23:28','2020-11-25 14:23:28');

/*Table structure for table `skp` */

DROP TABLE IF EXISTS `skp`;

CREATE TABLE `skp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_skp_header` int(11) DEFAULT NULL,
  `indikator` varchar(200) DEFAULT NULL,
  `formula` varchar(200) DEFAULT NULL,
  `sumber` varchar(200) DEFAULT NULL,
  `periode` varchar(100) DEFAULT NULL,
  `bobot` int(11) DEFAULT NULL,
  `target_tahunan` int(11) DEFAULT NULL,
  `trajectory` int(11) DEFAULT NULL,
  `satuan` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `skp` */

insert  into `skp`(`id`,`id_skp_header`,`indikator`,`formula`,`sumber`,`periode`,`bobot`,`target_tahunan`,`trajectory`,`satuan`,`created_at`,`updated_at`) values 
(1,1,'Rasio Pendapatan BLU Terhadap Biaya Operasional','Pendapatan Operasional/Biaya Operasional x 100%','LRA','Bulanan',30,50,7,'Persentase','2020-11-27 15:16:47','2020-11-28 04:44:08');

/*Table structure for table `skp_header` */

DROP TABLE IF EXISTS `skp_header`;

CREATE TABLE `skp_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(32) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `status` int(1) DEFAULT 0,
  `created_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `skp_header` */

insert  into `skp_header`(`id`,`nip`,`bulan`,`tahun`,`status`,`created_at`,`created_by`,`updated_at`,`updated_by`,`deleted_at`,`deleted_by`) values 
(1,'3175050908970003','11',2020,0,'2020-11-27 15:15:43',3,'2020-11-27 15:15:43',NULL,NULL,NULL);

/*Table structure for table `t_jabatan` */

DROP TABLE IF EXISTS `t_jabatan`;

CREATE TABLE `t_jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jabatan_baru` int(11) NOT NULL,
  `tugas` varchar(200) DEFAULT NULL,
  `kuantitas` int(11) DEFAULT NULL,
  `keluaran` varchar(100) DEFAULT NULL,
  `kualitas` int(11) DEFAULT NULL,
  `waktu` int(11) DEFAULT NULL,
  `biaya` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`id_jabatan_baru`),
  KEY `id_jabatan_baru` (`id_jabatan_baru`),
  CONSTRAINT `t_jabatan_ibfk_1` FOREIGN KEY (`id_jabatan_baru`) REFERENCES `m_jabatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

/*Data for the table `t_jabatan` */

insert  into `t_jabatan`(`id`,`id_jabatan_baru`,`tugas`,`kuantitas`,`keluaran`,`kualitas`,`waktu`,`biaya`,`created_at`,`updated_at`) values 
(1,48,'Membuat Peta Jabatan',1,'DOKUMEN',100,60,NULL,'2020-11-19 03:59:02','2020-11-19 03:59:02'),
(2,48,'MEMBUAT STRUKTUR URAIAN TUGAS POKOK DAN FUNGSI SUB BAG TUK',1,'DOKUMEN',100,60,NULL,'2020-11-19 04:52:15','2020-11-19 04:52:15'),
(3,48,'MENGUPDATE DATA BASE PEGAWAI (SIK DAN SIJ)',1,'DOKUMEN',100,360,NULL,'2020-11-19 06:16:49','2020-11-19 06:16:49'),
(4,48,'MEMBUAT FORMASI KEBUTUHAN PEGAWAI',1,'DOKUMEN',100,120,NULL,'2020-11-19 13:18:19','2020-11-20 06:27:00'),
(5,48,'MENERAPKAN SKP PEGAWAI STIP',12,'DOKUMEN',100,360,NULL,'2020-11-19 13:19:12','2020-11-19 13:19:14'),
(6,48,'MENGELOLA ARSIP DATABASE PEGAWAI',1,'DOKUMEN',100,120,NULL,'2020-11-19 13:21:23','2020-11-19 13:21:25'),
(7,48,'MEMBUAT DAFTAR NOMINATIF, DUK DAN PETA KEKUATAN STIP',12,'DOKUMEN',100,180,NULL,'2020-11-19 13:22:35','2020-11-19 13:22:38'),
(8,1,'MELAKSANAKAN DAN PENGEMBANGAN PENDIDIKAN AKADEMIK DAN / ATAU VOKASI DI BIDANG ILMU PELAYARAN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 08:03:38','2020-11-19 08:03:38'),
(9,1,'MELAKSANAKAN PENELITIAN DAN PENGABDIAN KEPADA MASYARAKAT',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 15:04:32','2020-11-19 15:04:36'),
(10,1,'MELAKSANAKAN PEMBINAAN SIKAP MENTAL DAN MORAL SERTA KESAMAPTAAN TARUNA',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 15:05:59','2020-11-19 15:06:02'),
(11,1,'MENGELOLA SARANA, PRASARANA DAN UNIT PENUNJANG',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 15:07:02','2020-11-19 15:07:05'),
(12,1,'MENGELOLA KEUANGAN DAN ADMINISTRASI UMUM',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 15:07:32','2020-11-19 15:07:37'),
(13,1,'MENGELOLA ADMINISTRASI AKADEMIK, KETARUNAAN DAN ALUMNI',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 15:08:36','2020-11-19 15:08:38'),
(14,1,'MELAKSANAKAN PENGEMBANGAN USAHA',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 15:09:11','2020-11-19 15:09:13'),
(15,1,'MELAKSANAKAN PEMERIKSAAN INTERN DAN PENJAMINAN MUTU',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 15:09:47','2020-11-19 15:09:49'),
(16,1,'MELAKSANAKAN PEMBINAAN SIVITAS AKADEMIKA DAN HUBUNGAN DENGAN LINGKUNGAN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 15:10:31','2020-11-19 15:10:33'),
(17,46,'MENYUSUN DAN MEREVISI BUKU PANDUAN PERAKTEK DAN TRAINING RECORD BOOK',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 10:41:47','2020-11-19 10:41:47'),
(18,46,'MEMBUAT DAN MENGEVALUASI DAFTAR TARUNA PASCA PRALA / PRADA',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:42:53','2020-11-19 17:42:55'),
(19,46,'MENGEVALUASI HASIL PEKERJAAN BUKU PERAKTEK DAN TRAINING RECORD BOOK',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:43:35','2020-11-19 17:43:38'),
(20,46,'MENGEVALUASI KELENGKAPAN PERSYARATAN TARUNA PASCA PRALA / PRADA UNTUK MENGIKUTI SEMESTER BERIKUTNYA',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:45:43','2020-11-19 17:45:45'),
(21,46,'MEMBANTU PENYELESAIAN MASALAH YANG DIHADAPI TARUNA SELAMA MENGIKUTI PRAKTEK KERJA NYATA',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:46:43','2020-11-19 17:46:45'),
(22,46,'MEREKAPITULASI DATA TARUNA / MAHASISWA DAN PERUSAHAAN TEMPAT PERAKTEK KERJA NYATA',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:47:27','2020-11-19 17:47:29'),
(23,46,'MEMBUAT KONSEP USULAN PEMBERIAN SANKSI KEPADA TARUNA YANG BERMASALAH DAN TERLAMBAT TURUN PRALA / PRADA YANG DISEBABKAN OLEH TARUNA ITU SENDIRI',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:48:49','2020-11-19 17:48:52'),
(24,41,'MERENCANAKAN KEBUTUHAN DOSEN SESUAI DENGAN JUMLAH TARUNA DAN MATA KULIAH',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 10:51:21','2020-11-19 10:51:21'),
(25,41,'MENGUSULKAN RENCANA PENAMBAHAN DAN PENGEMBANGAN DOSEN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:52:07','2020-11-19 17:52:09'),
(26,41,'MEMBUAT SURAT PERINTAH TUGAS MENGAJAR DOSEN PERSEMESTER SETIAP MATA KULIH',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:52:59','2020-11-19 17:53:02'),
(27,41,'MENGUMPULKAN, MENGELOLAH DAN MENYAJIKAN BIODATA SERTA DATABASE DOSEN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:53:52','2020-11-19 17:53:55'),
(28,41,'MENGEVALUASI USULAN DARI JURUSAN MENGENAI DOSEN PENGAMPU MATA KULIAH SESUAI DENGAN KPMPETENSINYA',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:54:56','2020-11-19 17:54:58'),
(29,41,'MEMBUAT KONSEP USULAN PEMBERIAN PENGHARGAAN DAN SANKSI TERHADAP DOSEN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:55:48','2020-11-19 17:55:50'),
(30,41,'MEMBUAT SURAT TEGURAN KEPADA DOSEN YANG TIDAK MEMBUAT BEBAN TUGAS MENGAJAR (BTM) DAN TIDAK MELAKSANAKAN TUGAS MENGAJAR',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:57:20','2020-11-19 17:57:22'),
(31,41,'MEMBUAT SURAT UNDANGAN DAN PENGUMUMAN KEGIATAN AKADEMIK',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 17:57:55','2020-11-19 17:57:58'),
(32,42,'MENGEVALUASI JADWAL KULIAH, PRAKTEK DAN UJIAN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 11:01:53','2020-11-19 11:01:53'),
(33,42,'MEMBUAT KONSEP PERUBAHAN PEDOMAN PENDIDIKAN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 18:02:22','2020-11-19 18:02:24'),
(34,42,'MEMBUAT LAPORAN HASIL PELAKSANAAN PENDIDIKAN SESUAI EVALUASI PROGRAM SETUDI BERDASARKAN EVALUASI DIRI (EPSBED)',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 18:03:24','2020-11-19 18:03:27'),
(35,42,'MEMBUAT REKAPITUASI ABSENSI TARUNA SESUAI LAPORAN KETUA JURUSAN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 18:03:57','2020-11-19 18:03:59'),
(36,42,'MEMBUAT RENCANA PELAKSANAAN YUDISIUM SESUAI LAPORAN DARI JURUSAN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 18:04:42','2020-11-19 18:04:44'),
(37,42,'MELAKUKAN EVALUASI SELURUH KEGIATAN SUB BAGIAN ADMINISTRASI PENDIDIKAN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 18:05:19','2020-11-19 18:05:21'),
(38,42,'MELAKUKAN TUGAS LAIN YANG DIPERINTAHKAN ATASAN BAIK TERTULIS MAUPUN LISAN',NULL,'LAPORAN',NULL,NULL,NULL,'2020-11-19 18:06:03','2020-11-19 18:06:05');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id_users` int(11) NOT NULL AUTO_INCREMENT,
  `id_jabatan` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_estonian_ci DEFAULT NULL,
  `nip` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `nik` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `password` varchar(65) COLLATE utf8mb4_estonian_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `jenis_kelamin` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `karpeg` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `unit` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `kode_unit` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `kode_unit_lama` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `kantor` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `kode_kantor` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `kode_kantor_lama` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `subkantor` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `kode_subkantor` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `tmt_golongan` date DEFAULT NULL,
  `golongan` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `pangkat` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `kode_eselon` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `eselon` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `telepon` varchar(50) COLLATE utf8mb4_estonian_ci DEFAULT NULL,
  `tmt_cpns` date DEFAULT NULL,
  `tmt_pns` date DEFAULT NULL,
  `pendidikan_terakhir` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `foto` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

/*Data for the table `users` */

insert  into `users`(`id_users`,`id_jabatan`,`username`,`nip`,`nik`,`email`,`password`,`tanggal_lahir`,`tempat_lahir`,`nama`,`jenis_kelamin`,`karpeg`,`unit`,`kode_unit`,`kode_unit_lama`,`kantor`,`kode_kantor`,`kode_kantor_lama`,`subkantor`,`kode_subkantor`,`tmt_golongan`,`golongan`,`pangkat`,`kode_eselon`,`eselon`,`telepon`,`tmt_cpns`,`tmt_pns`,`pendidikan_terakhir`,`foto`,`updated_at`,`created_at`) values 
(1,48,'123123','3175050908970002','3175050908970002','luthfi@google.com','$2y$12$Awje5trE7G6reuDOCmJLqegw9.d4nvHWEYxU39EZDLkvoi3TEGsPu',NULL,NULL,'Luthfi Rhamadan',NULL,'002','STIP Jakarta','001','010','Perhubungan','020','030','Ditlala','011','2001-01-01','II','Pengelola Kepegawain','02','II','012 008884','2000-01-01','2001-01-01','Sarjana','1060131596.jpeg','2020-11-24 08:24:24',NULL),
(3,1,'3175050908970003','3175050908970003','3175050908970003','satrio@zonakreatif.com','$2y$12$MSA5hXiYxtLKOjVt0I4JZOu6uaoBIJP7ZLJ44akx1SxT6hKvp8lC.','1997-08-09','Jakarta','Satrio Wibisono','Laki - Laki','002','STIP Jakarta','001','010','Perhubungan','020','030','Ditlala','011','2001-01-01','I','Ketua','02','I','021 857749','2000-01-01','2001-01-01','Sarjana','1060131596.jpeg','2020-11-24 08:24:24',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
