/*
SQLyog Enterprise v12.5.1 (64 bit)
MySQL - 10.5.5-MariaDB : Database - sirebeka
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sirebeka` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sirebeka`;

/*Table structure for table `datapegawai` */

DROP TABLE IF EXISTS `datapegawai`;

CREATE TABLE `datapegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(100) NOT NULL,
  `ink` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(1) DEFAULT NULL,
  `karpeg` varchar(20) DEFAULT NULL,
  `unit` varchar(250) DEFAULT NULL,
  `kode_unit` varchar(100) DEFAULT NULL,
  `kode_unit_lama` varchar(50) DEFAULT NULL,
  `kantor` varchar(250) DEFAULT NULL,
  `kode_kantor` varchar(100) DEFAULT NULL,
  `kode_kantor_lama` varchar(50) DEFAULT NULL,
  `subkantor` varchar(250) DEFAULT NULL,
  `kode_subkantor` varchar(100) DEFAULT NULL,
  `tmt_golongan` date DEFAULT NULL,
  `golongan` varchar(50) DEFAULT NULL,
  `pangkat` varchar(50) DEFAULT NULL,
  `kode_eselon` varchar(50) DEFAULT NULL,
  `eselon` varchar(100) DEFAULT NULL,
  `jabatan_struktular` varchar(100) DEFAULT NULL,
  `tmt_jabatan_struktural` date DEFAULT NULL,
  `jabatan_fungsional` varchar(100) DEFAULT NULL,
  `tmt_jabatan_fungsional` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telepon` varchar(20) DEFAULT NULL,
  `tmt_cpns` date DEFAULT NULL,
  `tmt_pns` date DEFAULT NULL,
  `pendidikan_terakhir` varchar(200) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `datapegawai` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`nama`,`username`,`email`,`password`,`created_at`,`updated_at`) values 
(1,'satrio winbisono','3175050908970003','satrio@zonakreatif.id','$2y$10$FR.FrUQp1w9A8cSTdirRCOA98NZ9nySBjyi99s0HtqAMEhGelcKmK','2020-11-12 05:45:03','2020-11-12 05:45:03');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
