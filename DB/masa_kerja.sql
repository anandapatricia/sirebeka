/*
SQLyog Enterprise v12.5.1 (64 bit)
MySQL - 10.5.5-MariaDB : Database - sirebeka
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sirebeka` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sirebeka`;

/*Table structure for table `masa_kerja` */

DROP TABLE IF EXISTS `masa_kerja`;

CREATE TABLE `masa_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `masa_kerja` int(11) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `masa_kerja` */

insert  into `masa_kerja`(`id`,`masa_kerja`,`nilai`,`created_at`,`updated_at`) values 
(1,10,25,'2020-11-25 14:23:28','2020-11-25 14:23:28');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
