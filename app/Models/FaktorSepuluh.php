<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorSepuluh extends Model
{
    use HasFactory;

    
        protected $table = 'f10_probabilitas_resiko';
        protected $fillable = ['jenis', 'tipe', 'nilai', 'created_at', 'updated_at'];
}
