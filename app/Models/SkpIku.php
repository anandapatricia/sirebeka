<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SkpIku extends Model
{
    protected $table = "skp_iku";
    protected $fillable = ['nip', 'id_iku', 'tahun', 'created_at',  'created_by', 'updated_at', 'updated_by'];

    public function get_iku(){

        return $this->belongsTo('App\Models\Iku', 'id_iku');
    }
}
