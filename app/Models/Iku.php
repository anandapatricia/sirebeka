<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Iku extends Model
{
    protected $table = 'm_iku';
    protected $fillable = ['iku', 'created_at', 'updated_at'];
}
