<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SkpTugasTambahan extends Model
{
    protected $table = "skp_tugas_tambahan";
    protected $fillable = ['tugas_tambahan', 'nip', 'tahun', 'created_at',  'created_by', 'updated_at', 'updated_by'];

    
}
