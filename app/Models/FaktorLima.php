<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorLima extends Model
{
    use HasFactory;

        protected $table = 'f5_pedoman_keputusan';
        protected $fillable = ['jenis', 'tipe', 'nilai', 'created_at', 'updated_at'];
}
