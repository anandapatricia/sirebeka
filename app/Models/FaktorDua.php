<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorDua extends Model
{
    use HasFactory;

    protected $table = 'f2_manajerial';
    protected $fillable = ['jenis', 'tipe', 'nilai', 'created_at', 'updated_at'];
}
