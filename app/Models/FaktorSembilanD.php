<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorSembilanD extends Model
{
    use HasFactory;
    
        protected $table = 'f9_determinate';
        protected $fillable = ['jenis', 'tipe', 'nilai', 'created_at', 'updated_at'];
}
