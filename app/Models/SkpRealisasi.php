<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SkpRealisasi extends Model
{
    protected $table = 'skp_header_realisasi';

    public function getHeader(){
        return $this->belongsTo('App\Model\SkpHeader', 'id_skp_header');
    }

}
