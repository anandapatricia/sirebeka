<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Skp extends Model
{

    protected $table = 'skp';
    protected $fillable = ['indikator', 'formula', 'sumber', 'periode', 'bobot', 'trajectory', 'satuan', 'created_at', 'updated_at'];

    public function getHeader(){
        return $this->belongsTo('App\Model\SkpHeader', 'id_skp_header');
    }

   
}
