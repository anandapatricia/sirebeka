<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorNkh extends Model
{
    use HasFactory;

        protected $table = 'f78_nkh';
        protected $fillable = ['jenis', 'kelas', 'umum', 'standar', 'menengah', 'menengah tinggi', 'tinggi', 'created_at', 'updated_at'];
}
