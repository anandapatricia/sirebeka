<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Remunerasi extends Model
{
    use HasFactory;
    protected $table = 'remunerasi';
    protected $fillable = [ 'nama','jabatan','grade','nilai_jabatan','bobot_grade','bobot_absensi','gaji_maks','honorarium','insentif_kinerja','insentif_kinerja_diterima','total_nilai_skp','jumlah_remunerasi','pph','pph_jumlah','jumlah_diterima','tahun','bulan'];
}
