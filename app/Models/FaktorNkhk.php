<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorNkhk extends Model
{
    use HasFactory;

        protected $table = 'f78_nkhk';
        protected $fillable = ['jenis', 'kelas', 'r', 'k', 'b', 'u', 'created_at', 'updated_at'];
}
