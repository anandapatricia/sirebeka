<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'm_grade';
    protected $fillable = ['grade', 'persentase', 'created_at', 'updated_at'];
}
