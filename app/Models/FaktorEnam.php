<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorEnam extends Model
{
    use HasFactory;

        protected $table = 'f6_kondisi_kerja';
        protected $fillable = ['jenis', 'tipe', 'kode', 'kelas', 'nilai', 'created_at', 'updated_at'];
}
