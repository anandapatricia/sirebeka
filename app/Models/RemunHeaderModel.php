<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RemunHeaderModel extends Model
{
    use HasFactory;
    protected $table = 'remunerasi_header';
    protected $guarded = [];
}
