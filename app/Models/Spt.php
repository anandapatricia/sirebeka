<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spt extends Model
{
    protected $table = 'spt';
    protected $fillable = [ 'nama', 'nip', 'pangkat', 'golongan', 'jabatan', 'tanggal_surat', 'nomor_surat', 'tanggal_mulai', 'tanggal_selesai', 'lokasi_spt', 'uraian', 'created_at', 'updated_at'];

}
