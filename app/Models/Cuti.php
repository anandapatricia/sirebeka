<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    protected $table = 'cuti';
    protected $fillable = ['id','nama', 'nip', 'jabatan', 'masakerja', 'unitkerja', 'jeniscuti', 'alasan', 'lamacuti', 'alamat' , 'created_at', 'updated_at'];
}
