<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorNkhm extends Model
{
    use HasFactory;

        protected $table = 'f78_nkhm';
        protected $fillable = ['jenis', 'kelas', 'r', 'k', 'b', 'u', 'created_at', 'updated_at'];
}
