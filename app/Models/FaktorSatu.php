<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorSatu extends Model
{
    use HasFactory;

        protected $table = 'f1_kompetensi_teknis';
        protected $fillable = ['jenis', 'tipe', 'nilai', 'created_at', 'updated_at'];
}
