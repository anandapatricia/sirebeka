<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PirModel extends Model
{
    use HasFactory;

    protected $table = 'm_pir';
    protected $fillable = ['tahun', 'proyeksi_pnbp', 'penyetaraan','bpjs_non_pns','uang_makan_non_pns','honorarium_ketua','honorarium_anggota','honorarium_sekertaris','created_at', 'updated_at'];
}
