<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportAbsensiModel extends Model
{
    use HasFactory;

    protected $table = 'absensi';
    protected $guarded = [];

    public function getNIP(){
        return $this->belongsTo('App\Model\User', 'nip');
    }
}
