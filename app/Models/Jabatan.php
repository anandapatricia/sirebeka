<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'm_jabatan';
    protected $fillable = ['jabatan_baru', 'grade_baru', 'kelas_jabatan', 'nilai_jabatan', 'gapok', 'created_at', 'updated_at'];
}
