<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorEmpat extends Model
{
    use HasFactory;

        protected $table = 'f4_analisis_lingkungan_pekerjaan';
        protected $fillable = ['jenis', 'kelas', 'nilai', 'created_at', 'updated_at'];
}
