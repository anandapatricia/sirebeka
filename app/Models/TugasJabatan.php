<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TugasJabatan extends Model
{
    protected $table = 't_jabatan';
    protected $fillable = ['id_jabatan_baru','tugas', 'kuantitas', 'keluaran', 'kualitas', 'waktu', 'biaya', 'created_at', 'updated_at'];
}
