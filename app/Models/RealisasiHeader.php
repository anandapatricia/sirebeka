<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RealisasiHeader extends Model
{
    protected $table = 'skp_header_realisasi';
}
