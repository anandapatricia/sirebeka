<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPegawai extends Model
{
    use HasFactory;
    protected $table = 'users';
    protected $guarded = [];

    public function jabatan_pegawai(){
        return $this->belongsTo('App\Models\Jabatan','jabatan_id');
    }
}
