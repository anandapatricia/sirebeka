<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaktorSembilanI extends Model
{
    use HasFactory;

        protected $table = 'f9_indeterminate';
        protected $fillable = ['jenis', 'tipe', 'nilai', 'created_at', 'updated_at'];
}
