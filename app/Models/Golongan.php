<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Golongan extends Model
{
    protected $table = 'm_golongan';
    protected $fillable = ['jenis', 'golongan', 'persen', 'created_at', 'updated_at'];
}
