<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Masakerja extends Model
{
    protected $table = 'masa_kerja';
    protected $fillable = ['masa_kerja', 'nilai', 'created_at', 'updated_at'];
}
