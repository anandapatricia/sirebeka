<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SkpHeader extends Model
{
    protected $table = 'skp_header';
    public function getNIP()
    {
        return $this->belongsTo('App\Model\SkpRealisasi', 'nip');
    }
}
