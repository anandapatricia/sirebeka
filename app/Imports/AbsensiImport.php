<?php

namespace App\Imports;

use App\Models\ImportAbsensiModel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AbsensiImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($row);
        return new ImportAbsensiModel([
            'nip'   =>  $row[2],
            'nama'  =>  str_replace(',','',$row[3]),
            'auto_assign'   =>  $row[4],
            'tanggal_absen' =>  $row[5],
            'timetable' =>   $row[6],
            'onduty'    =>  $row[7],
            'offduty'   => $row[8],
            'clockin'   => $row[9],
            'clockout'  =>    $row[10],
            'normal'    =>  $row[11],
            'realtime'  =>    $row[12],
            'late'  =>    $row[13],
            'eary'  =>    $row[14],
            'absent'    =>  $row[15],
            'ottime'    =>  $row[16],
            'worktime'  =>    $row[17],
            'exception' =>   $row[18],
            'mustin'    =>  $row[19],
            'mustout'   => $row[20],
            'department'    =>  $row[21],
            'ndays' =>   $row[22],
            'weekend'   => $row[23],
            'holiday'   => $row[24],
            'att_time'  =>    $row[25],
            'ndays_ot'  =>    str_replace(',','.',$row[26]),
            'weekend_ot'    =>  $row[27],
            'holiday_it'    =>  $row[28],
            ]);
    }
}
