<?php

namespace App\Http\Controllers;
use App\Models\Cuti;

use Illuminate\Http\Request;

class CutiController extends Controller
{

    public function table_pengajuan(Request $request){
        $list_cuti = Cuti::where('nip', auth()->user()->nip)->get();
        return view('cuti.table_pengajuan',compact('list_cuti'));
    }

     public function form_pengajuan(Request $request){
        return view('cuti.form_pengajuan');
    }

    public function pengajuan_save(Request $request)
    {
        // dd(auth()->user()->nip);
        // dd($request->input())
        $tmt_pns = new \DateTime(auth()->user()->tmt_cpns);
        $today = new \DateTime(date("Y-m-d"));
        $interval = date_diff($today, $tmt_pns);
        $cuti = new Cuti;
        $cuti->nama = auth()->user()->nama;
        $cuti->nip = auth()->user()->nip;
        $cuti->jabatan = !empty(auth()->user()->jabatan_struktural) ? auth()->user()->jabatan_struktural : auth()->user()->jabatan_fungsional;
        $tmt_pns = new \DateTime(auth()->user()->tmt_cpns);
        $today = new \DateTime(date("Y-m-d"));
        $interval = date_diff($today, $tmt_pns);
        $masa_kerja = $interval->format("%y tahun %m bulan %d hari");
        $cuti->masakerja = $masa_kerja;
        $cuti->unitkerja = auth()->user()->unit;

        $cuti->tanggalcuti = $request->input('tanggalcuti');
        $cuti->tanggal =  $request->input('tanggal');
        $cuti->jeniscuti = $request->input('jeniscuti');
        $cuti->alasan = $request->input('alasan');
        $cuti->alamat = $request->input('alamat');
        $cuti->save();

        return redirect()->route('table_pengajuan');
    }

    public function edit_pengajuan($id){
        $cuti = Cuti::find($id);
        return view('cuti.edit_pengajuan',compact('cuti'));
    }

    public function pengajuan_update(Request $request, $id)
    {
        $cuti= Cuti::find($id);
        $cuti->nama = $request->input('nama');
        $cuti->nip = $request->input('nip');
        $cuti->jabatan = $request->input('jabatan');
        $cuti->masakerja = $request->input('masakerja');
        $cuti->unitkerja = $request->input('unitkerja');
        $cuti->tanggalcuti = $request->input('tanggalcuti');
        $cuti->tanggal = $request->input('tanggal');
        $cuti->jeniscuti = $request->input('jeniscuti');
        $cuti->alasan = $request->input('alasan');
        $cuti->alamat = $request->input('alamat');
        $cuti->save();

        return redirect()->route('table_pengajuan');
    }

    public function view_pengajuan($id){
        $cuti = Cuti::find($id);
        return view('cuti.view_pengajuan',compact('cuti'));
    }

    public function cuti_kirim($id){
        $cuti = Cuti::find($id);
        $cuti->status = 1;
        $cuti->save();
        // dd($cuti);
        return redirect()->route('table_pengajuan');
    }

    public function cuti_setuju($id){
        $cuti = Cuti::find($id);
        $cuti->status = 2;
        $cuti->save();
        // dd($cuti);
        return redirect()->route('dashboard')->with('setuju','data '.$cuti->tanggalcuti.' disetujui');;
    }

    public function cuti_tolak($id){
        $cuti = Cuti::find($id);
        $cuti->status = 3;
        $cuti->save();
        // dd($cuti);
        return redirect()->route('dashboard')->with('tolak','data '.$cuti->tanggalcuti.' ditolak');;
    }

    public function delete_pengajuan($id){
        $cuti = Cuti::find($id);
        $tanggalcuti = $cuti->tanggal_cuti;
        $cuti->delete();
        return redirect()->route('table_pengajuan')->with('notif','data '.$tanggalcuti.' berhasil di hapus');
    }

}
