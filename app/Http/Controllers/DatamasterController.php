<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//panggil model class
use App\Models\FaktorSatu;
use App\Models\FaktorDua;
use App\Models\FaktorTiga;
use App\Models\FaktorEmpat;
use App\Models\FaktorLima;
use App\Models\FaktorEnam;
use App\Models\FaktorSepuluh;

class DatamasterController extends Controller
{

    //faktorsatu

    public function f1_kompetensi_teknis_table(Request $request){
        $f1_kompetensi_teknis = FaktorSatu::all();
        return view('faktorsatu.f1_kompetensi_teknis_table',compact('f1_kompetensi_teknis'));
    }

     public function f1_kompetensi_teknis(Request $request){
        return view('faktorsatu.f1_kompetensi_teknis');
    }

    public function f1_kompetensi_teknis_save(Request $request)
    {
        $f1_kompetensi_teknis = new FaktorSatu();
        $f1_kompetensi_teknis->jenis = $request->input('jenis');
        $f1_kompetensi_teknis->tipe = $request->input('tipe');
        $f1_kompetensi_teknis->nilai = $request->input('nilai');
        $f1_kompetensi_teknis->save();
    
        return redirect()->route('f1_kompetensi_teknis_table');
    }

    public function f1edit($id){
        $f1_kompetensi_teknis = FaktorSatu::find($id);
        return view('faktorsatu.f1edit',compact('f1_kompetensi_teknis'));
    }

    public function f1update(Request $request, $id)
    {
        $f1_kompetensi_teknis= FaktorSatu::find($id);
        $f1_kompetensi_teknis->jenis = $request->input('jenis');
        $f1_kompetensi_teknis->tipe = $request->input('tipe');
        $f1_kompetensi_teknis->nilai = $request->input('nilai');
        $f1_kompetensi_teknis->save();
    
        return redirect()->route('f1_kompetensi_teknis_table');
    }

    //faktordua

    public function f2table(Request $request){
        $f2 = FaktorDua::all();
        return view('faktordua.f2table',compact('f2'));
    }

    public function f2(Request $request){
        return view('faktordua.f2');
    }   

    public function f2_save(Request $request)
    {
        $f2 = new FaktorDua();
        $f2->jenis = $request->input('jenis');
        $f2->kelas = $request->input('kelas');
        $f2->tipe = $request->input('tipe');
        $f2->lingkup = $request->input('lingkup');
        $f2->nilai = $request->input('nilai');
        $f2->save();
    
        return redirect()->route('f2table');
    }

    public function f2edit($id){
        $f2 = FaktorDua::find($id);
        return view('faktordua.f2edit',compact('f2'));
    }

    public function f2update(Request $request, $id)
    {
        $f2= FaktorDua::find($id);
        $f2->jenis = $request->input('jenis');
        $f2->kelas = $request->input('kelas');
        $f2->tipe = $request->input('tipe');
        $f2->lingkup = $request->input('lingkup');
        $f2->nilai = $request->input('nilai');
        $f2->save();
    
        return redirect()->route('f2table');
    }

    //faktortiga

    public function f3table(Request $request){
        $f3 = FaktorTiga::all();
        return view('faktortiga.f3table',compact('f3'));
    }

     public function f3(Request $request){
        return view('faktortiga.f3');
    }

    public function f3_save(Request $request)
    {
        $f3 = new FaktorTiga();
        $f3->jenis = $request->input('jenis');
        $f3->kelas = $request->input('kelas');
        $f3->nilai = $request->input('nilai');
        $f3->save();
    
        return redirect()->route('f3table');
    }

    public function f3edit($id){
        $f3 = FaktorTiga::find($id);
        return view('faktortiga.f3edit',compact('f3'));
    }

    public function f3update(Request $request, $id)
    {
        $f3= FaktorTiga::find($id);
        $f3->jenis = $request->input('jenis');
        $f3->kelas = $request->input('kelas');
        $f3->nilai = $request->input('nilai');
        $f3->save();
    
        return redirect()->route('f3table');
    }

    //faktorempat

    public function f4table(Request $request){
        $f4 = FaktorEmpat::all();
        return view('faktorempat.f4table',compact('f4'));
    }

     public function f4(Request $request){
        return view('faktorempat.f4');
    }

    public function f4_save(Request $request)
    {
        $f4 = new FaktorEmpat();
        $f4->jenis = $request->input('jenis');
        $f4->kelas = $request->input('kelas');
        $f4->nilai = $request->input('nilai');
        $f4->save();
    
        return redirect()->route('f4table');
    }

    public function f4edit($id){
        $f4 = FaktorEmpat::find($id);
        return view('faktorempat.f4edit',compact('f4'));
    }

    public function f4update(Request $request, $id)
    {
        $f4= FaktorEmpat::find($id);
        $f4->jenis = $request->input('jenis');
        $f4->kelas = $request->input('kelas');
        $f4->nilai = $request->input('nilai');
        $f4->save();
    
        return redirect()->route('f4table');
    }

    //faktorlima

    public function f5table(Request $request){
        $f5 = FaktorLima::all();
        return view('faktorlima.f5table',compact('f5'));
    }

     public function f5(Request $request){
        return view('faktorlima.f5');
    }

    public function f5_save(Request $request)
    {
        $f5 = new FaktorLima();
        $f5->jenis = $request->input('jenis');
        $f5->tipe = $request->input('tipe');
        $f5->nilai = $request->input('nilai');
        $f5->save();
    
        return redirect()->route('f5table');
    }

    public function f5edit($id){
        $f5 = FaktorLima::find($id);
        return view('faktorlima.f5edit',compact('f5'));
    }

    public function f5update(Request $request, $id)
    {
        $f5= FaktorLima::find($id);
        $f5->jenis = $request->input('jenis');
        $f5->tipe = $request->input('tipe');
        $f5->nilai = $request->input('nilai');
        $f5->save();
    
        return redirect()->route('f5table');
    }

    //faktorenam

    public function f6table(Request $request){
        $f6 = FaktorEnam::all();
        return view('faktorenam.f6table',compact('f6'));
    }

     public function f6(Request $request){
        return view('faktorenam.f6');
    }

    public function f6_save(Request $request)
    {
        $f6 = new FaktorEnam();
        $f6->jenis = $request->input('jenis');
        $f6->tipe = $request->input('tipe');
        $f6->kode = $request->input('kode');
        $f6->kelas = $request->input('kelas');
        $f6->nilai = $request->input('nilai');
        $f6->save();
    
        return redirect()->route('f6table');
    }

    public function f6edit($id){
        $f6 = FaktorEnam::find($id);
        return view('faktorenam.f6edit',compact('f6'));
    }

    public function f6update(Request $request, $id)
    {
        $f6= FaktorEnam::find($id);
        $f6->jenis = $request->input('jenis');
        $f6->tipe = $request->input('tipe');
        $f6->kode = $request->input('kode');
        $f6->kelas = $request->input('kelas');
        $f6->nilai = $request->input('nilai');
        $f6->save();
    
        return redirect()->route('f6table');
    }

    //faktorsepuluh

    public function f10table(Request $request){
    $f10 = FaktorSepuluh::all();
    return view('faktorsepuluh.f10table',compact('f10'));
    }

    public function f10(Request $request){
        return view('faktorsepuluh.f10');
    }

    public function f10_save(Request $request)
    {
        $f10 = new FaktorSepuluh();
        $f10->jenis = $request->input('jenis');
        $f10->tipe = $request->input('tipe');
        $f10->nilai = $request->input('nilai');
        $f10->save();
    
        return redirect()->route('f10table');
    }

    public function f10edit($id){
        $f10 = FaktorSepuluh::find($id);
        return view('faktorsepuluh.f10edit',compact('f10'));
    }

    public function f10update(Request $request, $id)
    {
        $f10= FaktorSepuluh::find($id);
        $f10->jenis = $request->input('jenis');
        $f10->tipe = $request->input('tipe');
        $f10->nilai = $request->input('nilai');
        $f10->save();
    
        return redirect()->route('f10table');
    }
}
