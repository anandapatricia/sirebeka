<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Grade;

class masterGradeController extends Controller
{
    public function mgrade()
    {
        $grade = Grade::all();
        return view('datagrade.tabel-grade',compact('grade'));
    }

    public function formgrade(Request $request){
        return view('datagrade.form-grade');
    }

    
    public function savegrade(Request $request)
    {
        $grade = new Grade();
        $grade->grade = $request->input('grade');
        $grade->persentase = $request->input('persentase');
        $grade->save();

        return redirect('masterGrade');
    }

    public function edit($id)
    {
        $grade = Grade::find($id);
        return view('datagrade.edit-grade',compact('grade','id'));
    }

    public function update(Request $request, $id)
    {
        $grade = Grade::find($id);
        $grade->grade = $request->get('grade');
        $grade->persentase = $request->get('persentase');
        $grade->save();

        return redirect('masterGrade');
    }
}
