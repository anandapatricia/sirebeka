<?php

namespace App\Http\Controllers;
use App\Models\Cuti;
use App\Models\User;
use App\Models\Skp;
use App\Models\Spt;
use App\Models\RemunHeaderModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $list_cuti = Cuti::where('status',1)->orderBy('id','desc')->take(5)->get();
        $list_spt = Spt::where('status',1)->orderBy('id','desc')->take(5)->get();
        $list_skp = Skp::where('tahun',2020)->orderBy('id','desc')->take(5)->get();
        $jumlah_pns = User::where('role',3)->where('status_pns',1)->count();
        $jumlah_blu = User::where('role',3)->where('status','BLU')->count();
        $jumlah_honor = User::where('role',3)->where('status','HONOR')->count();

        $total_remun = RemunHeaderModel::where('tahun',date("Y"))->sum('total_diterima');
        $total_remun_jan = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','01')->sum('total_diterima');
        $total_remun_feb = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','02')->sum('total_diterima');
        $total_remun_mar = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','03')->sum('total_diterima');
        $total_remun_apr = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','04')->sum('total_diterima');
        $total_remun_mei = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','05')->sum('total_diterima');
        $total_remun_jun = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','06')->sum('total_diterima');
        $total_remun_jul = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','07')->sum('total_diterima');
        $total_remun_ags = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','08')->sum('total_diterima');
        $total_remun_sep = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','09')->sum('total_diterima');
        $total_remun_okt = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','10')->sum('total_diterima');
        $total_remun_nov = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','11')->sum('total_diterima');
        $total_remun_des = RemunHeaderModel::where('tahun',date("Y"))->where('bulan','12')->sum('total_diterima');

        // dd($total_remun_nov);
        $data_pendapatan = $this->__getPendapatan();
        $total_pendapatan = "tidak terhubung ke keuangan";
        if($data_pendapatan->status){
            $total_pendapatan = $data_pendapatan->data->total_pendapatan;
        }

        $persentase = round(($total_remun/$total_pendapatan)*100,2);

        // $persentase = 30;
        return view('dashboard.dashboardv1', compact(
            'list_cuti','jumlah_pns','jumlah_blu','jumlah_honor','list_spt',
            'total_pendapatan','total_remun','persentase','total_remun_jan','total_remun_feb','total_remun_mar','total_remun_apr','total_remun_mei','total_remun_jun','total_remun_jul','total_remun_ags','total_remun_sep','total_remun_okt','total_remun_nov','total_remun_des'
        ));
    }

    private function __getPendapatan(){
        $url = "http://103.85.63.254/api_keuangan/api/Pendapatan/";
        $post = [
            'sec_key'=>'c161c72ac4d39247f62a86c76b6b94df'
        ];

        $request = http_build_query($post);
        // dd($request);
        $headers = [
            "Content-Type:multipart/form-data",
            "User-Agent:SiReBeKa"
        ];

        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $url.'?'.$request,
            CURLOPT_HEADER => false,
            CURLOPT_POST => false,
            CURLOPT_HTTPHEADER => $headers,
            // CURLOPT_POSTFIELDS => $post,
            CURLOPT_USERPWD => "@piSt1P:keu4n64n",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_AUTOREFERER => true
        );
        // cURL options
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);

        curl_close($ch);
        $data = json_decode($response);
        // dd($data);
        return $data;
    }

}
