<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Masakerja;

class MasterMasaKerjaController extends Controller
{
    public function mmasakerja()
    {
        $masakerja = Masakerja::all();
        return view('masakerja.table-masakerja',compact('masakerja'));
    }

    public function formmasakerja(Request $request){
        return view('masakerja.form-masakerja');
    }

    
    public function savemasakerja(Request $request)
    {
        $masakerja = new Masakerja();
        $masakerja->masa_kerja = $request->input('masa_kerja');
        $masakerja->nilai = $request->input('nilai');
        $masakerja->save();

        return redirect('masterMasaKerja');
    }

    public function edit($id)
    {
        $masakerja = Masakerja::find($id);
        return view('masakerja.edit-masakerja',compact('masakerja','id'));
    }

    public function update(Request $request, $id)
    {
        $masakerja = Masakerja::find($id);
        $masakerja->masa_kerja = $request->input('masa_kerja');
        $masakerja->nilai = $request->input('nilai');
        $masakerja->save();

        return redirect('masterMasaKerja');
    }
}
