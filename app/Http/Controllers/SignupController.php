<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//panggil model class
use App\Models\User;

class SignupController extends Controller
{
    // untuk memanggil view register
    public function register(Request $request){
        return view('sessions.signUp');
    } 
    
    // untuk melakukan proses penyimpanan data registrasi user
    public function register_save(Request $request){
        $dataUser = User::where('username', $request->input('username'))->first();
        if(empty($dataUser)){
            $dataUser = new User;
            $dataUser->nama = $request->input('name');
            $dataUser->username = $request->input('username');
            $dataUser->email = $request->input('email');
            $dataUser->password = bcrypt($request->input('password'));
            $dataUser->save();
        }
        
//        dd($dataUser);
        return redirect()->route('signIn');
    }
}

