<?php

namespace App\Http\Controllers;
use App\Models\Skp;
use App\Models\SkpHeader;
use App\Models\TugasJabatan;
use App\Models\Iku;
use App\Models\SkpIku;
use App\Models\SkpTugasTambahan;
use App\Models\SkpRealisasi;
use App\Models\RealisasiHeader;
use Illuminate\Http\Request;
use DB;
class SkpController extends Controller
{
    public function index(Request $request){

        $skp = SkpHeader::where('created_by', auth()->user()->id_users)->get();
        // dd($skp);
        return view('skp.tabel-skp',compact('skp'));
    }

    private function create_skp_header(){
        $skpheader = SkpHeader::where('created_by', auth()->user()->id_users)->where('tahun', date('Y'))->first();
        if(!empty($skpheader)){
            return $skpheader->id;
        }
        $skpheader = new SkpHeader;
        $skpheader->nip = auth()->user()->nip;
        $skpheader->tahun = date('Y');
        $skpheader->created_by = auth()->user()->id_users;
        $skpheader->save();

        return $skpheader->id;
    }

    // ini tempat tampilan utama
    public function detailskp($id,Request $request){
        $form_skp = new Skp;
        $header = SkpHeader::find($id);
        $skp = Skp::where('id_skp_header', $id)->get();

        $data = [];
        foreach($skp as $value){
            $data[$value->jenis_indikator][]=$value;
        }
        return view('skp.form-skp',compact('skp', 'header','form_skp', 'data'));
    }

    public function editskp($id_header, $id,Request $request){
        $form_skp = Skp::find($id);
        $header = SkpHeader::find($id_header);
        $skp = Skp::where('id_skp_header', $id_header)->get();
        return view('skp.form-skp',compact('skp', 'header', 'form_skp', 'id_header'));
    }

    public function saveskp(Request $request)
    {
        $id_header = $this->create_skp_header();
        // dd($id_header);
        $post = $request->input('data');
        // dd($post);
        for($nomor=0; $nomor < count($post['jenis_indikator']); $nomor++){
            $skp = new Skp();
            $skp->nip = auth()->user()->nip;
            $skp->id_skp_header = $id_header;
            $skp->tahun = date('Y');
            $skp->jenis_indikator = $post['jenis_indikator'][$nomor];
            $skp->indikator = $post['indikator'][$nomor];
            $skp->formula = $post['formula'][$nomor];
            $skp->sumber = $post['sumber'][$nomor];
            $skp->periode = $post['periode'][$nomor];
            $skp->bobot = $post['bobot'][$nomor];
            $skp->target_tahunan = $post['target_tahunan'][$nomor];
            $skp->trajectory = $post['trajectory'][$nomor];
            $skp->satuan = $post['satuan'][$nomor];
            $skp->id_indikator = $post['id_indikator'][$nomor];
            $skp->uraian = $post['uraian'][$nomor];
            $skp->save();
        }
        
        return redirect()->route('skp');
    }

    public function kirimskp($id, Request $request)
    {
        $skpheader = SkpHeader::find($id);
        $skpheader->status = 1;
        $skpheader->save();

        return redirect()->route('skp');
    }

    public function buatskp(Request $request){
        $tugasjabatan = TugasJabatan::where('id_jabatan_baru', auth()->user()->id_jabatan)->get();
        $iku = Iku::where('tahun', date('Y'))->get()->pluck('iku','id');
        $skp_iku = SkpIku::where('tahun', date('Y'))->where('nip', auth()->user()->nip)->get();
        $skp_tugas_tambahan = SkpTugasTambahan::where('tahun', date('Y'))->where('nip', auth()->user()->nip)->get();

        // dd($skp_iku);
        $form_skp = new Skp;
        $header = new SkpHeader;

        // dd($tugasjabatan);
        return view('skp.buat-skp',compact('header', 'form_skp', 'iku', 'skp_iku', 'tugasjabatan','skp_tugas_tambahan'));
    }

    public function buat_iku(Request $request){
        $iku = Iku::where('tahun', date('Y'))->get()->pluck('iku','id');
        $form_skp = new Skp;
        $header = new SkpHeader;

        // dd($tugasjabatan);
        return view('skp.buat-iku',compact('header', 'iku', 'form_skp'));
    }

    public function skp_iku_save(Request $request){
        $id_iku = $request->input('id_iku');
        $skp_iku = new SkpIku;
        $skp_iku->nip = auth()->user()->nip;
        $skp_iku->id_iku = $id_iku;
        $skp_iku->created_by = auth()->user()->id_users;
        $skp_iku->updated_by = auth()->user()->id_users;
        $skp_iku->tahun = date('Y');
        $skp_iku->save();

        return redirect()->route('buat.skp');
    }

    public function skp_tugas_tambahan(Request $request){
        $id = $request->input('id');
        $tugas_tambahan = $request->input('tugas_tambahan');
        $skp_tugas_tambahan = new SkpTugasTambahan;
        $skp_tugas_tambahan->nip = auth()->user()->nip;
        $skp_tugas_tambahan->tugas_tambahan = $tugas_tambahan;
        $skp_tugas_tambahan->created_by = auth()->user()->id_users;
        $skp_tugas_tambahan->updated_by = auth()->user()->id_users;
        $skp_tugas_tambahan->tahun = date('Y');
        $skp_tugas_tambahan->save();

        return redirect()->route('buat.skp');
    }

    public function buat_realisasi($id_skp_header, Request $request){
        $header = SkpHeader::find($id_skp_header);
        $tugasjabatan = TugasJabatan::where('id_jabatan_baru', auth()->user()->id_jabatan)->get();
        $iku = Iku::where('tahun', $header->tahun)->get()->pluck('iku','id');
        $skp_iku = SkpIku::where('tahun', $header->tahun)->where('nip', auth()->user()->nip)->get();
        $skp_tugas_tambahan = SkpTugasTambahan::where('tahun',$header->tahun)->where('nip', auth()->user()->nip)->get();

        $form_skp = Skp::where('id_skp_header',$id_skp_header)->get();
        $data = [];
        foreach($form_skp as $item){
            $data[$item->jenis_indikator][] = $item;
        }

        return view('skp.buat-realisasi',compact('header', 'form_skp', 'iku', 'skp_iku', 'tugasjabatan','skp_tugas_tambahan','data'));
    }

    private function create_realisasi_header(){
        $realisasi_header = RealisasiHeader::where('created_by', auth()->user()->id_users)->where('bulan', date('m'))->where('tahun', date('Y'))->first();
        if(!empty($realisasi_header)){
            return $realisasi_header->id;
        }
        $realisasi_header = new RealisasiHeader;
        $realisasi_header->nip = auth()->user()->nip;
        $realisasi_header->bulan = date('m');
        $realisasi_header->tahun = date('Y');
        $realisasi_header->indesk_ril;
        $realisasi_header->indesk_proporsional;
        $realisasi_header->indesk_tambahan;
        $realisasi_header->created_by = auth()->user()->id_users;
        $realisasi_header->save();

        return $realisasi_header->id;
    }

    public function save_realisasi(Request $request){
        $skp_realisasi = new SkpRealisasi;
        $skp_realisasi->nip = auth()->user()->nip;
        $skp_realisasi->created_by = auth()->user()->id_users;
        $skp_realisasi->updated_by = auth()->user()->id_users;
        $skp_realisasi->tahun = date('Y');
        $skp_realisasi->save();

        return redirect()->route('buat.skp');
    }

    public function save_realisasi_skp(Request $request)
    {
        $id_header = $this->create_realisasi_header();
        $post = $request->input('data');
        
        $total_realisasi = 0;
        $total_bobot = 0;
        $total_proporsioal = 0;
        $total_tambahan = 0;
        $total_proporsioal_tambahan = 0;
        $total_realisasi_terhadap_target = 0;

        for($nomor=0; $nomor < count($post['jenis_indikator']); $nomor++){
            $skp_realisasi = new SkpRealisasi();
            $skp_realisasi->nip = auth()->user()->nip;
            $skp_realisasi->id_skp_header = $id_header;
            $skp_realisasi->tahun = date('Y');
            $skp_realisasi->jenis_indikator = $post['jenis_indikator'][$nomor];
            $skp_realisasi->id_indikator = $post['id_indikator'][$nomor];
            $skp_realisasi->indikator = $post['indikator'][$nomor];
            $skp_realisasi->formula = $post['formula'][$nomor];
            $skp_realisasi->sumber = $post['sumber'][$nomor];
            $skp_realisasi->periode = $post['periode'][$nomor];
            $skp_realisasi->bobot = $post['bobot'][$nomor];
            $skp_realisasi->target_tahunan = $post['target_tahunan'][$nomor];
            $skp_realisasi->trajectory = $post['trajectory'][$nomor];
            $skp_realisasi->satuan = $post['satuan'][$nomor];
            $skp_realisasi->id_indikator = $post['id_indikator'][$nomor];
            $skp_realisasi->uraian = $post['uraian'][$nomor];
            $skp_realisasi->realisasi = $post['realisasi'][$nomor];

            if($post['trajectory'][$nomor] == 0){
                $skp_realisasi->realisasi_target = 0;
            }else{
                $skp_realisasi->realisasi_target = (($skp_realisasi->realisasi/$skp_realisasi->trajectory)*100);
                $total_realisasi_terhadap_target +=  $skp_realisasi->realisasi_target;
            }
            
            if($post['bobot'][$nomor] == 0){
                $skp_realisasi->indesk_kinerja = 0;
            }else{
                $skp_realisasi->indesk_kinerja = (($skp_realisasi->realisasi_target/100)*($skp_realisasi->bobot/100)*100);
                $total_realisasi += ($skp_realisasi->indesk_kinerja/100)*100;
            }

            if($post['jenis_indikator'][$nomor] == 'TAMBAHAN'){
                $total_tambahan = 10;
            }

            if(isset($request->file('data')['file'][$nomor])){
                $path = $request->file('data')['file'][$nomor]->storePublicly('public/'.auth()->user()->nip.'/'.date('Y').'/'.date('m'));
                $skp_realisasi->file = $path;
            }

            $total_bobot += ($skp_realisasi->bobot/100)*100;

            $skp_realisasi->save();

        }

        if(($total_realisasi_terhadap_target/100) > 0){
            $bobot = $total_realisasi_terhadap_target/100;
        } else {
            $bobot = $total_bobot/100;
        }

        $total_proporsioal = $total_bobot;
        
        $total_proporsioal_tambahan = ($total_proporsioal + $total_tambahan);

        $data_header = RealisasiHeader::find($id_header);
        $data_header->indesk_ril = $total_realisasi;
        $data_header->indesk_proporsional = $total_proporsioal;
        $data_header->indesk_tambahan = $total_proporsioal_tambahan;
        $data_header->save();
        
        return redirect()->route('skp');
    }

    public function detail_realisasi($tahun, Request $request){

        $skp = RealisasiHeader::where('created_by', auth()->user()->id_users)->where('tahun', $tahun)->get();
        return view('skp.tabel-realisasi-skp',compact('skp'));
    }

    public function view_realisasi($id, Request $request){
        $header = RealisasiHeader::find($id);
        $skp = SkpRealisasi::where('id_skp_header', $header->id)->get();

        $data = [];
        foreach($skp as $item){
            $data[$item->jenis_indikator][] = $item;
        }

        return view('skp.data-realisasi-skp',compact('skp', 'data','header'));
    }

    public function kirim_realisasi($id, Request $request)
    {
        $realisasi_header = RealisasiHeader::find($id);
        $realisasi_header->status = 1;
        $realisasi_header->save();

        return redirect()->route('skp');
    }

}
