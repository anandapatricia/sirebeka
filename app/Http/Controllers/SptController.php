<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Spt;

class SptController extends Controller
{
    public function table_spt(Request $request){
        $spt = Spt::where('nip', auth()->user()->nip)->get();
        return view('spt.table_spt',compact('spt'));
    }

    public function form_spt(Request $request){
        return view('spt.form_spt');
    }

    public function save_spt(Request $request)
    {
        // dd(auth()->user()->nip);
        // dd($request->input())
        $spt = new Spt;
        $spt->nama = auth()->user()->nama;
        $spt->nip = auth()->user()->nip;
        $spt->jabatan = !empty(auth()->user()->jabatan_struktural) ? auth()->user()->jabatan_struktural : auth()->user()->jabatan_fungsional;
        $spt->pangkat = auth()->user()->pangkat;
        $spt->golongan = auth()->user()->golongan;
        $spt->tanggal_surat = $request->input('tanggal_surat');
        $spt->nomor_surat =  $request->input('nomor_surat');
        $spt->tanggal_mulai = $request->input('tanggal_mulai');
        $spt->tanggal_selesai = $request->input('tanggal_selesai');
        $spt->lokasi_spt = $request->input('lokasi_spt');
        $spt->uraian = $request->input('uraian');
        $spt->save();

        return redirect()->route('table_spt');
    }

    public function edit_spt($id){
        $spt = Spt::find($id);
        return view('spt.edit_spt',compact('spt'));
    }

    public function update_spt(Request $request, $id)
    {
        $spt= Spt::find($id);
        $spt->nama = $request->input('nama');
        $spt->nip = $request->input('nip');
        $spt->jabatan = $request->input('jabatan');
        $spt->pangkat = $request->input('pangkat');
        $spt->golongan = $request->input('golongan');
        $spt->tanggal_surat = $request->input('tanggal_surat');
        $spt->nomor_surat =  $request->input('nomor_surat');
        $spt->tanggal_mulai = $request->input('tanggal_mulai');
        $spt->tanggal_selesai = $request->input('tanggal_selesai');
        $spt->lokasi_spt = $request->input('lokasi_spt');
        $spt->uraian = $request->input('uraian');
        $spt->save();

        return redirect()->route('table_spt');
    }

    public function detail_spt($id){
        $spt = Spt::find($id);
        return view('spt.detail_spt',compact('spt'));
    }

    public function kirim_spt($id){
        $spt = Spt::find($id);
        $spt->status = 1;
        $spt->save();
        return redirect()->route('table_spt');
    }

    public function delete_spt($id){
        $spt = Spt::find($id);
        $nomor_surat = $spt->nomor_surat;
        $spt->delete();
        return redirect()->route('table_spt')->with('notif','data '.$nomor_surat.' berhasil di hapus');
    }
}
