<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SigninController extends Controller
{
    public function index(){
//        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
//            //Login Success
        return redirect()->route('signIn');
//        }else{
//        return redirect()->route('dashboard');
//        }
    }

    public function signIn(Request $request){

        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->route('dashboard');
        }
    //    dd($credentials);
    }

}

