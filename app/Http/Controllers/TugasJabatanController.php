<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jabatan;
use App\Models\TugasJabatan;

class TugasJabatanController extends Controller
{
    public function tjabatan($id)
    {
        $jabatan = Jabatan::find($id);
        return view('tugasjabatan.form-tugasjabatan',compact('jabatan'));
    }


    public function savetugasjabatan(Request $request)
    {
        $tugasjabatan = new TugasJabatan();
        $tugasjabatan->id_jabatan_baru = $request->input('id_jabatan_baru');
        $tugasjabatan->tugas = $request->input('tugas');
        $tugasjabatan->keluaran = $request->input('keluaran');
        $tugasjabatan->kuantitas = $request->input('kuantitas');
        $tugasjabatan->kualitas = $request->input('kualitas');
        $tugasjabatan->waktu = $request->input('waktu');
        $tugasjabatan->biaya = $request->input('biaya');
        $tugasjabatan->save();

        return redirect('masterJabatan');
    }

    public function tugasjabatan($id)
    {
        $tugasjabatan = TugasJabatan::where('id_jabatan_baru',$id)->get()->toArray();
        return view('tugasjabatan.tabel-tugasjabatan',compact('tugasjabatan'));
    }

    public function edittugasjabatan($id)
    {
        $tugasjabatan = TugasJabatan::find($id);
        return view('tugasjabatan.edit-tugasjabatan',compact('tugasjabatan','id'));
    }

    public function updatetugasjabatan(Request $request, $id)
    {
        $tugasjabatan = TugasJabatan::find($id);
        $tugasjabatan->tugas = $request->get('tugas');
        $tugasjabatan->keluaran = $request->get('keluaran');
        $tugasjabatan->kuantitas = $request->get('kuantitas');
        $tugasjabatan->kualitas = $request->get('kualitas');
        $tugasjabatan->waktu = $request->get('waktu');
        $tugasjabatan->biaya = $request->get('biaya');
        $tugasjabatan->save();

        return redirect('masterJabatan');
    }
}
