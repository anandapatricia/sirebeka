<?php

namespace App\Http\Controllers;
use App\Models\Golongan;
use Illuminate\Http\Request;

class masterGolonganController extends Controller
{
    public function mgolongan()
    {
        $golongan = Golongan::all();
        return view('datagolongan.tabel-golongan',compact('golongan'));
    }

    public function formgolongan(Request $request){
        return view('datagolongan.form-golongan');
    }

    public function savegolongan(Request $request)
    {
        $golongan = new Golongan();
        $golongan->jenis = $request->input('jenis');
        $golongan->golongan = $request->input('golongan');
        $golongan->persen = $request->input('persen');
        $golongan->save();

        return redirect('masterGolongan');
    }

    public function edit($id)
    {
        $golongan = Golongan::find($id);
        return view('datagolongan.edit-golongan',compact('golongan','id'));
    }

    public function update(Request $request, $id)
    {
        $golongan = Golongan::find($id);
        $golongan->jenis = $request->get('jenis');
        $golongan->golongan = $request->get('golongan');
        $golongan->persen = $request->get('persen');
        $golongan->save();

        return redirect('masterGolongan');
    }
}
