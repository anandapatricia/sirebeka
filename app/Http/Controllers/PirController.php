<?php

namespace App\Http\Controllers;
use App\Models\PirModel;
use App\Models\Jabatan;
use Illuminate\Http\Request;

class PirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $form_pir = new PirModel;
        $pir = PirModel::all();
        return view('pir.form-input',compact('pir','form_pir'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
    //    PirModel::create([
    //         `tahun` => $request->tahun,
    //         `proyeksi_pnbp` => $request->proyeksi_pnbp,
    //         `penyetaraan` => $request->penyetaraan,
    //         `bpjs_non_pns` => $request->bpjs_non_pns,
    //         `uang_makan_non_pns` => $request->uang_makan_non_pns,
    //         `honorarium_ketua` => $request->honorarium_ketua,
    //         `honorarium_anggota` => $request->honorarium_anggota,
    //         `honorarium_sekertaris` => $request->honorarium_sekertaris
    //     ]);

        $a =  $request->input('proyeksi_pnbp');
        $b =  $request->input('proyeksi_pnbp') * 0.4;
        $c =  $request->input('penyetaraan');
        $d =  $request->input('bpjs_non_pns');
        $e =  $request->input('uang_makan_non_pns');
        $f_ketua = $request->input('honorarium_ketua');
        $f_anggota = $request->input('honorarium_anggota');
        $f_sekretaris = $request->input('honorarium_sekertaris');
        $f = $f_ketua + $f_sekretaris + $f_anggota;
        $g = $c + $d + $e + $f;
        $h = $b - $g;
        $i = $h / 13;
        // $j = $total->total_job_value;
        $j = 315844;
        $k = $i / $j;
        $l = round($k);

        $pir = new PirModel();
        $pir->tahun = $request->input('tahun');
        $pir->proyeksi_pnbp = $request->input('proyeksi_pnbp');
        $pir->penyetaraan = $request->input('penyetaraan');
        $pir->bpjs_non_pns = $request->input('bpjs_non_pns');
        $pir->uang_makan_non_pns = $request->input('uang_makan_non_pns');
        $pir->honorarium_ketua = $request->input('honorarium_ketua');
        $pir->honorarium_anggota = $request->input('honorarium_anggota');
        $pir->honorarium_sekertaris = $request->input('honorarium_sekertaris');
        $pir->jumlah = $g;
        $pir->kebutuhan_remun_year = $h;
        $pir->kebutuhan_remun_month = $i;
        $pir->job_value = $j;
        $pir->point_index = $k;
        $pir->pir = $l;
        $pir->save();
        return redirect()->route('pir');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getData = PirModel::findOrFail($id);
        return view('pir.form-input',compact('getData'));    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pir = PirModel::findOrFail($id);
        $total = Jabatan::selectRaw('sum(nilai_jabatan) as total_job_value')->first();
        return view('pir.form-detail',compact('pir','total'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pir = PirModel::find($id);
        $pir->delete();

        return redirect()->route('pir');
    }
}
