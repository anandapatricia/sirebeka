<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Iku;

class MasterIKUController extends Controller
{
    public function ikutable(Request $request){
        $iku = Iku::all();
        return view('iku.ikutable',compact('iku'));
    }

     public function iku(Request $request){
        return view('iku.iku');
    }

    public function iku_save(Request $request)
    {
        $iku = new Iku();
        $iku->iku = $request->input('iku');
        $iku->save();
    
        return redirect()->route('ikutable');
    }

    public function ikuedit($id){
        $iku = Iku::find($id);
        return view('iku.ikuedit',compact('iku'));
    }

    public function ikuupdate(Request $request, $id)
    {
        $iku= Iku::find($id);
        $iku->iku = $request->input('iku');
        $iku->save();
    
        return redirect()->route('ikutable');
    }
}
