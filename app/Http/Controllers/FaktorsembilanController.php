<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//panggil model class
use App\Models\FaktorSembilanI;
use App\Models\FaktorSembilanD;

class FaktorsembilanController extends Controller
{
    
    //faktorsembilanIndeterminate

    public function fi9table(Request $request){
        $fi9 = FaktorSembilanI::all();
        return view('faktorsembilanI.fi9table',compact('fi9'));
    }

    public function fi9(Request $request){
        return view('faktorsembilanI.fi9');
    }

    public function fi9_save(Request $request)
    {
        $fi9 = new FaktorSembilanI();
        $fi9->jenis = $request->input('jenis');
        $fi9->tipe = $request->input('tipe');
        $fi9->nilai = $request->input('nilai');
        $fi9->save();
    
        return redirect()->route('fi9table');
    }

    public function fi9edit($id)
    {
        $fi9 = FaktorSembilanI::find($id);
        return view('faktorsembilanI.fi9edit',compact('fi9'));
    }

    public function fi9update(Request $request, $id)
    {
        $fi9= FaktorSembilanI::find($id);
        $fi9->jenis = $request->input('jenis');
        $fi9->tipe = $request->input('tipe');
        $fi9->nilai = $request->input('nilai');
        $fi9->save();

        return redirect()->route('fi9table');
    }

    //faktorsembilanDeterminate

    public function fd9table(Request $request){
        $fd9 = FaktorSembilanD::all();
        return view('faktorsembilanD.fd9table',compact('fd9'));
    }

    public function fd9(Request $request){
        return view('faktorsembilanD.fd9');
    }

    public function fd9_save(Request $request)
    {
        $fd9 = new FaktorSembilanD();
        $fd9->jenis = $request->input('jenis');
        $fd9->tipe = $request->input('tipe');
        $fd9->nilai = $request->input('nilai');
        $fd9->save();
    
        return redirect()->route('fd9table');
    }

    public function fd9edit($id)
    {
        $fd9 = FaktorSembilanD::find($id);
        return view('faktorsembilanD.fd9edit',compact('fd9'));
    }

    public function fd9update(Request $request, $id)
    {
        $fd9= FaktorSembilanD::find($id);
        $fd9->jenis = $request->input('jenis');
        $fd9->tipe = $request->input('tipe');
        $fd9->nilai = $request->input('nilai');
        $fd9->save();

        return redirect()->route('fd9table');
    }
}
