<?php

namespace App\Http\Controllers;
use App\Models\DataPegawai;
use Illuminate\Http\Request;
use App\Models\Jabatan;
use App\Models\User;
use App\Models\ImportAbsensiModel;
use App\Models\Golongan;
use DB;

class HomeController extends Controller
{

    // untuk memanggil view data pegawai
    public function datapegawai(Request $request){
        $jabatan  = Jabatan::all();
        $golongan = Golongan::all();
        $html = '';
        return view('pegawai.data', compact('jabatan','golongan'));
    }

    public function index()
    {
        $list = User::where('role',3)->get();
        return view('pegawai.index',compact('list'));
    }

    public function getAbsensiByID($nip)
    {
        $absen_pegawai = ImportAbsensiModel::where('nip',$nip)->get();
        return view('pegawai.data_absensi',compact('absen_pegawai'));
    }


    public function __getData($function_name, $params){
        $endpoint = "https://sik.dephub.go.id/api/index.php/soap_services/sik_api?wsdl";
        $username = "getdatasik";
        $password = "123456";

        $options = array(
            'login' => $username,
            'password' => $password,
            'trace'=>1
        );
        $client = new \SoapClient($endpoint, $options);
        try {
            $responseData = $client->__soapCall($function_name,$params);
            if($responseData['status']->status == 1){
                $response = [
                    'rc'=>'00',
                    'rcMessage'=>'success',
                    'data'=>$responseData
                ];
            } else {
                $response = [
                    'rc'=>'01',
                    'rcMessage'=>'error',
                    'data'=>$responseData
                ];
            }
        } catch (\SoapFault $fault) {
            $response = [
                'rc'=>'99',
                'rcMessage'=>$fault->faultcode."-".$fault->faultstring,
                'data'=>null
            ];
        }
        return $response;
    }

    public function getSIK()
    { $html = '';


        return view('pegawai.getSIK', compact('html'));
    }

    // untuk melakukan proses penyimpanan data pegawai
    public function datadetail(Request $request){
        $nip = $request->input('nosik');

        $getContent = file_get_contents('https://skemaraja.dephub.go.id/api/sik/'.$nip);
        $nosik = json_decode($getContent);
        if($nosik->rcMessage != "sukses"){
            echo 'data tidak ditemukan';
        }
        else{
            $nosik = $nosik->data;
            $html =
                  '
            <form id="data" action="save" methode="post">

            <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                    <label>NIP</label>
                    <input type="text" class="form-control" id="nip" value="'.$nosik->nip.'" name="nip" disabled>
                </div>

                <div class="form-group">
                    <label>NIK</label>
                    <input type="text" class="form-control" id="nik" value="'.$nosik->nik.'"  name="nik" disabled>
                </div>

                <div class="form-group">
                    <label>TANGGAL LAHIR</label>
                    <input type="text" class="form-control" id="tanggal_lahir" value="'.$nosik->tanggal_lahir.'" name="tanggal_lahir" disabled>
                </div>

                <div class="form-group">
                    <label>TEMPAT LAHIR</label>
                    <input type="text" class="form-control" id="tempat_lahir" value="'.$nosik->tempat_lahir.'"  name="tempat_lahir" disabled>
                </div>

                <div class="form-group">
                    <label>NAMA</label>
                    <input type="text" class="form-control" id="nama" value="'.$nosik->nama.'" name="nama" disabled>
                </div>

                <div class="form-group">
                    <label>JENIS KELAMIN</label>
                    <input type="text" class="form-control" id="jenis_kelamin" value="'.$nosik->jenis_kelamin.'"  name="jenis_kelamin" disabled>
                </div>

                <div class="form-group">
                    <label>KARPEG</label>
                    <input type="text" class="form-control" id="karpeg" value="'.$nosik->karpeg.'" karpeg="nama" disabled>
                </div>

                <div class="form-group">
                    <label>UNIT</label>
                    <input type="text" class="form-control" id="unit" value="'.$nosik->unit.'"  name="unit" disabled>
                </div>

                <div class="form-group">
                    <label>KODE UNIT</label>
                    <input type="text" class="form-control" id="kode_unit" value="'.$nosik->kode_unit.'"  name="kode_unit" disabled>
                </div>

                <div class="form-group">
                    <label>KODE UNIT LAMA</label>
                    <input type="text" class="form-control" id="kode_unit_lama" value="'.$nosik->kode_unit_lama.'"  name="kode_unit_lama" disabled>
                </div>

                <div class="form-group">
                <label>KANTOR</label>
                <input type="text" class="form-control" id="kantor" value="'.$nosik->kantor.'"  name="kantor" disabled>
            </div>

            <div class="form-group">
                <label>KODE KANTOR</label>
                <input type="text" class="form-control" id="kode_kantor" value="'.$nosik->kode_kantor.'"  name="kode_kantor" disabled>
            </div>

            <div class="form-group">
                <label>KODE KANTOR LAMA</label>
                <input type="text" class="form-control" id="kode_kantor_lama" value="'.$nosik->kode_kantor_lama.'"  name="kode_kantor_lama" disabled>
            </div>

            <div class="form-group">
                <label>SUBKANTOR</label>
                <input type="text" class="form-control" id="subkantor" value="'.$nosik->subkantor.'"  name="subkantor" disabled>
            </div>

            <div class="form-group">
                <label>KODE SUBKANTOR</label>
                <input type="text" class="form-control" id="kode_subkantor" value="'.$nosik->kode_subkantor.'"  name="kode_subkantor" disabled>
            </div>
                </div>
                <div class="col-md-6">


            <div class="form-group">
                <label>TMT GOLONGAN</label>
                <input type="text" class="form-control" id="tmt_golongan" value="'.$nosik->tmt_golongan.'"  name="tmt_golongan" disabled>
            </div>

            <div class="form-group">
                <label>GOLONGAN</label>
                <input type="text" class="form-control" id="golongan" value="'.$nosik->golongan.'"  name="golongan" disabled>
            </div>

            <div class="form-group">
                <label>PANGKAT</label>
                <input type="text" class="form-control" id="pangkat" value="'.$nosik->pangkat.'"  name="pangkat" disabled>
            </div>

            <div class="form-group">
                <label>KODE ESELON</label>
                <input type="text" class="form-control" id="kode_eselon" value="'.$nosik->kode_eselon.'"  name="kode_eselon" disabled>
            </div>

            <div class="form-group">
                <label>ESELON</label>
                <input type="text" class="form-control" id="eselon" value="'.$nosik->eselon.'"  name="eselon"disabled >
            </div>

            <div class="form-group">
                <label>EMAIL</label>
                <input type="text" class="form-control" id="email" value="'.$nosik->email.'"  name="email" disabled>
            </div>

            <div class="form-group">
                <label>PHONE</label>
                <input type="text" class="form-control" id="telepon" value="'.$nosik->telepon.'"  name="telepon" disabled>
            </div>

            <div class="form-group">
                <label>TMT CPNS</label>
                <input type="text" class="form-control" id="tmt_cpns" value="'.$nosik->tmt_cpns.'"  name="tmt_cpns" disabled>
            </div>

            <div class="form-group">
                <label>TMT PNS</label>
                <input type="text" class="form-control" id="tmt_pns" value="'.$nosik->tmt_pns.'"  name="tmt_pns" disabled>
            </div>

            <div class="form-group">
                <label>PENDIDIKAN TERAKHIR</label>
                <input type="text" class="form-control" id="pendidikan_terakhir" value="'.$nosik->pendidikan_terakhir.'"  name="pendidikan_terakhir" disabled>
            </div>

                <div class="form-group">
                    <label>FOTO</label>
                    <input type="text" class="form-control" id="foto" value="'.$nosik->foto.'"  name="foto" disabled>
                </div>
                </div>
            </div>

                <div class="form-group">
                    <button type="text" class="btn btn-primary"> SAVE !</button>
                </div>

                ';
            }
            return view('pegawai.getSIK',compact('html'));
        }

        public function save(Request $request){
            $nip = $request->input('nip');
            // $data = $this->__getData('get_pegawai_by_nip',['nip'=>$nip]);
            $getContent = file_get_contents('https://skemaraja.dephub.go.id/api/sik/'.$nip);
            $nosik = json_decode($getContent);
            if($nosik->rcMessage != "sukses"){
                echo 'data tidak ditemukan';
            }
            $pegawai = $nosik->data;


            $user = new DataPegawai;
            // $user->password = bcrypt($pegawai->nip);
            $user->nip = property_exists($pegawai,"nip") ? $pegawai->nip : "";
            $user->nama = property_exists($pegawai,"nama") ? $pegawai->nama : "";
            $user->email = property_exists($pegawai,"email") ? $pegawai->email : "";
            $user->tanggal_lahir = property_exists($pegawai,"tanggal_lahir") ? $pegawai->tanggal_lahir : "";
            $user->tempat_lahir = property_exists($pegawai,"tempat_lahir") ? $pegawai->tempat_lahir : "";
            $user->jenis_kelamin = property_exists($pegawai,"jenis_kelamin") ? $pegawai->jenis_kelamin : "";
            $user->karpeg = property_exists($pegawai,"karpeg") ? $pegawai->karpeg : "";
            $user->unit = property_exists($pegawai,"unit") ? $pegawai->unit : "";
            $user->kode_unit = property_exists($pegawai,"kode_unit") ? $pegawai->kode_unit : "";
            $user->kantor = property_exists($pegawai,"kantor") ? $pegawai->kantor : "";
            $user->kode_kantor = property_exists($pegawai,"kode_kantor") ? $pegawai->kode_kantor : "";
            $user->subkantor = property_exists($pegawai,"subkantor") ? $pegawai->subkantor : "";
            $user->kode_subkantor = property_exists($pegawai,"kode_subkantor") ? $pegawai->kode_subkantor : "";
            $tmt_golongan = property_exists($pegawai,"tmt_golongan") ? $pegawai->tmt_golongan : "";
            $user->tmt_golongan = !empty($tmt_golongan) ? $tmt_golongan : null;
            $user->golongan = property_exists($pegawai,"golongan") ? $pegawai->golongan : "";
            $user->pangkat = property_exists($pegawai,"pangkat") ? $pegawai->pangkat : "";
            $user->telepon = property_exists($pegawai,"telepon") ? $pegawai->telepon : "";
            $tmt_cpns = property_exists($pegawai,"tmt_cpns") ? $pegawai->tmt_cpns : "";
            $user->tmt_cpns = !empty($tmt_cpns) ? $tmt_cpns : null;
            $tmt_pns = property_exists($pegawai,"tmt_pns") ? $pegawai->tmt_pns : "";
            $user->tmt_pns = !empty($tmt_pns) ? $tmt_pns : null;
            $user->pendidikan_terakhir = property_exists($pegawai,"pendidikan_terakhir") ? $pegawai->pendidikan_terakhir : "";
            $user->foto = property_exists($pegawai,"foto") ? $pegawai->foto : "";
            $user->save();
            return redirect()->route('data.index');
        }




    public function insertData(Request $request)
    {
        $file = $request->file('foto');
        $nama_file = time()."_".$file->getClientOriginalName();
        $tujuan_upload = 'foto';
        $file->move($tujuan_upload,$nama_file);

        DataPegawai::create([
            'foto' => $nama_file,
            'nip'=> $request->nip,
            'nik'=> $request->nik,
            'username' => $request->nik,
            'email'=> $request->email,
            'password' => bcrypt($request->nik),
            'tanggal_lahir'=> $request->tanggal_lahir,
            'tempat_lahir'=> $request->tempat_lahir,
            'nama'=> $request->nama,
            'jenis_kelamin'=> $request->jenis_kelamin,
            'karpeg'=> $request->karpeg,
            'unit'=> $request->unit,
            'kode_unit'=> $request->kode_unit,
            'kode_unit_lama'=> $request->kode_unit_lama,
            'kantor'=> $request->kantor,
            'kode_kantor'=> $request->kode_kantor,
            'kode_kantor_lama'=> $request->kode_kantor_lama,
            'subkantor'=> $request->subkantor,
            'kode_subkantor'=> $request->kode_subkantor,
            'tmt_golongan'=> $request->tmt_golongan,
            'golongan'=> $request->golongan,
            'pangkat'=> $request->pangkat,
            'kode_eselon'=> $request->kode_eselon,
            'eselon'=> $request->eselon,
            'id_jabatan'=> $request->id_jabatan,
            'telepon'=> $request->telepon,
            'tmt_cpns'=> $request->tmt_cpns,
            'tmt_pns'=> $request->tmt_pns,
            'id_jabatan'=> $request->id_jabatan,
            'golongan_global'=> $request->golongan_global,
            'pendidikan_terakhir'=> $request->pendidikan_terakhir
        ]);

        return redirect()->route('data.index');

    }

    public function detailPegawai($id)
    {
        // $detail = DataPegawai::findOrFail($id);
        $detail = DB::table('users')
                ->leftJoin('m_jabatan', 'users.id_jabatan', '=', 'm_jabatan.id')
                ->leftJoin('m_golongan','users.golongan','=', 'm_golongan.id')
                ->select('users.*','m_jabatan.*','m_golongan.*')
                ->where('users.id_users', '=', $id)
                ->get();

        $jabatan = Jabatan::all();
        $golongan = Golongan::all();

        return view('pegawai.datadetail',compact('detail','jabatan','golongan'));
    }

    public function updatePegawai(Request $request)
    {

        $image_name = $request->hidden_image;
        $image = $request->file('foto');
        if($image != '')
        {
            $request->validate([
                'foto'         =>  'image|max:2048'
            ]);

            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('foto'), $image_name);
        }

        $data = [
            'foto' => $image_name,
            'nip'=> $request->nip,
            'nik'=> $request->nik,
            'tanggal_lahir'=> $request->tanggal_lahir,
            'tempat_lahir'=> $request->tempat_lahir,
            'nama'=> $request->nama,
            'jenis_kelamin'=> $request->jenis_kelamin,
            'karpeg'=> $request->karpeg,
            'unit'=> $request->unit,
            'kode_unit'=> $request->kode_unit,
            'kode_unit_lama'=> $request->kode_unit_lama,
            'kantor'=> $request->kantor,
            'kode_kantor'=> $request->kode_kantor,
            'kode_kantor_lama'=> $request->kode_kantor_lama,
            'subkantor'=> $request->subkantor,
            'kode_subkantor'=> $request->kode_subkantor,
            'tmt_golongan'=> $request->tmt_golongan,
            'golongan'=> $request->golongan,
            'pangkat'=> $request->pangkat,
            'kode_eselon'=> $request->kode_eselon,
            'eselon'=> $request->eselon,
            'email'=> $request->email,
            'telepon'=> $request->telepon,
            'tmt_cpns'=> $request->tmt_cpns,
            'tmt_pns'=> $request->tmt_pns,
            'id_jabatan'=> $request->id_jabatan,
            'golongan_global'=> $request->golongan_global,
            'pendidikan_terakhir'=> $request->pendidikan_terakhir
        ];

       	DB::table('users')->where('id_users',$request->id_users)->update($data);
        return redirect()->route('data.index');
    }

    public function syncPegawai(Request $request)
    {
            ini_set('max_execution_time', 300);
            $params = '008017000000000';
            $data_pegawai = $this->__getData('get_pegawai_per_kantor', [
                    'kode_kantor' => $params
            ]);

            $data = $data_pegawai['data']['return'];
            foreach($data as $pegawai)
            {
                $user = new User;
                $user->username = $pegawai->nip;
                $user->password = bcrypt($pegawai->nip);
                $user->nip = property_exists($pegawai,"nip") ? $pegawai->nip : "";
                $user->nama = property_exists($pegawai,"nama") ? $pegawai->nama : "";
                $user->email = property_exists($pegawai,"email") ? $pegawai->email : "";
                $user->tanggal_lahir = property_exists($pegawai,"tanggal_lahir") ? $pegawai->tanggal_lahir : "";
                $user->tempat_lahir = property_exists($pegawai,"tempat_lahir") ? $pegawai->tempat_lahir : "";
                $user->jenis_kelamin = property_exists($pegawai,"jenis_kelamin") ? $pegawai->jenis_kelamin : "";
                $user->karpeg = property_exists($pegawai,"karpeg") ? $pegawai->karpeg : "";
                $user->unit = property_exists($pegawai,"unit") ? $pegawai->unit : "";
                $user->kode_unit = property_exists($pegawai,"kode_unit") ? $pegawai->kode_unit : "";
                $user->kantor = property_exists($pegawai,"kantor") ? $pegawai->kantor : "";
                $user->kode_kantor = property_exists($pegawai,"kode_kantor") ? $pegawai->kode_kantor : "";
                $user->subkantor = property_exists($pegawai,"subkantor") ? $pegawai->subkantor : "";
                $user->kode_subkantor = property_exists($pegawai,"kode_subkantor") ? $pegawai->kode_subkantor : "";
                $tmt_golongan = property_exists($pegawai,"tmt_golongan") ? $pegawai->tmt_golongan : "";
                $user->tmt_golongan = !empty($tmt_golongan) ? $tmt_golongan : null;
                $user->golongan = property_exists($pegawai,"golongan") ? $pegawai->golongan : "";
                $user->pangkat = property_exists($pegawai,"pangkat") ? $pegawai->pangkat : "";
                if(!empty($pegawai->jabatan_struktural)){
                    $user->jabatan_struktural = property_exists($pegawai,"jabatan_struktural") ? $pegawai->jabatan_struktural : "";
                    $tmt_jabatan_struktural = property_exists($pegawai,"tmt_jabatan_struktural") ? $pegawai->tmt_jabatan_struktural : "";
                    $user->tmt_jabatan_struktural = !empty($tmt_jabatan_struktural) ? $tmt_jabatan_struktural : null;
                } else {
                    $user->jabatan_fungsional = property_exists($pegawai,"jabatan_fungsional") ? $pegawai->jabatan_fungsional : "";
                    $tmt_jabatan_fungsional = property_exists($pegawai,"tmt_jabatan_fungsional") ? $pegawai->tmt_jabatan_fungsional : "";
                    $user->tmt_jabatan_fungsional = !empty($tmt_jabatan_fungsional) ? $tmt_jabatan_fungsional : null;
                }
                $user->telepon = property_exists($pegawai,"telepon") ? $pegawai->telepon : "";
                $tmt_cpns = property_exists($pegawai,"tmt_cpns") ? $pegawai->tmt_cpns : "";
                $user->tmt_cpns = !empty($tmt_cpns) ? $tmt_cpns : null;
                $tmt_pns = property_exists($pegawai,"tmt_pns") ? $pegawai->tmt_pns : "";
                $user->tmt_pns = !empty($tmt_pns) ? $tmt_pns : null;
                $user->pendidikan_terakhir = property_exists($pegawai,"pendidikan_terakhir") ? $pegawai->pendidikan_terakhir : "";
                $user->foto = property_exists($pegawai,"foto") ? $pegawai->foto : "";
                $user->status_pns = !empty($pegawai->nip) ? 1 : 0;
                $user->role = 3;
                $user->save();

        }
        return redirect()->route('data.index');
    }

    public function datasik(Request $request){
        $nip = $request->input('nip');
        // $data = $this->__getData('get_pegawai_by_nip',['nip'=>$nip]);
        $getContent = file_get_contents('https://skemaraja.dephub.go.id/api/sik/'.$nip);
        $nosik = json_decode($getContent);
        if($nosik->rcMessage != "sukses"){
            echo 'data tidak ditemukan';
        }
        // dd($nosik);
            $pegawai = $nosik->data;

            // if(empty($pegawai->jabatan_struktural))
            // {
                $user = new DataPegawai;
                // $user->password = bcrypt($pegawai->nip);
                $user->nip = property_exists($pegawai,"nip") ? $pegawai->nip : "";
                $user->nama = property_exists($pegawai,"nama") ? $pegawai->nama : "";
                $user->email = property_exists($pegawai,"email") ? $pegawai->email : "";
                $user->tanggal_lahir = property_exists($pegawai,"tanggal_lahir") ? $pegawai->tanggal_lahir : "";
                $user->tempat_lahir = property_exists($pegawai,"tempat_lahir") ? $pegawai->tempat_lahir : "";
                $user->jenis_kelamin = property_exists($pegawai,"jenis_kelamin") ? $pegawai->jenis_kelamin : "";
                $user->karpeg = property_exists($pegawai,"karpeg") ? $pegawai->karpeg : "";
                $user->unit = property_exists($pegawai,"unit") ? $pegawai->unit : "";
                $user->kode_unit = property_exists($pegawai,"kode_unit") ? $pegawai->kode_unit : "";
                $user->kantor = property_exists($pegawai,"kantor") ? $pegawai->kantor : "";
                $user->kode_kantor = property_exists($pegawai,"kode_kantor") ? $pegawai->kode_kantor : "";
                $user->subkantor = property_exists($pegawai,"subkantor") ? $pegawai->subkantor : "";
                $user->kode_subkantor = property_exists($pegawai,"kode_subkantor") ? $pegawai->kode_subkantor : "";
                $tmt_golongan = property_exists($pegawai,"tmt_golongan") ? $pegawai->tmt_golongan : "";
                $user->tmt_golongan = !empty($tmt_golongan) ? $tmt_golongan : null;
                $user->golongan = property_exists($pegawai,"golongan") ? $pegawai->golongan : "";
                $user->pangkat = property_exists($pegawai,"pangkat") ? $pegawai->pangkat : "";
                if(!empty($pegawai->jabatan_struktural)){
                    $user->jabatan_struktural = property_exists($pegawai,"jabatan_struktural") ? $pegawai->jabatan_struktural : "";
                    $tmt_jabatan_struktural = property_exists($pegawai,"tmt_jabatan_struktural") ? $pegawai->tmt_jabatan_struktural : "";
                    $user->tmt_jabatan_struktural = !empty($tmt_jabatan_struktural) ? $tmt_jabatan_struktural : null;
                } else {
                    $user->jabatan_fungsional = property_exists($pegawai,"jabatan_fungsional") ? $pegawai->jabatan_fungsional : "";
                    $tmt_jabatan_fungsional = property_exists($pegawai,"tmt_jabatan_fungsional") ? $pegawai->tmt_jabatan_fungsional : "";
                    $user->tmt_jabatan_fungsional = !empty($tmt_jabatan_fungsional) ? $tmt_jabatan_fungsional : null;
                }
                $user->telepon = property_exists($pegawai,"telepon") ? $pegawai->telepon : "";
                $tmt_cpns = property_exists($pegawai,"tmt_cpns") ? $pegawai->tmt_cpns : "";
                $user->tmt_cpns = !empty($tmt_cpns) ? $tmt_cpns : null;
                $tmt_pns = property_exists($pegawai,"tmt_pns") ? $pegawai->tmt_pns : "";
                $user->tmt_pns = !empty($tmt_pns) ? $tmt_pns : null;
                $user->pendidikan_terakhir = property_exists($pegawai,"pendidikan_terakhir") ? $pegawai->pendidikan_terakhir : "";
                $user->foto = property_exists($pegawai,"foto") ? $pegawai->foto : "";
                // $user->status_pns = 1;
                // $user->role = 9;
                $user->save();

        // }
        return redirect()->route('data.index');
    }

    public function absensi($id, Request $request){
        $pegawai = User::find($id);
        $bulan = !empty($request->input('bulan')) ? $request->input('bulan') : date("m");
        $tahun = !empty($request->input('tahun')) ? $request->input('tahun') : date("Y");

        $data_absensi = $this->__getAbsensi($pegawai->nip, $bulan, $tahun);
        // dd($data_absensi);
        if($data_absensi->status){
            $list = $data_absensi->data;
        } else {
            $list = null;
        }
        return view('pegawai.absensi',compact('pegawai','pegawai','bulan','tahun','list'));
    }


    private function __getAbsensi($nip, $bulan, $tahun){
        $url = "http://103.85.63.254/api_tu/api/absen/";
        $post = [
            'nip'=>$nip,
            'bulan'=>$bulan,
            'tahun'=>$tahun,
            'sec_key'=>'bf705e141157d1d4f5e8921fa973713b'
        ];

        $request = http_build_query($post);
        // dd($request);
        $headers = [
            "Content-Type:multipart/form-data",
            "User-Agent:SiReBeKa"
        ];

        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $url.'?'.$request,
            CURLOPT_HEADER => false,
            CURLOPT_POST => false,
            CURLOPT_HTTPHEADER => $headers,
            // CURLOPT_POSTFIELDS => $post,
            CURLOPT_USERPWD => "@piSt1P:tu_5t1p",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_AUTOREFERER => true
        );
        // cURL options
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);

        curl_close($ch);
        $data = json_decode($response);
        // dd($data);
        return $data;
    }
}
