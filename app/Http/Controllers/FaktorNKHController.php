<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//panggil model class
use App\Models\FaktorNkh;
use App\Models\FaktorNkhk;
use App\Models\FaktorNkhs;
use App\Models\FaktorNkhm;
use App\Models\FaktorNkht;

class FaktorNKHController extends Controller
{
    
    //faktornkh

    public function f78table(Request $request){
        $f78 = FaktorNkh::all();
        return view('faktorNkh.f78table',compact('f78'));
    }

     public function f78(Request $request){
        return view('faktorNkh.f78');
    }

    public function f78_save(Request $request)
    {
        $f78 = new FaktorNkh();
        $f78->jenis = $request->input('jenis');
        $f78->kelas = $request->input('kelas');
        $f78->umum = $request->input('umum');
        $f78->standar = $request->input('standar');
        $f78->menengah = $request->input('menengah');
        $f78->menengah_tinggi = $request->input('menengah_tinggi');
        $f78->tinggi = $request->input('tinggi');
        $f78->save();
    
        return redirect()->route('f78table');
    }

    public function f78edit($id){
        $f78 = FaktorNkh::find($id);
        return view('faktorNkh.f78edit',compact('f78'));
    }

    public function f78update(Request $request, $id)
    {
        $f78= FaktorNkh::find($id);
        $f78->jenis = $request->input('jenis');
        $f78->kelas = $request->input('kelas');
        $f78->umum = $request->input('umum');
        $f78->standar = $request->input('standar');
        $f78->menengah = $request->input('menengah');
        $f78->menengah_tinggi = $request->input('menengah_tinggi');
        $f78->tinggi = $request->input('tinggi');
        $f78->save();
    
        return redirect()->route('f78table');
    }

    //faktornkhk

    public function f78ktable(Request $request){
        $f78k = FaktorNkhk::all();
        return view('faktorNkhk.f78ktable',compact('f78k'));
    }

     public function f78k(Request $request){
        return view('faktorNkhk.f78k');
    }

    public function f78k_save(Request $request)
    {
        $f78k = new FaktorNkhk();
        $f78k->jenis = $request->input('jenis');
        $f78k->kelas = $request->input('kelas');
        $f78k->r = $request->input('r');
        $f78k->k = $request->input('k');
        $f78k->b = $request->input('b');
        $f78k->u = $request->input('u');
        $f78k->save();
    
        return redirect()->route('f78ktable');
    }

    public function f78kedit($id){
        $f78k = FaktorNkhk::find($id);
        return view('faktorNkhk.f78kedit',compact('f78k'));
    }

    public function f78kupdate(Request $request, $id)
    {
        $f78k= FaktorNkhk::find($id);
        $f78k->jenis = $request->input('jenis');
        $f78k->kelas = $request->input('kelas');
        $f78k->r = $request->input('r');
        $f78k->k = $request->input('k');
        $f78k->b = $request->input('b');
        $f78k->u = $request->input('u');
        $f78k->save();
    
        return redirect()->route('f78ktable');
    }

    //faktornkhs

    public function f78stable(Request $request){
        $f78s = FaktorNkhs::all();
        return view('faktorNkhs.f78stable',compact('f78s'));
    }

     public function f78s(Request $request){
        return view('faktorNkhs.f78s');
    }

    public function f78s_save(Request $request)
    {
        $f78s = new FaktorNkhs();
        $f78s->jenis = $request->input('jenis');
        $f78s->kelas = $request->input('kelas');
        $f78s->r = $request->input('r');
        $f78s->k = $request->input('k');
        $f78s->b = $request->input('b');
        $f78s->u = $request->input('u');
        $f78s->save();
    
        return redirect()->route('f78stable');
    }

    public function f78sedit($id){
        $f78s = FaktorNkhs::find($id);
        return view('faktorNkhs.f78sedit',compact('f78s'));
    }

    public function f78supdate(Request $request, $id)
    {
        $f78s= FaktorNkhs::find($id);
        $f78s->jenis = $request->input('jenis');
        $f78s->kelas = $request->input('kelas');
        $f78s->r = $request->input('r');
        $f78s->k = $request->input('k');
        $f78s->b = $request->input('b');
        $f78s->u = $request->input('u');
        $f78s->save();
    
        return redirect()->route('f78stable');
    }

    //faktornkhm

    public function f78mtable(Request $request){
        $f78m = FaktorNkhm::all();
        return view('faktorNkhm.f78mtable',compact('f78m'));
    }

     public function f78m(Request $request){
        return view('faktorNkhm.f78m');
    }

    public function f78m_save(Request $request)
    {
        $f78m = new FaktorNkhm();
        $f78m->jenis = $request->input('jenis');
        $f78m->kelas = $request->input('kelas');
        $f78m->r = $request->input('r');
        $f78m->k = $request->input('k');
        $f78m->b = $request->input('b');
        $f78m->u = $request->input('u');
        $f78m->save();
    
        return redirect()->route('f78mtable');
    }

    public function f78medit($id){
        $f78m = FaktorNkhm::find($id);
        return view('faktorNkhm.f78medit',compact('f78m'));
    }

    public function f78mupdate(Request $request, $id)
    {
        $f78m= FaktorNkhm::find($id);
        $f78m->jenis = $request->input('jenis');
        $f78m->kelas = $request->input('kelas');
        $f78m->r = $request->input('r');
        $f78m->k = $request->input('k');
        $f78m->b = $request->input('b');
        $f78m->u = $request->input('u');
        $f78m->save();
    
        return redirect()->route('f78mtable');
    }

    //faktornkht

    public function f78ttable(Request $request){
        $f78t = FaktorNkht::all();
        return view('faktorNkht.f78ttable',compact('f78t'));
    }

     public function f78t(Request $request){
        return view('faktorNkht.f78t');
    }

    public function f78t_save(Request $request)
    {
        $f78t = new FaktorNkht();
        $f78t->jenis = $request->input('jenis');
        $f78t->kelas = $request->input('kelas');
        $f78t->r = $request->input('r');
        $f78t->k = $request->input('k');
        $f78t->b = $request->input('b');
        $f78t->u = $request->input('u');
        $f78t->save();
    
        return redirect()->route('f78ttable');
    }

    public function f78tedit($id){
        $f78t = FaktorNkht::find($id);
        return view('faktorNkht.f78tedit',compact('f78t'));
    }

    public function f78tupdate(Request $request, $id)
    {
        $f78t= FaktorNkht::find($id);
        $f78t->jenis = $request->input('jenis');
        $f78t->kelas = $request->input('kelas');
        $f78t->r = $request->input('r');
        $f78t->k = $request->input('k');
        $f78t->b = $request->input('b');
        $f78t->u = $request->input('u');
        $f78t->save();
    
        return redirect()->route('f78ttable');
    }

}