<?php

namespace App\Http\Controllers;
use App\Models\DataPegawai;
use Illuminate\Http\Request;
use App\Models\Remunerasi;
use App\Models\RemunHeaderModel;
use App\Models\PirModel;
use App\Models\User;
use DB;
class RemunerasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $remunerasi = RemunHeaderModel::all();
        return view('remunerasi.index',compact('remunerasi'));
    }
    public function hitung()
    {
        $pegawai = DB::table('users')
        ->join('m_jabatan', 'users.id_jabatan', '=', 'm_jabatan.id')
        ->join('m_golongan','users.golongan','=', 'm_golongan.id')
        ->select('users.*','m_jabatan.*','m_golongan.*')
        ->where('users.status', '=', 'BLU')
        ->orWhere('users.status','=','PNS')
        ->limit(3)->get();
                    
        $insentif_kerja     = '40501000';
        $nilai_skp          = '100';
        $masa_kerja         = '100';
        $bobot_absensi      = '100';
    

        $total_diterima = 0;
        $total_honorium = 0;
        $total_pph21    = 0;
        $total_insentif = 0;
        foreach($pegawai as $row){
            $gaji               = $row->gapok;       
            $pph                = $row->persen;

            // FORMULA PERHITUNGAN
            $honorium           = $gaji*$masa_kerja/100*$bobot_absensi/100;
            $insentif_diterima  = $insentif_kerja*$nilai_skp/100;
            $jumlah_remunerasi  = $insentif_diterima + $honorium;
            $jumlah_pph21       = $jumlah_remunerasi*$pph/100;
            $jumlah_diterima    = $jumlah_remunerasi-$jumlah_pph21;
            
            // Perhitungan Total
            
            $total_diterima     += $jumlah_diterima;
            $total_honorium     += $honorium;
            $total_pph21        += $jumlah_pph21;
            $total_insentif     += $insentif_diterima;
          
        }        
        return view('remunerasi.data-remunerasi',compact('pegawai','honorium','insentif_diterima','jumlah_remunerasi','jumlah_pph21','jumlah_diterima','total_diterima','total_honorium','total_pph21','total_insentif'));
    }

   

    private function gradeToPersen($grade){
        if($grade >= 12){
            return 50;
        } elseif($grade == 11){
            return 45;
        } elseif($grade == 10){
            return 40;
        } else {
            return 35;
        }
    }

    public function generate()
    {

        ini_set('max_execution_time', 300);
        // dd(Remunerasi::where('tahun', date('Y'))->where('bulan',date('m',strtotime('-1 Months')))->first());
        $check = Remunerasi::where('tahun', date('Y'))->where('bulan',date('m',strtotime('-1 Months')))->first();
        
        // dd($check);
        if(!empty($check)){
            return redirect()->back()->with('status','data sudah ada');
        }else{

        $pir = PirModel::where('tahun', date("Y"))->first();
        $pegawai = User::where('role',3)->get();
      
        // $masa_kerja = 100;
        

        $total_diterima = 0;
        $total_honorium = 0;
        $total_pph21    = 0;
        $total_insentif = 0;

        foreach($pegawai as $pegawai_item){
            $nilai_skp = rand(90,100);
            $nilai_absen =  rand(95,100);
            $total_tahun_kerja  = date('Y',strtotime($pegawai_item->tmt_cpns));
            $hitung_masa_kerja  = date('Y') - $total_tahun_kerja;
            if($hitung_masa_kerja >= 10){
                $total_masa_kerja = 20;
            }else{
                $total_masa_kerja = 15;
            }

            
            // print_r($total_masa_kerja);exit();die();
            // dd($this->gradeToPersen($pegawai_item->getJabatan->grade));
            $masa_kerja         = $this->gradeToPersen($pegawai_item->getJabatan->grade_baru) + $pegawai_item->getGolongan->persen + $total_masa_kerja;
            $job_value          = $pegawai_item->getJabatan->nilai_jabatan;
            // dd($job_value);
            $pph21              = $pegawai_item->getGolongan->persen_pph;
            $remunerasi_100     = $job_value*$pir->pir;
            // untuk P1
            $gaji_max           = $remunerasi_100*0.3;
            $gaji_diterima      = $gaji_max * $masa_kerja/100 * $nilai_absen/100;

            // untuk P2
            $insentif_max       = $remunerasi_100 * 0.7;
            $insentif_kinerja_diterima    = $insentif_max * $nilai_skp/100; // asumsu skp 100 dan di ambil dari spk_realisasi_header
            $total_terima_sebelum_pph = $insentif_kinerja_diterima + $gaji_diterima;
            $hasilpph           = $total_terima_sebelum_pph * $pegawai_item->getGolongan->persen_pph/100;
            $jumlah_diterima    = $total_terima_sebelum_pph - $hasilpph;
           
            $data = [
                'nama' => $pegawai_item->nama, 
                'jabatan'=> $pegawai_item->getJabatan->jabatan_baru,
                'grade' => $pegawai_item->getJabatan->grade_baru,
                'nilai_jabatan'=>$job_value,
                'bobot_grade'=> $masa_kerja,
                'bobot_absensi'=> $nilai_absen,
                'gaji_maks'=> $gaji_max,
                'honorarium'=> $gaji_diterima,
                'insentif_kinerja' => $insentif_max,
                'insentif_kinerja_diterima'=> $insentif_kinerja_diterima,
                'total_nilai_skp'=> $nilai_skp,
                'jumlah_remunerasi'=> $remunerasi_100,
                'pph'=> $pph21,
                'pph_jumlah'=> $hasilpph,
                'jumlah_diterima'=>$jumlah_diterima,
                'tahun'=> date("Y"),
                'bulan'=> date('m',strtotime('-1 Months'))
            ];
            // dd($data);
            // INPUT DATA PERULANGAN
            $id = Remunerasi::create($data);
            $id_last = $id->id;
         
            //PERHITUNGAN TOTAL
            $total_diterima     += $jumlah_diterima;
            $total_honorium     += $gaji_diterima;
            $total_pph21        += $hasilpph;
            $total_insentif     += $insentif_max;

        }
        
        RemunHeaderModel::create([
            'id_remunerasi' => $id_last,
            'total_diterima' => $total_diterima,
            'total_honorium' => $total_honorium,
            'total_pph21' => $total_pph21,
            'total_insentif' => $total_insentif,
            'tahun'=> date("Y"),
            'bulan'=> date('m',strtotime('-1 Months'))
        ]);

        return redirect()->route('remunerasi')->with('sukses','Berhasil generate Remunerasi Bulan '.date('m',strtotime('-1 Months')).'');
     }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   
        $check = Remunerasi::where('tahun', date('Y'))
        ->where('bulan',date("m"))->get();
        if(!empty($check)){
            return redirect()->back()->with('status','data sudah ada');
        }

        foreach(request()->pegawai as $id_users => $row){
            dd( sum($row['jumlah_diterima']));
            Remunerasi::create([
                'nama' => $row['nama'],
                'jabatan' => $row['jabatan'],
                'grade' => $row['grade'],
                'nilai_jabatan' => $row['nilai_jabatan'],
                'bobot_grade' => $row['bobot_grade'],
                'bobot_absensi' => $row['bobot_absensi'],
                'gaji_maks' => $row['gaji_maks'],
                'honorarium' => $row['honorarium'],
                'insentif_kerja' => $row['insentif_kerja'],
                'total_nilai_skp' => $row['total_nilai_skp'],
                'bulan' => date('m',strtotime('-1 Months')),
                'tahun' => date('Y'),
                'jumlah_remunerasi' => $row['jumlah_remunerasi'],
                'pph' => $row['pph'],
                'pph_jumlah' => $row['pph_jumlah'],
                'jumlah_diterima' => $row['jumlah_diterima']
            ]);
           
        }
               

        return redirect()->route('remunerasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($bulan)
    {
        // dd($bulan);
        $data =DB::table('remunerasi_header')
        ->join('remunerasi', 'remunerasi_header.bulan', '=', 'remunerasi.bulan')
        ->select('remunerasi_header.*','remunerasi.*')
        ->where('remunerasi_header.bulan', '=', $bulan)
        ->get();

        // dd($data);
        return view('remunerasi.data-remunerasi',compact('data'));
    }

    public function adjustment(Request $request)
    {
        $bulan = $request->input('bulan');
        $nilai = $request->input('adjustment');

        $data = Remunerasi::where('bulan', $bulan)->get();
        
        foreach($data as $remunerasi){
            // print_r($remunerasi->nama);
            // print_r($remunerasi->id_remunerasi);
            // exit();
            // die();
            $adjustment = $nilai/100*$remunerasi->insentif_kinerja;
            DB::table('remunerasi')->where('id_remunerasi',$remunerasi->id_remunerasi)->update([
                'insentif_kinerja_diterima' => $adjustment
                ]);
        }

    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
