<?php

namespace App\Http\Controllers;
use App\Models\Jabatan;
use App\Models\Grade;
use Illuminate\Http\Request;

class masterJabatanController extends Controller
{
    // public function mjabatan(Request $request){
    //     return view('datajabatan.tabel-jabatan');
    // }

    public function mjabatan()
    {
        $jabatan = Jabatan::all();
        return view('datajabatan.tabel-jabatan',compact('jabatan'));
    }

    public function formjabatan(Request $request)
    {
        $grade = Grade::pluck('grade', 'id');

        return view('datajabatan.form-jabatan',['grade' => $grade]);
    }

    
    public function savejabatan(Request $request)
    {
        $jabatan = new Jabatan();
        $jabatan->jabatan_baru = $request->input('jabatan_baru');
        $jabatan->grade_baru = $request->input('grade_baru');
        $jabatan->kelas_jabatan = $request->input('kelas_jabatan');
        $jabatan->nilai_jabatan = $request->input('nilai_jabatan');
        $jabatan->gapok = $request->input('gapok');
        $jabatan->save();

        return redirect('masterJabatan');
    }

    public function edit($id)
    {
        $grade = Grade::pluck('grade', 'id');
        $jabatan = Jabatan::find($id);
        return view('datajabatan.edit-jabatan',['grade' => $grade],compact('jabatan','id'));
    }

    public function update(Request $request, $id)
    {
        $jabatan= Jabatan::find($id);
        $jabatan->jabatan_baru = $request->get('jabatan_baru');
        $jabatan->grade_baru = $request->get('grade_baru');
        $jabatan->kelas_jabatan = $request->get('kelas_jabatan');
        $jabatan->nilai_jabatan = $request->get('nilai_jabatan');
        $jabatan->gapok = $request->get('gapok');
        $jabatan->save();

        return redirect('masterJabatan');
    }
}
