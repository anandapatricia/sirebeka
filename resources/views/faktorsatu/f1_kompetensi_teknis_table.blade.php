@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
td{
    text-align : center;
}

th{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Master Data</h1>
            <ul>
                <li>Faktor Evaluasi</li>
                <li>Kompetensi Teknis</li>
            </ul>
    </div>
        <div class="separator-breadcrumb border-top"></div>

            <!-- end of row -->

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="table-responsive">
                                <a href="{{ route('f1_kompetensi_teknis') }}" class="btn btn-primary">TAMBAH</a><br><br>
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th colspan="4">FAKTOR #1 : KOMPETENSI TEKNIS</th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">TINGKAT</th>
                                            <th>NILAI</th>
                                            <th>AKSI</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($f1_kompetensi_teknis as $f1_kompetensi_teknis)
                                        <tr>
                                            <td>{{ strtoupper($f1_kompetensi_teknis['jenis']) }}</td>
                                            <td>{{ strtoupper($f1_kompetensi_teknis['tipe']) }}</td>
                                            <td>{{ strtoupper($f1_kompetensi_teknis['nilai']) }}</td>
                                            <td>
                                                <a href="{{ route('f1edit', $f1_kompetensi_teknis['id']) }}" class="btn btn-success btn-sm m-1" type="button">Edit</a>
                                                <!-- <button class="btn btn-danger btn-sm m-1" type="button">Delete</button> -->
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

            <!-- end of row

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
