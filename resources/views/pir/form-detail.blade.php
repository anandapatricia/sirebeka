@extends('layouts.master')
@section('before-css')
@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>PIR Formula</h1>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- <h4>Pilih Waktu Remunerasi</h4> -->
            <!-- <p>Masukan Nomor NIP atau NIK Anda!</p> -->
            <div class="card mb-12">
                <div class="card-body table-responsive">
                   <form action="{{route('pir.save')}}" method="post">
                   @csrf
                   @php 
                        $a = $pir->proyeksi_pnbp;
                        $b = $pir->proyeksi_pnbp * 0.4;
                        $c = $pir->penyetaraan;
                        $d = $pir->bpjs_non_pns;
                        $e = $pir->uang_makan_non_pns;
                        $f_ketua = $pir->honorarium_ketua;
                        $f_anggota = $pir->honorarium_anggota;
                        $f_sekretaris = $pir->honorarium_sekertaris;
                        $f = $f_ketua + $f_sekretaris + $f_anggota;
                        $g = $c + $d + $e + $f;
                        $h = $b - $g;
                        $i = $h / 13;
                        $j = $total->total_job_value;
                        $k = $i / $j;
                        $l = round($k);
                        
                   @endphp
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="50">No</th>
                                <th colspan="2">Keteriangan</th>
                                <th>REF</th>
                                <th class="text-right">Nominal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td colspan="2">Proyeksi PNBP {{ $pir->tahun }}</td>
                                <td>A</td>
                                <td class="text-right">{{ number_format($pir->proyeksi_pnbp)}}</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td colspan="2">Pagu remunerasi 40%</td>
                                <td>B</td>
                                <td class="text-right">{{ number_format($b )}}</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td colspan="2">Penyetaraan</td>
                                <td>C</td>
                                <td class="text-right">{{ number_format($pir->penyetaraan)}}</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td colspan="2">BPJS Non PNS</td>
                                <td>D</td>
                                <td class="text-right">{{ number_format($pir->bpjs_non_pns)}}</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td colspan="2">Uang Makan Non PNS</td>
                                <td>E</td>
                                <td class="text-right">{{ number_format($pir->uang_makan_non_pns)}}</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td rowspan="3">Honorarium Dewas</td>
                                <td>Ketua</td>
                                <td rowspan="3">F</td>
                                <td class="text-right">{{ number_format($pir->honorarium_ketua)}}</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>Anggota</td>
                                <td class="text-right">{{ number_format($pir->honorarium_anggota)}}</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>Sekretaris</td>
                                <td class="text-right">{{ number_format($pir->honorarium_sekertaris)}}</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td colspan="2">Jumlah</td>
                                <td>G = C + D + E + F</td>
                                <td class="text-right">{{ number_format($pir->jumlah)}}</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td colspan="2">Kebutuhan remunerasi 1th + Remunerasi ke-13</td>
                                <td>H = B - G</td>
                                <td class="text-right">{{ number_format($pir->kebutuhan_remun_year)}}</td>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td colspan="2">Kebutuhan remunerasi 1 bulan</td>
                                <td>I = H / 13</td>
                                <td class="text-right">{{ number_format($pir->kebutuhan_remun_month,2)}}</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td colspan="2">Total Job Value</td>
                                <td>J</td>
                                <td class="text-right">{{ number_format($pir->job_value)}}</td>
                            </tr>
                            <tr>
                                <td>13</td>
                                <td colspan="2">Point Index Rupiah</td>
                                <td>K = H / J</td>
                                <td class="text-right">{{ number_format($pir->point_index,2)}}</td>
                            </tr>
                            <tr>
                                <td>14</td>
                                <td colspan="2">Point Index Rupiah (dibulatkan)</td>
                                <td>L</td>
                                <td class="text-right">{{ number_format(round($pir->pir))}}</td>
                            </tr>
                        </tbody>
                    </table>
				   </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
@endsection

@section('bottom-js')




@endsection
