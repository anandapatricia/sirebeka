@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>PIR Formula</h1>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- <h4>Pilih Waktu Remunerasi</h4> -->
            <!-- <p>Masukan Nomor NIP atau NIK Anda!</p> -->
            <div class="card mb-12">
                <div class="card-body">
                   <form action="{{route('pir.save')}}" method="post">
                   @csrf
				   <div class="row row-xs">
                        <div class="col-md-3">
                            <label for="">Proyeksi PNBP</label>
                            {{ Form::text('proyeksi_pnbp',$form_pir->proyeksi_pnbp, ['class'=>'form-control']) }}
                        </div>
                        <div class="col-md-3">
                            <label for="">Penyetaraan</label>
                            {{ Form::text('penyetaraan', $form_pir->pennyetaraan, ['class'=>'form-control']) }}
                        </div>
                        <div class="col-md-3">
                            <label for="">BPJS Non PNS</label>
                            {{ Form::text('bpjs_non_pns', $form_pir->bpjs_non_pns, ['class'=>'form-control']) }}
                        </div>
                        <div class="col-md-3">
                            <label for="">Uang Makan Non PNS</label>
                            {{ Form::text('uang_makan_non_pns', $form_pir->uang_makan_non_pns, ['class'=>'form-control']) }}
                        </div>
                       
                    </div>
                    <div class="row row-xs">
                      <div class="col-md-3">
                            <label for="">Honorarium Dewas Ketua</label>
                            {{ Form::text('honorarium_ketua', $form_pir->honorarium_ketua, ['class'=>'form-control']) }}
                        </div>
                        <div class="col-md-3">
                            <label for="">Honorarium Dewas Anggota</label>
                            {{ Form::text('honorarium_anggota',$form_pir->honorarium_anggota, ['class'=>'form-control']) }}
                        </div>
                        <div class="col-md-3">
                            <label for="">Honorarium Dewas Sekertaris</label>
                            {{ Form::text('honorarium_sekertaris', $form_pir->honorarium_sekertaris, ['class'=>'form-control']) }}
                        </div>
                        <div class="col-md-3">
                            <label for="">Tahun</label>
                            {{ Form::text('tahun', $form_pir->tahun, ['class'=>'form-control']) }}
                        </div>
                    </div>
                   <div class="row row-xs">
                   <div class="col-md-12 mt-4">
                            <button type="submit" class="btn btn-primary btn-block">Kirim!</button>
                    </div>
                   </div>


                  
				   </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Tahun</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach( $pir as $pir )
                                        <tr>
                                            <td>{{ strtoupper($pir['tahun']) }} </td>
                                            <td>
                                                <a href="{{ route('pir.edit', $pir['id']) }}" class="btn btn-warning btn-sm m-1">Edit</a>
                                                <a href="{{ route('pir.detail', $pir['id']) }}" class="btn btn-success btn-sm m-1">Detail</a>
                                                <a href="{{ route('pir.delete', $pir['id']) }}" class="btn btn-danger btn-sm m-1">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
				<script src="{{asset('assets/js/datatables.script.js')}}"></script>
@endsection

@section('bottom-js')




@endsection
