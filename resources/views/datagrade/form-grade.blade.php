@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
   <div class="breadcrumb">
                <h1>Tambah Grade</h1>
                <form method="POST" action="{{ route('savegrade') }}">
                @csrf
                <ul>
                    <li><a href="">Master Data</a></li>
                    <li>Master Grade</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Form Input Grade</div>
                            <form >
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="grade">Nilai Grade</label>
                                        <input type="text" class="form-control" name="grade" id="grade" placeholder="Masukan Nilai Grade">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="persentase">Persentase</label>
                                        <input type="text" class="form-control" name="persentase" id="persentase" placeholder="Masukan Nilai Persentase">
                                    </div>

                                    <div class="col-md-12">
                                         <button type="submit" class="btn btn-primary">Simpan</button>
                                         <a href="{{ route('mgrade') }}" class="btn btn-warning">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
