
    <thead>
        <tr>
            <th>GRADE</th>
            <th>PERSENTASE</th>
            <th>AKSI</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $grade as $grade )
        <tr>
            <td>{{ $grade['grade'] }}</td>
            <td>{{ $grade['persentase'] }}%</td>
            <td>
                <a href="{{ route('editgrade', $grade['id']) }}" class="btn btn-success btn-sm m-1" type="button">Edit</a>
            </td>
        </tr>
        @endforeach
    </tbody>
