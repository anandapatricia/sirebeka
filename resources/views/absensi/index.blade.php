@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('main-content')
<div class="breadcrumb">
   <h1>Import Absensi</h1>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
   <div class="col-lg-12 col-md-12">
     
      	
            @if(!empty(Session::get('status')))
                                 <div class="alert alert-danger" role="alert"><strong class="text-capitalize">Peringatan !</strong> 
                                 Data Bulan {{date('F-Y',strtotime('-1 Months'))}} Sudah Di Remunerasi !
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                 </div>
            @elseif(!empty(Session::get('sukses')))
                         <div class="alert alert-success" role="alert"><strong class="text-capitalize">Pemberitahuan !</strong> 
                                 Data Bulan {{date('F-Y',strtotime('-1 Months'))}} Berhasil Di Remunerasi !
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                 </div>
            @endif
  
            <div class="card mb-12">
               <div class="card-body">
               <h4>Tools Import Absensi</h4>
                  <form method="POST" action="{{ route('importabsensi.imports') }}"  enctype="multipart/form-data">
                  {{ csrf_field() }}
                     <div class="row row-xs">
                        <div class="col-md-10">
                        <code>Peringatan !, File yang diupload harus berekstensi : csv,xls,xlsx</code>
                           <input type="file" name="file" class="form-control">
                        </div>
                        <div class="col-md-2">
                              <label for=""></label>
                           <button type="submit" class="btn btn-success btn-block">Import !</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="card mb-12">
               <div class="card-body">
            <div class="breadcrumb">
               <h1>Absensi</h1>
            </div>
               <table class="table table-bordered yajra-datatable">
                  <thead>
                        <tr>
                           <th>No</th>
                           <th>NAMA</th>
                           <th>NIP</th>
                           <th>TANGGAL</th>
                           <th>MASUK</th>
                           <th>KELUAR</th>
                         
                        </tr>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
           
            </div> </div> </div>
</div>


@endsection
@section('page-js')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
  $(function () {
    
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('importabsensi.data.absensi') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'nama', name: 'nama'},
            {data: 'nip', name: 'nip'},
            {data: 'tanggal_absen', name: 'tanggal_absen'},
            {data: 'clockin', name: 'clockin'},
            {data: 'clockout', name: 'clockout'},
        ]
    });
    
  });
</script>
@endsection
@section('bottom-js')
@endsection