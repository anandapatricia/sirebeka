
    <thead>
        <tr>
            <th>MASA KERJA</th>
            <th>JUMLAH</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $masakerja as $masakerja )
        <tr>
            <td>{{ strtoupper($masakerja['masa_kerja']) }}</td>
            <td>{{ strtoupper($masakerja['nilai']) }}</td>
            <td>
                <a href="{{ route('editmasakerja', $masakerja['id']) }}" class="btn btn-success btn-sm m-1" type="button">Edit</a>
            </td>
        </tr>
        @endforeach
    </tbody>
