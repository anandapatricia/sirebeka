@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
   <div class="breadcrumb">
                <h1>Tambah Masa Kerja</h1>
                <form method="POST" action="{{ route('updatemasakerja', $masakerja['id']) }}">
                @csrf
                <ul>
                    <li><a href="">Master Data</a></li>
                    <li>Masa Kerja</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Form Masa Kerja</div>
                            <form >
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="masa_kerja">Masa Kerja</label>
                                        <input type="text" class="form-control" name="masa_kerja" id="masa_kerja" placeholder="" value="{{ $masakerja->masa_kerja }}">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="nilai">Nilai</label>
                                        <input type="text" class="form-control" name="nilai" id="nilai" placeholder="Masukan Nilai %" value="{{ $masakerja->nilai }}">
                                    </div>

                                    <div class="col-md-12">
                                         <button type="submit" class="btn btn-primary">Ubah</button>
                                         <a href="{{ route('mmasakerja') }}" class="btn btn-warning">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection