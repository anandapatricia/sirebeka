@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
            <div class="breadcrumb">
                <h1>Masa Kerja</h1>
                <ul>
                    <li><a href="">Master Data</a></li>
                    <li>Masa Kerja</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="table-responsive">
                            <a href="{{ route('formmasakerja') }}" class="btn btn-primary">TAMBAH</a><br><br>
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>MASA KERJA</th>
                                            <th>JUMLAH</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach( $masakerja as $masakerja )
                                        <tr>
                                            <td>{{ strtoupper($masakerja['masa_kerja']) }} Tahun</td>
                                            <td>{{ strtoupper($masakerja['nilai']) }}%</td>
                                            <td>
                                                <a href="{{ route('editmasakerja', $masakerja['id']) }}" class="btn btn-success btn-sm m-1" type="button">Edit</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
