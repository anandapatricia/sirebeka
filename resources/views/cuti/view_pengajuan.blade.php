<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="styles/style.css" rel="stylesheet" type="text/css">
    <title>Permohonan Pengajuan Cuti</title>

    <style>

        .data{
            margin: 0px 0px 20px 40px;
            border: 1px solid black;
            border-collapse: collapse;
        }

        .td {
            text-align: center;
        }

        /* th {
            margin: 0px 0px 20px 40px;
            border: 1px solid black;
            border-collapse: collapse;
        } */
    </style>

</head>

<body onload="window.print()">
    <div class="border" style="border: 1px solid black; padding: 10px 10px 200px 10px; margin: 0px 0px 0px 0px;"> <br><br><br>
        <table>
            <table align="right">
                <tr>
                    <td>Jakarta, {{ date("d F Y", strtotime($cuti['created_at']))}}</td>
                </tr>
                <tr>
                    <td>Kepada</td>
                </tr>
                <tr>
                    <td>Yth. Ketua Sekolah Tinggi Ilmu Pelayaran </td>
                </tr>
                <tr>
                    <td>di Jakarta</td>
                </tr>
            </table>
                <br><br><br><br><br>
            <table style="width:90%" class="data">
                <tr class="data">
                    <center><h3>PERMOHONAN PENGAJUAN CUTI</h3></center>
                </tr>

                <tr class="data">
                    <td><b>I. DATA PEGAWAI</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr class="data">
                    <td>Nama</td>
                    <td>:</td>
                    <td>{{ ($cuti['nama']) }}</td>
                    <td >NIP</td>
                    <td>:</td>
                    <td>{{ ($cuti['nip']) }}</td>
                </tr>

                <tr class="data">
                    <td>Jabatan</td>
                    <td>:</td>
                    <td>{{ ($cuti['jabatan']) }}</td>
                    <td>Masa Kerja</td>
                    <td>:</td>
                    <td>{{ ($cuti['masakerja']) }}</td>
                </tr>

                <tr>
                    <td>Unit Kerja</td>
                    <td>:</td>
                    <td>Sekolah Tinggi Ilmu Pelayaran</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>

            <table style="width:90%" class="data">
                <tr class="data">
                    <td><b>II. JENIS CUTI YANG DIAMBIL</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr class="data">
                    <td><input type="checkbox" {{ ($cuti['jeniscuti'] == 'Cuti Tahunan' ? 'checked':'') }}>Cuti Tahunan</td>
                    <td></td>
                    <td><input type="checkbox" {{ ($cuti['jeniscuti'] == 'Cuti Besar' ? 'checked':'') }}>Cuti Besar</td>
                    <td></td>
                </tr>

                <tr class="data">
                    <td><input type="checkbox" {{ ($cuti['jeniscuti'] == 'Cuti Sakit' ? 'checked':'') }}>Cuti Sakit</td>
                    <td></td>
                    <td><input type="checkbox" {{ ($cuti['jeniscuti'] == 'Cuti Melahirkan' ? 'checked':'') }}>Cuti Melahirkan</td>
                    <td></td>
                </tr>

                <tr class="data">
                    <td><input type="checkbox" {{ ($cuti['jeniscuti'] == 'Cuti Alasan Penting' ? 'checked':'') }}>Cuti Karena Alasan Penting</td>
                    <td></td>
                    <td><input type="checkbox" {{ ($cuti['jeniscuti'] == 'Cuti Diluar Tanggungan Negara' ? 'checked':'') }}>Cuti Diluar Tanggungan Negara</td>
                    <td></td>
                </tr>
            </table>

            <table style="width:90%" class="data">
                <tr class="data">
                    <td><b>III. ALASAN CUTI</b></td>
                </tr>

                <tr>
                    <td>{{ ($cuti['alasan']) }}</td>
                </tr>
            </table>

            <table style="width:90%" class="data">
                <tr class="data">
                    <td><b> IV. LAMANYA CUTI</b></td>
                </tr>

                <tr>
                    <td>{{ (date("d F Y", strtotime($cuti['tanggalcuti']))) }} sampai {{ (date("d F Y", strtotime($cuti['tanggal']))) }}</td>
                </tr>
            </table>

            <table style="width:90%" class="data">
                <tr class="data">
                    <td><b>ALAMAT SELAMA MENJALANKAN CUTI</b></td>
                </tr>

                <tr>
                    <td>{{ ($cuti['alamat']) }}</td>
                </tr>
            </table>

            <table width="250" align="right">
                <tr>
                    <td class="td">Hormat saya, </td>
                </tr>
                <tr>
                    <td><br><br><br></td>
                </tr>
                <tr>
                    <td class="td">{{ ($cuti['nama']) }}</td>
                </tr>
                <tr>
                    <td class="td">NIP. {{ ($cuti['nip']) }}</td>
                </tr>
            </table>
        </table>
    </div>
</body>
</html>
