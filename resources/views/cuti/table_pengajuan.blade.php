
@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
td{
    text-align : center;
}

th{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Form Pengajuan Cuti</h1>
            <ul>
                <li>Cuti</li>
            </ul>
    </div>
        <div class="separator-breadcrumb border-top"></div>

            <!-- end of row -->

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                          @if(Session::get('notif'))
                            <div class="alert alert-success" role="alert"><strong class="text-capitalize">Success!</strong> {{ Session::get('notif') }}.
                                <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            </div>
                           @endif

                            <div class="table-responsive">
                                <a href="{{ route('form_pengajuan') }}" class="btn btn-primary">BUAT PENGAJUAN</a><br><br>
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th colspan="11">FORM PENGAJUAN CUTI</th>
                                        </tr>
                                        <tr>
                                            <th>Nama</th>
                                            <th>NIP</th>
                                            <th>Jabatan</th>
                                            <th>Masa Kerja</th>
                                            <th>Unit Kerja</th>
                                            <th>Jenis Cuti</th>
                                            <th>Alasan</th>
                                            <th>Tanggal Cuti</th>
                                            <th>Alamat</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($list_cuti as $cuti)
                                        <tr>

                                            <td>{{ strtoupper($cuti['nama']) }}</td>
                                            <td>{{ strtoupper($cuti['nip']) }}</td>
                                            <td>{{ strtoupper($cuti['jabatan']) }}</td>
                                            <td>{{ strtoupper($cuti['masakerja']) }}</td>
                                            <td>{{ strtoupper($cuti['unitkerja']) }}</td>
                                            <td>{{ strtoupper($cuti['jeniscuti']) }}</td>
                                            <td>{{ strtoupper($cuti['alasan']) }}</td>
                                            <td>{{ strtoupper($cuti['tanggalcuti']) }} sampai {{ strtoupper($cuti['tanggal']) }}</td>
                                            <td>{{ strtoupper($cuti['alamat']) }}</td>
                                            <td>
                                                <a href="{{ route('view_pengajuan',$cuti->id) }}" target="_blank" class="btn btn-info btn-sm m-1">View</a>
                                                @if($cuti['status'] == 0)
                                                  <a href="{{ route('edit_pengajuan',$cuti->id) }}" class="btn btn-warning btn-sm m-1">Edit</a>
                                                  <a href="{{ route('delete_pengajuan',$cuti->id) }}" class="btn btn-danger btn-sm m-1">Delete</a>
                                                  <a href="{{ route('cuti_kirim',$cuti->id) }}" class="btn btn-success btn-sm m-1">Kirim</a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- end of row

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
