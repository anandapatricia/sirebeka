@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
/* td{
    text-align : center;
} */

th{
    text-align : center;
}
h2{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Form Pengajuan Cuti</h1>
            <ul>
                <li>Cuti</li>
            </ul>
    </div>
        <div class="separator-breadcrumb border-top"></div>

            <body>
                <div class="row mb-4">
                    <div class="col-md-12 mb-4">
                        <div class="card text-left">
                            <div class="card-body">
                                <h2>FORM PENGAJUAN CUTI</h2><br/>
                                <form method="post" action="{{ route('pengajuan_update', $cuti['id']) }}">
                                @csrf
                                    <div class="row">
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="nama">Nama</label>
                                            <input class="form-control" name="nama" id="nama" type="text" value="{{$cuti->nama}}" required />
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="nip">NIP</label>
                                            <input class="form-control" name="nip" id="nip" type="text" value="{{$cuti->nip}}" required />
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="jabatan">Jabatan</label>
                                            <input class="form-control" name="jabatan" id="jabatan" type="text" value="{{$cuti->jabatan}}" required />
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="masakerja">Masa Kerja</label>
                                            <input class="form-control" name="masakerja" id="masakerja" value="{{$cuti->masakerja}}" required />
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="unitkerja">Unit Kerja</label>
                                            <input class="form-control" name="unitkerja" id="unitkerja" type="text" value="{{$cuti->unitkerja}}" required />
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="jeniscuti">Jenis Cuti</label>
                                                <select name="jeniscuti" nama="jeniscuti" id="jeniscuti" class="form-control" value="{{$cuti->jeniscuti}}" required>
                                                    <option value="0">Pilih Jenis Cuti</option>
                                                    <option value="Cuti Sakit" >Cuti Sakit</option>
                                                    <option value="Cuti Tahunan">Cuti Tahunan</option>
                                                    <option value="Cuti Besar">Cuti Besar</option>
                                                    <option value="Cuti Melahirkan">Cuti Melahirkan</option>
                                                    <option value="Cuti Alasan Penting">Cuti Karena Alasan Penting</option>
                                                    <option value="Cuti Diluar Tanggungan Negara">Cuti Diluar Tanggungan Negara</option>
                                                </select>
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="alasan">Alasan Cuti</label>
                                            <input class="form-control" name="alasan" id="alasan" type="text" value="{{$cuti->alasan}}" required />
                                        </div>

                                         <div class="col-md-6 form-group mb-3">
                                            <label for="tanggalcuti">Tanggal Cuti</label>
                                            <input class="form-control" name="tanggalcuti" id="tanggalcuti" type="date"value="{{$cuti->tanggalcuti}}" required />
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="tanggal">Sampai Tanggal</label>
                                            <input class="form-control" name="tanggal" id="tanggal" type="date" value="{{$cuti->tanggal}}" required />
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="alamat">Alamat Selama Menjalankan Cuti</label>
                                            <input class="form-control" name="alamat" id="alamat" type="text" value="{{$cuti->alamat}}" required />
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4" style="margin-top:10px">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="{{ route('table_pengajuan') }}" class="btn btn-warning">Kembali</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </body>


@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
