@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
td{
    text-align : center;
}

th{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Master Data</h1>
            <ul>
                <li>Indikator Kinerja Utama</li>
            </ul>
    </div>
        <div class="separator-breadcrumb border-top"></div>

            <body>
                <div class="row mb-4">
                    <div class="col-md-12 mb-4">
                        <div class="card text-left">
                            <div class="card-body">
                                <h2>INDIKATOR KINERJA UTAMA</h2><br/>
                                <form method="post" action="{{ route('ikuupdate', $iku['id']) }}">
                                @csrf
                                    <div class="row">
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="tahun">TAHUN:</label>
                                            <input class="form-control" name="tahun" id="tahun" type="text" value="{{$iku->tahun}}" required/>
                                        </div>

                                         <div class="col-md-6 form-group mb-3">
                                            <label for="iku">IKU:</label>
                                            <input class="form-control" name="iku" id="iku" type="text" value="{{$iku->iku}}" required />
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4" style="margin-top:10px">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="{{ route('ikutable') }}" class="btn btn-warning">Kembali</a>
                                        </div>
                                    </div>
                                </form>      
                            </div>
                        </div>
                    </div>
                </div>
            </body>


@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
