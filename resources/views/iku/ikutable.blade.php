@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
/* td{
    text-align : center;
} */

th{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Master Data</h1>
            <ul>
                <li>Indikator Kinerja Utama</li>
            </ul>
    </div>
        <div class="separator-breadcrumb border-top"></div>

            <!-- end of row -->

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="table-responsive">
                                <a href="{{ route('iku') }}" class="btn btn-primary">TAMBAH</a><br><br>
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>TAHUN</th>
                                            <th colspan="">Indikator Kinerja Utama</th>
                                            <th>AKSI</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($iku as $iku)
                                        <tr>
                                            <td>{{ strtoupper($iku['tahun']) }}</td>
                                            <td>{{ strtoupper($iku['iku']) }}</td>
                                            <td align="center">
                                                <a href="{{ route('ikuedit', $iku['id']) }}" class="btn btn-success btn-sm m-1" type="button">Edit</a>
                                                <!-- <button class="btn btn-danger btn-sm m-1" type="button">Delete</button> -->
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

            <!-- end of row

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
