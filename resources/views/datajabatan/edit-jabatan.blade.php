@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
   <div class="breadcrumb">
                <h1>Tambah Jabatan</h1>
                <form method="POST" action="{{ route('updatejabatan', $jabatan['id']) }}">
                @csrf
                <ul>
                    <li><a href="">Master Data</a></li>
                    <li>Msster Jabatan</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Form Input Jabatan</div>
                            <form >
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="jabatan_baru">Jabatan Baru</label>
                                        <input type="text" class="form-control" name="jabatan_baru" id="jabatan_baru" placeholder="Masukan Nama Jabatan Sesuai Peta Jabatan Baru" value="{{$jabatan->jabatan_baru}}">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="grade_baru">Grade Baru</label>
                                        <select class="form-control" name="grade_baru">
                                            @foreach ($grade as $id => $grade)
                                                <option value="{{ $id }}">{{ $grade }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="kelas_jabatan">Kelas Jabatan</label>
                                        <input type="text" class="form-control" name="kelas_jabatan" id="kelas_jabatan" placeholder="Masukan Kelas Jabatan" value="{{$jabatan->kelas_jabatan}}">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="nilai_jabatan">Nilai Jabata</label>
                                        <input class="form-control" name="nilai_jabatan" id="nilai_jabatan" placeholder="Masukan Nilai Jabatan" value="{{$jabatan->nilai_jabatan}}">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="gapok">Gaji Pokok</label>
                                        <input type="text" class="form-control" name="gapok" id="gapok" placeholder="Masukan Gaji Pokok Sesuai Jabatan" value="{{$jabatan->gapok}}">
                                    </div>

                                    <div class="col-md-12">
                                         <button type="submit" class="btn btn-primary">Ubah</button>
                                         <a href="{{ route('mjabatan') }}" class="btn btn-warning">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
