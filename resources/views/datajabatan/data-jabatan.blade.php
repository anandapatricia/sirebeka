
    <thead>
        <tr>
            <th>JABATAN SESUAI PETA JABATAN BARU</th>
            <th>GRADE BARU</th>
            <th>KELAS JABATAN</th>
            <th>NILAI JABATAN</th>
            <!-- <th>GAJI POKOK</th> -->
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $jabatan as $jabatan )
        <tr>
            <td width="40%">{{ strtoupper($jabatan['jabatan_baru']) }}</td>
            <td>{{ strtoupper($jabatan['grade_baru']) }}</td>
            <td>{{ strtoupper($jabatan['kelas_jabatan']) }}</td>
            <td>{{ strtoupper($jabatan['nilai_jabatan']) }}</td>
            <!-- <td>Rp. {{ number_format($jabatan['gapok'],2,',','.') }}</td> -->
            <td>
                <a href="{{ route('editjabatan', $jabatan['id']) }}" class="btn btn-success btn-sm m-1" type="button">Edit</a>
                <a href="{{ route('tjabatan', $jabatan['id']) }}" class="btn btn-primary btn-sm m-1" type="button">Tambah Tusi</a>
                <a href="{{ route('tugasjabatan', $jabatan['id']) }}" class="btn btn-warning btn-sm m-1" type="button">Lihat Tusi</a>
            </td>
        </tr>
        @endforeach
    </tbody>
