@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
td{
    text-align : center;
}

th{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Master Data</h1>
            <ul>
                <li>Faktor Evaluasi</li>
                <li>Kondisi Kerja</li>
            </ul>
    </div>
        <div class="separator-breadcrumb border-top"></div>

            <body>
                <div class="row mb-4">
                    <div class="col-md-12 mb-4">
                        <div class="card text-left">
                            <div class="card-body">
                                <h2>KONDISI KERJA</h2><br/>
                                <form method="post" action="{{ route('f6update', $f6['id']) }}">
                                @csrf
                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="jenis">JENIS:</label>
                                            <input id="jenis" type="jenis" value="{{$f6->jenis}}" class="form-control" name="jenis" >
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="tipe">TIPE:</label>
                                            <input id="tipe" value="{{$f6->tipe}}" class="form-control" name="tipe">
                                        </div>
                                    </div>

                                     <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="kode">KODE:</label>
                                            <input id="kode" value="{{$f6->kode}}" class="form-control" name="kode">
                                        </div>
                                    </div>

                                     <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="kelas">KELAS:</label>
                                            <input id="kelas" value="{{$f6->kelas}}" class="form-control" name="kelas">
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="nilai">NILAI:</label>
                                            <input id="nilai" value="{{$f6->nilai}}" class="form-control" name="nilai">
                                        </div>
                                    </div> 

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4" style="margin-top:10px">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="{{ route('f6table') }}" class="btn btn-warning">Kembali</a>
                                        </div>
                                    </div>
                                </form>      
                            </div>
                        </div>
                    </div>
                </div>
            </body>


@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
