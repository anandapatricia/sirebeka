@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
/* td{
    text-align : center;
} */

th{
    text-align : center;
}
h2{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Form SPT</h1>
            <ul>
                <li></li>
            </ul>
    </div>
        <div class="separator-breadcrumb border-top"></div>

            <body>
                <div class="row mb-4">
                    <div class="col-md-12 mb-4">
                        <div class="card text-left">
                            <div class="card-body">
                                <h2>FORM SPT</h2><br/>
                                <form method="POST" action="{{ route('save_spt')}}">
                                @csrf
                                    <div class="row">
                                      <div class="col-md-6 form-group mb-3">
                                          <label for="tanggal_surat">Tanggal Surat</label>
                                          <input class="form-control" name="tanggal_surat" id="tanggal_surat" type="date" required/>
                                      </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="nomor_surat">Nomor Surat</label>
                                            <input class="form-control" name="nomor_surat" id="nomor_surat" type="text" required/>
                                        </div>

                                         <div class="col-md-6 form-group mb-3">
                                            <label for="tanggal_mulai">Tanggal Mulai</label>
                                            <input class="form-control" name="tanggal_mulai" id="tanggal_mulai" type="date" required />
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="tanggal_selesai">Tanggal Selesai</label>
                                            <input class="form-control" name="tanggal_selesai" id="tanggal_selesai" type="date" required />
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="lokasi_spt">Lokasi SPT</label>
                                            <input class="form-control" name="lokasi_spt" id="lokasi_spt" type="text" required />
                                        </div>

                                        <div class="col-md-6 form-group mb-3">
                                            <label for="uraian">uraian</label>
                                            <textarea class="form-control" name="uraian" id="uraian" rows="5" required ></textarea>
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4" style="margin-top:10px">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="{{ route('table_spt') }}" class="btn btn-warning">Kembali</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </body>


@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
