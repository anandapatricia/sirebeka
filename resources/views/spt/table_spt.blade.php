
@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
td{
    text-align : center;
}

th{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Form SPT</h1>
            <ul>
                <li></li>
            </ul>
    </div>
        <div class="separator-breadcrumb border-top"></div>

            <!-- end of row -->

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            @if(Session::get('notif'))
                              <div class="alert alert-success" role="alert"><strong class="text-capitalize">Success!</strong> {{ Session::get('notif') }}.
                                  <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                              </div>
                            @endif
                            <div class="table-responsive">
                                <a href="{{ route('form_spt') }}" class="btn btn-primary">BUAT SPT</a><br><br>
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th colspan="6">SPT</th>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Surat</th>
                                            <th>Nomor Surat</th>
                                            <th>Tanggal Mulai</th>
                                            <th>Tanggal Selesai</th>
                                            <th>Lokasi SPT</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($spt as $spt)
                                        <tr>
                                            <td>{{ strtoupper($spt['tanggal_surat']) }}</td>
                                            <td>{{ strtoupper($spt['nomor_surat']) }}</td>
                                            <td>{{ strtoupper($spt['tanggal_mulai']) }}</td>
                                            <td>{{ strtoupper($spt['tanggal_selesai']) }}</td>
                                            <td>{{ strtoupper($spt['lokasi_spt']) }}</td>
                                            <td>
                                                <a href="{{ route('detail_spt',['id'=>$spt['id']]) }}" class="btn btn-info btn-sm m-1">Detail</a>
                                                @if($spt['status'] == 0)
                                                  <a href="{{ route('edit_spt',['id'=>$spt['id']]) }}" class="btn btn-warning btn-sm m-1">Edit</a>
                                                  <a href="{{ route('delete_spt',['id'=>$spt['id']]) }}" class="btn btn-danger btn-sm m-1">Delete</a>
                                                  <a href="{{ route('kirim_spt',['id'=>$spt['id']]) }}" class="btn btn-success btn-sm m-1">Kirim</a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- end of row

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
