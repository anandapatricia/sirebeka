@extends('layouts.master')
@section('page-css')


<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
<style>
	input{
		border-width:0px;
		border:none;
	
	}
</style>
@endsection

@section('main-content')
  
	<div class="breadcrumb">
		<h1>Data Remunerasi</h1>
	</div>
	<div class="separator-breadcrumb border-top"></div>
	<div class="row mb-4">
		<div class="col-md-12 mb-4">
			<div class="card text-left">
				<div class="card-body">
					<div class="table-responsive">
						<div class="row mb-4">
							<div class="col-md-2">
									</div>
									<!-- <div class="col-md-6">
										<form action="{{ route('syncPegawai') }}" method="post">
                                            @csrf
                                            
											<input type="hidden" name="kodekantor" value="008017000000000">
												<button type="submit" class="btn btn-success" > Sync Data Pegawai</button>
											</form>
										</div> -->
									</div>
									<form action="{{route('remunerasi.save')}}" method="post">
									@csrf
									<!-- &nbsp;&nbsp;<button type="submit" class="btn btn-primary"> Generate</button>
									<br><br><br>
									
									@if(!empty(Session::get('status')))
									<div class="alert alert-danger" role="alert"><strong class="text-capitalize">Peringatan !</strong> 
									Data Bulan {{date('F-Y',strtotime('-1 Months'))}} Sudah Di Remunerasi !
										<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
									</div>
									@endif -->
									<table id="zero_configuration_table" class="display table table-striped table-bordered">
										<thead>
											<tr>
												<th rowspan="3">NAMA</th>
												<th rowspan="3">JABATAN SESUAI PETA JABATAN BARU</th>
												<th rowspan="3">GRADE BARU</th>
												<th rowspan="3">NILAI JABATAN</th>
												<th>Bobot Grade</th>
												<th>Bobot Absensi</th>
												<th>Gaji</th>
												<th>Gaji Pnbp yang</th>
                                                <th rowspan="3">Insentif Kinerja</th>
												<th rowspan="3">Total Nilai SKP</th>
												<th rowspan="3">Insentif Kinerja Yang Diterima</th>
												<th rowspan="3">Jumlah Remunerasi 100%</th>
												<th rowspan="2">Pph Pasal 21</th>
												<th rowspan="3">Jumlah Yang Diterima</th>
											</tr>
                                            <tr>
                                                <th>Pangkat</th>
												<th></th>
												<th>Pnbp</th>
												<th>Diterima /</th>
                                            </tr>   
                                            <tr>
                                                <th>Masa Kerja</th>
												<th></th>
												<th>Maks</th>
												<th>honorarium Dewas</th>
                                                <!-- <th>Pph </th> -->
												<th>Jumlah</th>
                                            </tr>
										</thead>
										<tbody>  
                                       @foreach($data as $row)

                                       <tr style="background:white;">
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][nama]" value="{{$row->nama}}" readonly></td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][jabatan]" value="{{$row->jabatan}}" readonly></td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][grade]" value="{{$row->grade}}" readonly> </td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][nilai_jabatan]" value="{{$row->nilai_jabatan}}" readonly></td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][bobot_grade]" value="{{$row->bobot_grade}}" readonly></td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][bobot_absensi]"	value="{{$row->bobot_absensi}}" readonly></td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][gaji_maks]"	value="Rp. {{ number_format($row->gaji_maks,2,',','.') }}" readonly></td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][honorarium]"	value="Rp. {{ number_format($row->honorarium,2,',','.') }}" readonly> </td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][insentif_kinerja]"	value="Rp. {{ number_format($row->insentif_kinerja,2,',','.') }}" readonly></td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][total_nilai_skp]"	value="{{$row->total_nilai_skp}}" readonly></td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][insentif_kinerja_diterima]"	value="Rp. {{ number_format($row->insentif_kinerja_diterima,2,',','.') }}"readonly> </td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][jumlah_remunerasi]"	value="Rp. {{ number_format($row->jumlah_remunerasi,2,',','.') }}"readonly> </td>
                                             <!-- <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][pph]"	value="{{$row->pph}}"readonly></td> -->
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][pph_jumlah]"	value="Rp. {{ number_format($row->pph_jumlah,2,',','.') }}"readonly> </td>
                                             <td><input type="text" name="pegawai[{{ $row->id_remunerasi }}][jumlah_diterima]"	value="Rp. {{ number_format($row->jumlah_diterima,2,',','.') }}"readonly> </td>
                                        </tr>           
                                       @endforeach               

										</tbody>
										<tfoot>
											<tr>
												<th colspan="6">TOTAL</th>
												<th>Rp. {{ number_format($row->total_honorium,2,',','.') }}</th>
												<th colspan="1"></th>
												<th>Rp. {{ number_format($row->total_insentif,2,',','.') }}</th>
												<th colspan="3"></th>
												<th>Rp. {{ number_format($row->total_pph21,2,',','.') }}</th>
												<th>Rp. {{ number_format($row->total_diterima,2,',','.') }}</th>
											</tr>
										</tfoot>
									</table>
									</form>
								</div>
							</div>
						</div>
						<div class="separator-breadcrumb border-top"></div>
						<div class="card mb-12">
							<div class="card-body">
														
								<div class="breadcrumb">
									<h1>Form Adjustment Remunerasi</h1>
								</div>
								<form action="{{route('remunerasi.adjustment')}}" method='post'>
								@csrf
								<div class="row row-xs">
									<div class="col-md-10">
										<input type="hidden" value="{{$row->bulan}}" name="bulan">
										<input type="number"  class="form-control" name="adjustment"  placeholder="Masukan Nominal Adjustment">
									</div>
									<div class="col-md-2 mt-3 mt-md-0">
										<button type="submit" class="btn btn-success btn-block">Kirim !</button>
									</div>
								</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end of col -->
				</div>
				<!-- end of row -->
@endsection
@section('page-js')
				<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
				<script src="{{asset('assets/js/datatables.script.js')}}"></script>
@endsection
