@extends('layouts.master')
@section('before-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection
@section('main-content')
<div class="breadcrumb">
   <h1>Remunerasi</h1>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
   <div class="col-lg-12 col-md-12">
      <h4>Pilih Waktu Remunerasi</h4>
      	
            @if(!empty(Session::get('status')))
                                 <div class="alert alert-danger" role="alert"><strong class="text-capitalize">Peringatan !</strong> 
                                 Data Bulan {{date('F-Y',strtotime('-1 Months'))}} Sudah Di Remunerasi !
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                 </div>
            @elseif(!empty(Session::get('sukses')))
                         <div class="alert alert-success" role="alert"><strong class="text-capitalize">Pemberitahuan !</strong> 
                                 Data Bulan {{date('F-Y',strtotime('-1 Months'))}} Berhasil Di Remunerasi !
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                 </div>
            @endif
      <!-- <p>Masukan Nomor NIP atau NIK Anda!</p> -->
      <div class="card mb-12">
         <div class="card-body">
            <form action="{{route('remunerasi.generate')}}" method='post'>
            @csrf
               <div class="row row-xs">
                  <div class="col-md-10">
                     <input type="text" value="{{ date('m-Y',strtotime('-1 Months'))}}" readonly class="form-control" id="datepicker1">
                  </div>
                  <div class="col-md-2 mt-3 mt-md-0">
                     <button type="submit" class="btn btn-success btn-block">Generate !</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<div class="row">
   <div class="col-lg-12 col-md-12">
    
      <!-- <p>Masukan Nomor NIP atau NIK Anda!</p> -->
      <div class="card mb-12">
         <div class="card-body">
            <div class="row row-xs">
               <div class="col-md-12">
                  <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                     <thead>
                        <tr>
                           <th>Tahun</th>
                           <th>Bulan</th>
                           <th>Aksi</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($remunerasi as $row)
                        <tr>
                           <td>{{$row->tahun}}</td>
                           <td>{{$row->bulan}}</td>
                           <td><a href="{{route('remunerasi.detail',$row->bulan)}}" class="btn btn-primary">Detail</a></td>
                        </tr>
                        @endforeach               
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('page-js')
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.script.js')}}"></script>
@endsection
@section('bottom-js')
@endsection