@extends('layouts.master')
@section('page-css')

@endsection

@section('main-content')
            <div class="breadcrumb">
                <h1>SKP</h1>
                <ul>
                    <li><a href="">Form Realisasi SKP</a></li>
                    <li>Realisasi</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <td width="150">Nama</td>
                                    <td>{{ auth()->user()->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Nip</td>
                                    <td>{{ auth()->user()->nip }}</td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td>{{ auth()->user()->getJabatan->jabatan_baru }}</td>
                                </tr>
                                <tr>
                                    <td>Periode</td>
                                    <td>{{ $header->bulan.'/'.$header->tahun }}</td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="card-title">Form SKP</div>
                            {{ Form::open(['route' => ['skp.realisasi.save'], 'method'=>'post' ,'files' => true]) }}
                            @csrf
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Indikator</th>
                                            <th>Formula</th>
                                            <th>Uraian</th>
                                            <th>Sumber Data</th>
                                            <th>Periode Penilaian (Trajectory)</th>
                                            <th>Bobot</th>
                                            <th>Target Tahunan</th>
                                            <th>Target Tiap Trajectory</th>
                                            <th>Satuan Target IKU</th>
                                            <th>Realisasi</th>
                                            <th>Upload File</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <form>
                                            @foreach($data as $key=>$skp)
                                                @php 
                                                    if($key == 'IKU'){
                                                        $indikator = 'INDIKATOR KINERJA UTAMA';
                                                    }elseif($key == 'TUSI'){
                                                        $indikator = 'INDIKATOR TUGAS DAN FUNGSI';
                                                    }else{
                                                        $indikator = 'INDIKATOR TUGAS TAMBAHAN';
                                                    }
                                                @endphp
                                                <tr>
                                                    <th colspan="13">{{ $indikator }}</th>
                                                </tr>
                                                @php $total = count($skp); @endphp
                                                @foreach($skp as $skp_item)
                                                    <tr>
                                                        {{ Form::hidden('data[jenis_indikator][]', $skp_item->jenis_indikator) }}
                                                        {{ Form::hidden('data[id_indikator][]', $skp_item->id_indikator) }}
                                                        {{ Form::hidden('data[indikator][]', $skp_item->indikator) }}
                                                        {{ Form::hidden('data[formula][]', $skp_item->formula) }}
                                                        {{ Form::hidden('data[uraian][]', $skp_item->uraian) }}
                                                        {{ Form::hidden('data[sumber][]', $skp_item->sumber) }}
                                                        {{ Form::hidden('data[periode][]', $skp_item->periode) }}
                                                        {{ Form::hidden('data[bobot][]', $skp_item->bobot) }}
                                                        {{ Form::hidden('data[target_tahunan][]', $skp_item->target_tahunan) }}
                                                        {{ Form::hidden('data[trajectory][]', $skp_item->trajectory) }}
                                                        {{ Form::hidden('data[satuan][]', $skp_item->satuan) }}
                                                        {{ Form::hidden('data[satuan][]', $skp_item->realisasi_target) }}
                                                        {{ Form::hidden('data[satuan][]', $skp_item->indesk_kinerja) }}

                                                        <td>{{ strtoupper($skp_item['indikator']) }}</td>
                                                        <td>{{ ucwords($skp_item['formula']) }}</td>
                                                        <td>{{ ucwords($skp_item['uraian']) }}</td>
                                                        <td>{{ ucwords($skp_item['sumber']) }}</td>
                                                        <td>{{ ucwords($skp_item['periode']) }}</td>
                                                        <td class="text-right">{{ number_format($skp_item['bobot']) }}</td>
                                                        <td class="text-right">{{ number_format($skp_item['target_tahunan']) }}</td>
                                                        <td class="text-right">{{ number_format($skp_item['trajectory']) }}</td>
                                                        <td>{{ $skp_item['satuan'] }}</td>
                                                        <td>{{ Form::number('data[realisasi][]', $skp_item->realisasi, ['class'=>'form-control', 'style'=>'width:200px']) }}</td>
                                                        <td>{{ Form::file('data[file][]', $skp_item->file = ['accept'=>'application/pdf', 'title'=>'']) }}</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </form>
                                    </tbody>
                                </table>
                            </div>
                            <!-- <button type="submit" class="btn btn-primary">Simpan</button> -->
                            {{ Form::submit('Simpan', ['class'=>'btn btn-primary ripple m-1']) }}
                            <a href="{{ route('skp') }}" class="btn btn-warning ripple m-1" type="button">Kembali</a>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->

@endsection

@section('page-js')

@endsection