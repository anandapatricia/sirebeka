@extends('layouts.master')
@section('page-css')

@endsection

@section('main-content')
            <div class="breadcrumb">
                <h1>SKP</h1>
                <ul>
                    <li><a href="">Form Realisasi SKP</a></li>
                    <li>Form Realisasi</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <form method="POST" action="{{ route('saveskp', ['id_skp_header' => $header->id]) }}">
                            @csrf
                            <div class="card-body">
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <td width="150">Nama</td>
                                        <td>{{ auth()->user()->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nip</td>
                                        <td>{{ auth()->user()->nip }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jabatan</td>
                                        <td>{{ auth()->user()->getJabatan->jabatan_baru }}</td>
                                    </tr>
                                    <tr>
                                        <td>Periode</td>
                                        <td>{{ $header->bulan.'/'.$header->tahun }}</td>
                                    </tr>
                                </table>

                                <br><br>

                                <div class="table-responsive">
                                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                            <th>Indikator</th>
                                            <th>Formula</th>
                                            <th>Uraian</th>
                                            <th>Sumber Data</th>
                                            <th>Periode Penilaian (Trajectory)</th>
                                            <th>Bobot</th>
                                            <th>Target Tahunan</th>
                                            <th>Target Tiap Trajectory</th>
                                            <th>Satuan Target IKU</th>
                                            <th>Realisasi</th>
                                            <th>Realisasi Terhadap Target</th>
                                            <th>Indesk Kerja</th>
                                            <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no = 1; @endphp
                                                @foreach($data as $key=>$skp)
                                                @php 
                                                    if($key == 'IKU'){
                                                        $indikator = 'INDIKATOR KINERJA UTAMA';
                                                    }elseif($key == 'TUSI'){
                                                        $indikator = 'INDIKATOR TUGAS DAN FUNGSI';
                                                    }else{
                                                        $indikator = 'INDIKATOR TUGAS TAMBAHAN';
                                                    }
                                                @endphp
                                                <tr>
                                                    <th colspan="13">{{ $indikator }}</th>
                                                </tr>
                                                @php $total = count($skp); @endphp
                                                @foreach($skp as $skp_item)
                                                <tr>
                                                    <td>{{ strtoupper($skp_item['indikator']) }}</td>
                                                    <td>{{ ucwords($skp_item['formula']) }}</td>
                                                    <td>{{ ucwords($skp_item['uraian']) }}</td>
                                                    <td>{{ ucwords($skp_item['sumber']) }}</td>
                                                    <td>{{ ucwords($skp_item['periode']) }}</td>
                                                    <td class="text-right">{{ number_format($skp_item['bobot']) }}%</td>
                                                    <td class="text-right">{{ number_format($skp_item['target_tahunan']) }}</td>
                                                    <td class="text-right">{{ number_format($skp_item['trajectory']) }}</td>
                                                    <td>{{ $skp_item['satuan'] }}</td>
                                                    <td>{{ number_format($skp_item['realisasi']) }}</td>
                                                    <td>{{ $skp_item['realisasi_target'] }}%</td>
                                                    <td>{{ $skp_item['indesk_kinerja'] }}%</td>
                                                    <td>
                                                        @if(!empty($skp_item['file']))
                                                            <a href="{{ Storage::url($skp_item['file']) }}" class="btn btn-success btn-sm" target="_blank">Download</a>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @endforeach
                                            <tr>
                                                <th colspan="13"></th>
                                            </tr>
                                            <tr>
                                                <th colspan="5">Indesk Kinerja Ril</th>
                                                <th></th>
                                                <th colspan="5"></th>
                                                <th colspan="2">{{ $header->indesk_ril }}%</th>
                                            </tr>
                                            <tr>
                                                <th colspan="5">Indesk Kinerja Propoesional</th>
                                                <th></th>
                                                <th colspan="5"></th>
                                                <th colspan="2">{{ $header->indesk_proporsional }}%</th>
                                            </tr>
                                            <tr>
                                                <th colspan="5">Indeks Kinerja Proporsional Termasuk Tugas Tambahan</th>
                                                <th></th>
                                                <th colspan="5"></th>
                                                <th colspan="2">{{ $header->indesk_tambahan }}%</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- <button type="submit" class="Indesk Kinerja Rilbtn btn-primary">Simpan</button> -->
                                <a href="{{ route('skp') }}" class="btn btn-warning ripple m-1" type="button">Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- end of col -->

            </div>
            <!-- end of row -->

@endsection

@section('page-js')

@endsection