@extends('layouts.master')
@section('page-css')

@endsection

@section('main-content')
            <div class="breadcrumb">
                <h1>Form SKP</h1>
                <ul>
                    <li><a href="">Tambah SKP</a></li>
                    <li>SKP</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <td width="150">Nama</td>
                                    <td>{{ auth()->user()->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Nip</td>
                                    <td>{{ auth()->user()->nip }}</td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td>{{ auth()->user()->getJabatan->jabatan_baru }}</td>
                                </tr>
                                <tr>
                                    <td>Periode</td>
                                    <td>{{ $header->bulan.'/'.$header->tahun }}</td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-md-6 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="card-title">Tambah IKU</div>
                            <form method="POST" action="{{ route('skp.iku.save') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">   
                                            {{ Form::select('id_iku', $iku, null, ['class'=>'form-control','required'=>true]) }}
                                            <div class="input-group-append">
                                                {{ Form::submit('Tambah', ['class'=>'btn btn-primary ripple m-1']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="card-title">Tugas Tambahan</div>
                            <form method="POST" action="{{ route('skp.tugas.tambahan') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">   
                                            {{ Form::text('tugas_tambahan', $form_skp->tugas_tambahan, ['class'=>'form-control','required'=>true]) }}
                                            <div class="input-group-append">
                                                {{ Form::submit('Tambah', ['class'=>'btn btn-primary ripple m-1']) }}   
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="card-title">Form SKP</div>
                            <form method="POST" action="{{ route('skp.save', $header) }}">
                            @csrf
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="20%">Indikator</th>
                                            <th width="18%">Formula</th>
                                            <th>Uraian</th>
                                            <th>Sumber Data</th>
                                            <th>Periode Penilaian (Trajectory)</th>
                                            <th>Bobot</th>
                                            <th>Target Tahunan</th>
                                            <th>Target Tiap Trajectory</th>
                                            <th width="8%">Satuan Target IKU</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <form>
                                            <tr>
                                                <th colspan="10">Indikator Kinerja Utama</th>
                                            </tr>
                                            @foreach($skp_iku as $item)
                                            <tr>
                                                <td>
                                                    {{ Form::hidden('data[jenis_indikator][]', 'IKU') }}
                                                    {{ Form::hidden('data[id_indikator][]', $item->id_iku) }}
                                                    {{ Form::textarea('data[indikator][]', $item->get_iku->iku, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::textarea('data[formula][]', $form_skp->formula, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::textarea('data[uraian][]', $form_skp->uraian, ['class'=>'form-control']) }}
                                                </td>
                                                <td>
                                                    {{ Form::textarea('data[sumber][]', $form_skp->sumber, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::select('data[periode][]',[
                                                        ''=>'Pilih Periode',
                                                        'Bulanan' => 'Bulanan',
                                                        'Triwulan' => 'Triwulan',
                                                        'Caturwulan' => 'Caturwulan',
                                                        'Tahunan' => 'Tahunan',
                                                        'Semester' => 'Semester',
                                                    ], $form_skp->periode, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::text('data[bobot][]', $form_skp->bobot, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::text('data[target_tahunan][]', $form_skp->target_tahunan, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::text('data[trajectory][]', $form_skp->trajectory, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::select('data[satuan][]',[
                                                        ''=>'Pilih Satuan',
                                                        'Dokumen' => 'Dokumen',
                                                        'Persentase' => 'Persentase',
                                                        'Rupiah' => 'Rupiah',
                                                    ], $form_skp->satuan, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <th colspan="10">Indikator Tugas Dan Fungsi</th>
                                            </tr>
                                            @foreach($tugasjabatan as $item)
                                            <tr>
                                                <td>
                                                    {{ Form::hidden('data[jenis_indikator][]', 'TUSI') }}
                                                    {{ Form::hidden('data[id_indikator][]', $item->id) }}
                                                    {{ Form::textarea('data[indikator][]', $item->tugas, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::textarea('data[formula][]', $form_skp->formula, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::textarea('data[uraian][]', $form_skp->uraian, ['class'=>'form-control']) }}
                                                </td>
                                                <td>
                                                    {{ Form::textarea('data[sumber][]', $form_skp->sumber, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::select('data[periode][]',[
                                                        ''=>'Pilih Periode',
                                                        'Bulanan' => 'Bulanan',
                                                        'Triwulan' => 'Triwulan',
                                                        'Caturwulan' => 'Caturwulan',
                                                        'Tahunan' => 'Tahunan',
                                                        'Semester' => 'Semester',
                                                    ], $form_skp->periode, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::text('data[bobot][]', $form_skp->bobot, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::text('data[target_tahunan][]', $form_skp->target_tahunan, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::text('data[trajectory][]', $form_skp->trajectory, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::select('data[satuan][]',[
                                                        ''=>'Pilih Satuan',
                                                        'Dokumen' => 'Dokumen',
                                                        'Persentase' => 'Persentase',
                                                        'Rupiah' => 'Rupiah',
                                                    ], $form_skp->satuan, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <th colspan="10">Indikator Tugas Tambahan</th>
                                            </tr>
                                            @foreach($skp_tugas_tambahan as $item)
                                            <tr>
                                                <td>
                                                    {{ Form::hidden('data[jenis_indikator][]', 'TAMBAHAN') }}
                                                    {{ Form::hidden('data[id_indikator][]', $item->id) }}
                                                    {{ Form::textarea('data[indikator][]', $item->tugas_tambahan, ['class'=>'form-control','required'=>true]) }}
                                                </td>
                                                <td>
                                                    {{ Form::textarea('data[formula][]', $form_skp->formula, ['class'=>'form-control']) }}
                                                </td>
                                                <td>
                                                    {{ Form::textarea('data[uraian][]', $form_skp->uraian, ['class'=>'form-control']) }}
                                                </td>
                                                <td>
                                                    {{ Form::textarea('data[sumber][]', $form_skp->sumber, ['class'=>'form-control']) }}
                                                </td>
                                                <td>
                                                    {{ Form::select('data[periode][]',[
                                                        ''=>'Pilih Periode',
                                                        'Bulanan' => 'Bulanan',
                                                        'Triwulan' => 'Triwulan',
                                                        'Caturwulan' => 'Caturwulan',
                                                        'Tahunan' => 'Tahunan',
                                                        'Semester' => 'Semester',
                                                    ], $form_skp->periode, ['class'=>'form-control']) }}
                                                </td>
                                                <td>
                                                    {{ Form::text('data[bobot][]', $form_skp->bobot, ['class'=>'form-control']) }}
                                                </td>
                                                <td>
                                                    {{ Form::text('data[target_tahunan][]', $form_skp->target_tahunan, ['class'=>'form-control']) }}
                                                </td>
                                                <td>
                                                    {{ Form::text('data[trajectory][]', $form_skp->trajectory, ['class'=>'form-control']) }}
                                                </td>
                                                <td>
                                                    {{ Form::select('data[satuan][]',[
                                                        ''=>'Pilih Satuan',
                                                        'Dokumen' => 'Dokumen',
                                                        'Persentase' => 'Persentase',
                                                        'Rupiah' => 'Rupiah',
                                                    ], $form_skp->satuan, ['class'=>'form-control']) }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </form>
                                    </tbody>
                                </table>
                            </div>
                            <!-- <button type="submit" class="btn btn-primary">Simpan</button> -->
                            {{ Form::submit('Simpan', ['class'=>'btn btn-primary ripple m-1']) }}
                            <a href="{{ route('skp') }}" class="btn btn-warning ripple m-1" type="button">Kembali</a>
                        </div>
                        </form>
                    </div>
                </div>
                <!-- end of col -->
            </div>
            <!-- end of row -->

@endsection

@section('page-js')

@endsection