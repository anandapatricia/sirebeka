
    <thead>
        <tr>
            <th width="20%">Indikator Kinerja Ujama</th>
            <th width="18%">Formula</th>
            <th width>Uraian</th>
            <th>Sumber Data</th>
            <th>Periode Penilaian (Trajectory)</th>
            <th>Bobot</th>
            <th>Target Tahunan</th>
            <th>Target Tiap Trajectory</th>
            <th>Satuan Target IKU</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
    
    @foreach($skp as $skp)
        <tr>
            <td>{{ strtoupper($skp['indikator']) }}</td>
            <td>{{ strtoupper($skp['formula']) }}</td>
            <td>{{ strtoupper($skp['uraian']) }}</td>
            <td>{{ strtoupper($skp['sumber']) }}</td>
            <td>{{ strtoupper($skp['periode']) }}</td>
            <td>{{ $skp['bobot'] }}</td>
            <td>{{ $skp['target_tahunan'] }}</td>
            <td>{{ $skp['trajectory'] }}</td>
            <td>{{ $skp['satuan'] }}</td>
            <td>
                <?php 
                    $warna;
                    $tombol;

                    if($skp['status'] == 0){
                        $warna = 'success';
                        $tombol = 'Kirim';
                    } else if($skp['status'] == 1) {
                        $warna = 'info';
                        $tombol = 'Terkirim';
                    } else if($skp['status'] == 2) {
                        $warna = 'warning';
                        $tombol = 'Revisi';
                    } else if($skp['status'] == 3) {
                        $warna = 'danger';
                        $tombol = 'Ditolak';
                    }
                ?>
                <form method="POST" action="{{ route('kirimskp',$skp->id) }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$skp['id']}}">
                    <button class="btn btn-{{ $warna }} btn-sm m-1" type="submit">{{ $tombol }}</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>

