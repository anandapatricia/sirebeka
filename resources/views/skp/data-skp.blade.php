    @php $no = 1; @endphp
    @foreach($data as $key=>$skp)
        @php 
            if($key == 'IKU'){
                $indikator = 'INDIKATOR KINERJA UTAMA';
            }elseif($key == 'TUSI'){
                $indikator = 'INDIKATOR TUGAS DAN FUNGSI';
            }else{
                $indikator = 'INDIKATOR TUGAS TAMBAHAN';
            }
        @endphp
        <tr>
            <th colspan="10">{{ $indikator }}</th>
        </tr>
        @php $total = count($skp); @endphp
        @foreach($skp as $skp_item)
        <tr>
            <td>{{ strtoupper($skp_item['indikator']) }}</td>
            <td>{{ ucwords($skp_item['formula']) }}</td>
            <td>{{ ucwords($skp_item['uraian']) }}</td>
            <td>{{ ucwords($skp_item['sumber']) }}</td>
            <td>{{ ucwords($skp_item['periode']) }}</td>
            <td class="text-right">{{ number_format($skp_item['bobot']) }}</td>
            <td class="text-right">{{ number_format($skp_item['target_tahunan']) }}</td>
            <td class="text-right">{{ number_format($skp_item['trajectory']) }}</td>
            <td>{{ $skp_item['satuan'] }}</td>
            
            <td>
            @if($key == 'IKU' or $key == "TAMBAHAN" )
                @if($header->status == 0)
                    <!-- <a href="{{ route('editskp.detail',['id_header'=>$skp_item['id_skp_header'], 'id'=>$skp_item['id']]) }}" class="btn btn-warning btn-sm">Ubah</a> -->
                    <!-- @if($key == 'IKU' or $key == "TAMBAHAN" )
                        <a href="" class="btn btn-danger btn-sm" >Hapus</a>
                    @endif -->
                    <a href="" class="btn btn-danger btn-sm" >Hapus</a>
                @endif
            @endif
            </td>
                
        </tr>
        @endforeach
    @endforeach