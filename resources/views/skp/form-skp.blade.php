@extends('layouts.master')
@section('page-css')

@endsection

@section('main-content')
            <div class="breadcrumb">
                <h1>SKP</h1>
                <ul>
                    <li><a href="">Delail SKP</a></li>
                    <li>Detail</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <form method="POST" action="{{ route('saveskp', ['id_skp_header' => $header->id]) }}">
                            @csrf
                            <div class="card-body">
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <td width="150">Nama</td>
                                        <td>{{ auth()->user()->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td>Nip</td>
                                        <td>{{ auth()->user()->nip }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jabatan</td>
                                        <td>{{ auth()->user()->getJabatan->jabatan_baru }}</td>
                                    </tr>
                                    <tr>
                                        <td>Periode</td>
                                        <td>{{ $header->bulan.'/'.$header->tahun }}</td>
                                    </tr>
                                </table>

                                <br><br>

                                <div class="table-responsive">
                                    <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="20%">Indikator</th>
                                                <th width="18%">Formula</th>
                                                <th>Uraian</th>
                                                <th>Sumber Data</th>
                                                <th>Periode Penilaian (Trajectory)</th>
                                                <th>Bobot</th>
                                                <th>Target Tahunan</th>
                                                <th>Target Tiap Trajectory</th>
                                                <th width="8%">Satuan Target IKU</th>
                                                <th width="8%">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @include('skp.data-skp')
                                        </tbody>
                                    </table>
                                </div>
                                <!-- <button type="submit" class="btn btn-primary">Simpan</button> -->
                                <a href="{{ route('skp') }}" class="btn btn-warning ripple m-1" type="button">Kembali</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- end of col -->

            </div>
            <!-- end of row -->

@endsection

@section('page-js')

@endsection