@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
            <div class="breadcrumb">
                <h1>SKP</h1>
                <ul>
                    <li><a href="">Daftar SKP</a></li>
                    <li>SKP</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <a href="{{ route('buat.skp') }}" class="btn btn-warning ripple m-1" type="button">Buat SKP</a>
                            <br><br>
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tahun</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($skp as $item)
                                        <tr>
                                            <td>1</td>
                                            <td>{{ $item->tahun }}</td>
                                            <td>{{ ($item->status == 1) ? 'Terkirim':'Draft' }}</td>
                                            <td>
                                                <a href="{{ route('tambahskp.detail', ['id' => $item->id]) }}" class="btn btn-primary btn-sm">Detail</a>
                                                <a href="{{ route('skp.detail.realisasi', ['tahun' => $item->tahun]) }}" class="btn btn-secondary btn-sm">Detail Realisasi</a>
                                                @if($item->status == 0)
                                                    <a href="{{ route('ubah.skp', ['id' => $item->id]) }}" class="btn btn-success btn-sm">Edit</a>
                                                    <a href="{{ route('skp.kirim', ['id' => $item->id]) }}" class="btn btn-info btn-sm">Kirim</a>
                                                @elseif($item->status == 1)
                                                    <a href="{{ route('skp.buat.realisasi', ['id' => $item->id]) }}" class="btn btn-warning btn-sm" type="button">Buat Realisasi SKP</a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end of col -->

            </div>
            <!-- end of row -->

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
