@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
            <div class="breadcrumb">
                <h1>SKP</h1>
                <ul>
                    <li><a href="">Data Realisasi SKP</a></li>
                    <li>Realisasi</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <!-- <a href="{{ route('buat.skp') }}" class="btn btn-warning ripple m-1" type="button">Buat Realisasi SKP</a> -->
                            <!-- <br><br> -->
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th>Tahun</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $no = 1; @endphp
                                        @foreach($skp as $item)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $item->bulan }}</td>
                                            <td>{{ $item->tahun }}</td>
                                            <td>{{ ($item->status == 1) ? 'Terkirim':'Draft' }}</td>
                                            <td>
                                                <a href="{{ route('skp.detail.realisasi.data', ['id' => $item->id]) }}" class="btn btn-primary btn-sm">Detail</a>
                                                @if($item->status == 0)
                                                    <a href="{{ route('ubah.skp', ['id' => $item->id]) }}" class="btn btn-success btn-sm">Edit</a>
                                                    <a href="{{ route('kirim.realisasi', ['id' => $item->id]) }}" class="btn btn-info btn-sm">Kirim</a>
                                                @endif
                                            </td>
                                        </tr>
                                        @php $no++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end of col -->

            </div>
            <!-- end of row -->

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
