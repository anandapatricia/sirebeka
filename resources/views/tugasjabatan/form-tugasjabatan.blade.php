@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">


@endsection

@section('main-content')
   <div class="breadcrumb">
                <h1>Tambah Tugas Jabatan</h1>
                <form method="POST" action="{{ route('savetugasjabatan') }}">
                @csrf
                <ul>
                    <li><a href="">Master Data</a></li>
                    <li>Tugas Jabatan</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Form Input Tugas Jabatan</div>
                            <form>
                                <div class="row">
                                    <input type="hidden" class="form-control" name="id_jabatan_baru" id="id_jabatan_baru" value="{{$jabatan->id}}">

                                    <div class="col-md-6 form-group mb-3">
                                        <label>Jabatan Baru</label>
                                        <input type="text" class="form-control" name="jabatan_baru" value="{{ $jabatan->jabatan_baru }}" readonly>
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="tugas">Tugas Jabatan</label>
                                        <input type="text" class="form-control" name="tugas" id="tugas" placeholder="Masukan Tugas Jabatan">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="kuantitas">Kuantitas</label>
                                        <input type="text" class="form-control" name="kuantitas" id="kuantitas" placeholder="Masukan Banyak Kuantitas">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="keluaran">Keluaran</label>
                                        <select class="form-control" name="keluaran">
                                            <option value="DOKUMEN">DOKUMEN</option>
                                            <option value="KONSEP">KONSEP</option>
                                            <option value="LAPORAN">LAPORAN</option>
                                            <option value="KEGIATAN">KEGIATAN</option>
                                            <option value="DATA">DATA</option>
                                            <option value="BAHAN">BAHAN</option>
                                        </select>
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="kualitas">Kualitas/Mutu</label>
                                        <input type="text" class="form-control" name="kualitas" id="kualitas" placeholder="Masukan Nilai Persen 0 - 100">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="waktu">Waktu</label>
                                        <input class="form-control" name="waktu" id="waktu" placeholder="Masukan waktu dalam bentuk jam">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="biaya">Biaya</label>
                                        <input class="form-control" name="biaya" id="biaya" placeholder="Masukan Biaya dalam rupiah">
                                    </div>

                                    <div class="col-md-12">
                                         <button type="submit" class="btn btn-primary">Simpan</button>
                                         <a href="{{ route('mjabatan') }}" class="btn btn-warning">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>



@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>


@endsection
