@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
            <div class="breadcrumb">
                <h1>Data Tugas Jabatan</h1>
                <ul>
                    <li><a href="">Master Data</a></li>
                    <li>Tugas Jabatan</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            {{-- <a href="{{ route('formjabatan') }}" class="btn btn-primary ripple m-1" type="button">Tambah Jabatan</a> --}}
                            <br><br>
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    @include('tugasjabatan.data-tugasjabatan')
                                </table>
                            </div>

                            <br>

                            <div>
                                <a href="{{ route('mjabatan') }}" class="btn btn-warning">Kembali</a>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end of col -->

            </div>
            <!-- end of row -->

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
