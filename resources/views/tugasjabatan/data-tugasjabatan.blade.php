
    <thead>
        <tr>
            <th>KEGIATAN TUGAS JABATAN</th>
            <th>KUANTITAS</th>
            <th>SATUAN HASIL KERJA</th>
            <th>KUALITAS/MUTU (%)</th>
            <th>WAKTU (Jam)</th>
            <!-- <th>BIAYA (Rp.)</th> -->
            <th>AKSI</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $tugasjabatan as $tugas )
        <tr>
            <td>{{ strtoupper($tugas['tugas']) }}</td>
            <td>{{ $tugas['kuantitas'] }}</td>
            <td>{{ $tugas['keluaran'] }}</td>
            <td>{{ $tugas['kualitas'] }}</td>
            <td>{{ $tugas['waktu'] }}</td>
            <!-- <td>{{ $tugas['biaya'] }}</td> -->
            <td>
                <a href="{{ route('edittugasjabatan', $tugas['id']) }}" class="btn btn-success btn-sm m-1" type="button">Edit</a>
            </td>
        </tr>
        @endforeach
    </tbody>
