@extends('layouts.master')
@section('main-content')
           <div class="breadcrumb">
                <h1>Dashboard </h1>
                <ul>
                    <li><a href="">SiReBeKa</a></li>
                    <li>Dashboard</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <!-- ICON BG -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Business-ManWoman"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">PNS</p>
                            <p class="text-primary text-24 line-height-1 mb-2">{{ $jumlah_pns }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Administrator"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">BLU</p>
                                <p class="text-primary text-24 line-height-1 mb-2">{{ $jumlah_blu }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Add-User"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Honorer</p>
                                <p class="text-primary text-24 line-height-1 mb-2">{{ $jumlah_honor }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Business-Mens"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Total</p>
                                <p class="text-primary text-24 line-height-1 mb-2">{{ ($jumlah_pns + $jumlah_blu + $jumlah_honor) }}</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Money-Bag"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Total Pendapatan</p>
                            <p class="text-primary text-24 line-height-1 mb-2">{{ number_format($total_pendapatan) }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                        <div class="card-body text-center">
                            <i class="i-Money-Bag"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Total Remunerasi</p>
                            <p class="text-primary text-24 line-height-1 mb-2">{{ number_format($total_remun) }}</p>
                            </div>
                        </div>
                    </div>
                </div>
               @if($persentase >= '40')
               <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary bg-danger o-hidden mb-4 text-white">
                        <div class="card-body">
                            <i class="i-Line-Chart" style="color: white !important "></i>
                            <div class="content">
                                <p class="mt-2 mb-0">Rasio remunerasi terhadap pendapatan</p>
                            <p class="text-24 line-height-1 mb-2">{{ $persentase }} %</p>
                            </div>
                        </div>
                    </div>
                </div>

                @elseif($persentase >= 30)
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary bg-warning o-hidden mb-4 text-white">
                        <div class="card-body">
                            <i class="i-Line-Chart" style="color: white !important "></i>
                            <div class="content">
                                <p class="mt-2 mb-0">Rasio remunerasi terhadap pendapatan</p>
                            <p class="text-24 line-height-1 mb-2">{{ $persentase }} %</p>
                            </div>
                        </div>
                    </div>
                </div>


                @else
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4 text-white">
                        <div class="card-body">
                            <i class="i-Line-Chart"></i>
                            <div class="content">
                                <p class="text-muted mt-2 mb-0">Rasio remunerasi terhadap pendapatan</p>
                            <p class="text-primary text-24 line-height-1 mb-2">{{ $persentase }} %</p>
                            </div>
                        </div>
                    </div>
                </div>


               @endif
            </div>

            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title">Total Remunerasi</div>
                            <div id="pendapatanChart" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title">Total Pegawai PNS dan BLU</div>
                            <div id="totalBLUPNS" style="height: 300px;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="card o-hidden mb-4">
                        <div class="card-header">
                            <h3 class="w-50 float-left card-title m-0">Pengajuan Cuti</h3>
                            {{-- <div class="dropdown dropleft text-right w-50 float-right">
                                           <button class="btn bg-gray-100" type="button" id="dropdownMenuButton_table1"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="nav-icon i-Gear-2"></i>
                                        </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton_table1">
                                    <a class="dropdown-item" href="#">Add new user</a>
                                    <a class="dropdown-item" href="#">View All users</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div> --}}

                        </div>
                        <div class="card-body">
                            @if(Session::get('setuju'))
                                <div class="alert alert-primary" role="alert"><strong class="text-capitalize">Success!</strong> {{ Session::get('setuju') }}.
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                            @elseif(Session::get('tolak'))
                                <div class="alert alert-danger" role="alert"><strong class="text-capitalize">Rejected!</strong> {{ Session::get('tolak') }}.
                                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                            @endif
                            <div class="table-responsive">
                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <th width="150">Nama</th>
                                            <!-- <th>NIP</th> -->
                                            <th>Jabatan</th>
                                            <th>Jenis Cuti</th>
                                            <th>Tanggal Mulai</th>
                                            <th>Tanggal Selesai</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($list_cuti as $item)
                                        <tr>
                                            <td>{{ $item->nama}} </td>
                                            <!-- <td>{{ $item->nip}} </td> -->
                                            <td>{{ $item->jabatan}} </td>
                                            <td>{{ $item->jeniscuti}} </td>
                                            <td>{{ $item->tanggalcuti}} </td>
                                            <td>{{ $item->tanggal}} </td>
                                            <td>
                                                @if($item['status'] == 1 or 3)
                                                    <a href="{{ route('cuti_setuju',$item->id) }}" class="btn btn-primary btn-sm"><i class="i-Yes"></i></a>
                                                    <a href="{{ route('cuti_tolak',$item->id) }}" class="btn btn-danger btn-sm"><i class="i-Remove"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of col-->
                <div class="col-md-6">
                    <div class="card o-hidden mb-4">
                        <div class="card-header">
                            <h3 class="w-50 float-left card-title m-0">Report SPT Terakhir</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">

                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <th scope="col">Nomor</th>
                                            <th scope="col">Tangal</th>
                                            <th scope="col">Mulai</th>
                                            <th scope="col">Selesai</th>
                                            <th scope="col">Lokasi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($list_spt as $spt)
                                        <tr>
                                            <td>{{ $spt->nomor_surat}}</td>
                                            <td>{{ $spt->tanggal_surat}}</td>
                                            <td>{{ $spt->tanggal_mulai}}</td>
                                            <td>{{ $spt->tanggal_selesai}}</td>
                                            <td>{{ $spt->lokasi_spt}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card o-hidden mb-4">
                        <div class="card-header">
                            <h3 class="w-50 float-left card-title m-0">Pengajuan SKP</h3>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">

                                <table class="table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Tahun</th>
                                            <th scope="col">NIP</th>
                                            <th scope="col">Indikator</th>
                                            <th scope="col">Formula</th>
                                            <th scope="col">Sumber</th>
                                            <th scope="col">Periode</th>
                                            <th scope="col">Satuan</th>
                                            <th scope="col">Jenis Indikator</th>
                                            <th scope="col">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <a href="#" class="text-success mr-2">
                                                    <i class="nav-icon i-Pen-2 font-weight-bold"></i>
                                                </a>
                                                <a href="#" class="text-danger mr-2">
                                                    <i class="nav-icon i-Close-Window font-weight-bold"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of col-->
            </div>


@endsection

@section('page-js')
     <script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/dashboard.v1.script.js')}}"></script>
<script>
$(document).ready(function () {
var echartElemBar = document.getElementById('pendapatanChart');
    if (echartElemBar) {
        var echartBar = echarts.init(echartElemBar);
        echartBar.setOption({
            legend: {
                borderRadius: 0,
                orient: 'horizontal',
                x: 'right',
                data: ['Remunerasi']
            },
            grid: {
                left: '8px',
                right: '8px',
                bottom: '0',
                containLabel: true
            },
            tooltip: {
                show: true,
                backgroundColor: 'rgba(0, 0, 0, .8)'
            },
            xAxis: [{
                type: 'category',
                data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
                axisTick: {
                    alignWithLabel: true
                },
                splitLine: {
                    show: false
                },
                axisLine: {
                    show: true
                }
            }],
            yAxis: [{
                type: 'value',
                axisLabel: {
                    formatter: 'Rp. {value}'
                },
                min: 0,
                max: 3196320400,
                interval: 250000000,
                axisLine: {
                    show: false
                },
                splitLine: {
                    show: true,
                    interval: 'auto'
                }
            }],

            series: [{
                name: 'Remunerasi',
                data: [{{$total_remun_jan}},{{$total_remun_feb}},{{$total_remun_mar}},{{$total_remun_apr}},{{$total_remun_mei}},{{$total_remun_jun}},{{$total_remun_jul}},{{$total_remun_ags}},{{$total_remun_sep}},{{$total_remun_okt}},{{$total_remun_nov}},{{$total_remun_des}}],
                label: { show: false, color: '#0168c1' },
                type: 'bar',
                barGap: 0,
                color: '#ffc107',
                smooth: true,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowOffsetY: -2,
                        shadowColor: 'rgba(0, 0, 0, 0.3)'
                    }
                }
            }]
        });
        $(window).on('resize', function () {
            setTimeout(function () {
                echartBar.resize();
            }, 500);
        });
    }

    var echartElemPie = document.getElementById('totalBLUPNS');
    if (echartElemPie) {
        var echartPie = echarts.init(echartElemPie);
        echartPie.setOption({
            color: ['#002898', '#083c05'],
            tooltip: {
                show: true,
                backgroundColor: 'rgba(0, 0, 0, .8)'
            },

            series: [{
                name: 'Pegawai STIP',
                type: 'pie',
                radius: '60%',
                center: ['50%', '50%'],
                data: [{ value: {{$jumlah_blu}}, name: 'BLU' }, { value: {{$jumlah_pns}}, name: 'PNS' }],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }]
        });
        $(window).on('resize', function () {
            setTimeout(function () {
                echartPie.resize();
            }, 500);
        });
    }
});
</script>
@endsection
