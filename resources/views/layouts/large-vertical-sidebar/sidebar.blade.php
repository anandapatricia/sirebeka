<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{ request()->is('dashboard/*') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ route('dashboard') }}">
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('sessions/*') ? 'active' : '' }}" data-item="sessions">
                <a class="nav-item-hold" href="#">
                    <i class="nav-icon i-Administrator"></i>
                    <span class="nav-text">Master Data</span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('cuti') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ route('table_pengajuan') }}"> 
                    <i class="nav-icon i-Computer-Secure"></i>
                    <span class="nav-text">Pengajuan Cuti</span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('remunerasi') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{route('remunerasi')}}">
                    <i class="nav-icon i-Financial"></i>
                    <span class="nav-text">Remunerasi</span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('skp') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ route('skp') }}"> 
                    <i class="nav-icon i-File-Clipboard-File--Text"></i>
                    <span class="nav-text">SKP</span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('spt') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ route('table_spt') }}"> 
                    <i class="nav-icon i-Computer-Secure"></i>
                    <span class="nav-text">SPT</span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('importabsensi') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ route('importabsensi') }}"> 
                    <i class="nav-icon i-Address-Book"></i>
                    <span class="nav-text">Absensi</span>
                </a>
            </li>
        </ul>
    </div>

    <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <!-- Submenu Dashboards -->

        <!-- user management -->
        <ul class="childNav" data-parent="sessions">
            <li class="nav-item">
                <a href="{{ route('data.index') }}">
                    <i class="nav-icon i-Add-User"></i>
                    <span class="item-name">Master Pegawai</span>
                </a>
            </li>
        </ul>

        <ul class="childNav" data-parent="sessions">
            <li class="nav-item">
                <a href="{{ route('mjabatan') }}">
                    <i class="nav-icon i-File-Horizontal-Text"></i>
                    <span class="item-name">Master Jabatan</span>
                </a>
            </li>
        </ul>

        <ul class="childNav" data-parent="sessions">
            <li class="nav-item">
                <a href="{{ route('mgrade') }}">
                    <i class="nav-icon i-File-Horizontal-Text"></i>
                    <span class="item-name">Master Grade</span>
                </a>
            </li>
        </ul>

        <ul class="childNav" data-parent="sessions">
            <li class="nav-item">
                <a href="{{ route('mgolongan') }}">
                    <i class="nav-icon i-File-Horizontal-Text"></i>
                    <span class="item-name">Master Golongan</span>
                </a>
            </li>
        </ul>

        <ul class="childNav" data-parent="sessions">
            <li class="nav-item">
                <a href="{{ route('mmasakerja') }}">
                    <i class="nav-icon i-File-Horizontal-Text"></i>
                    <span class="item-name">Master Masa Kerja</span>
                </a>
            </li>
        </ul>

        <ul class="childNav" data-parent="sessions">
            <li class="nav-item">
                <a href="{{ route('ikutable') }}">
                    <i class="nav-icon i-File-Horizontal-Text"></i>
                    <span class="item-name">Master Indikator Kinerja Utama</span>
                </a>
            </li>
        </ul>

    <ul class="childNav" data-parent="sessions">

            <li class="nav-item dropdown-sidemenu">
                <a href="">
                    <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                        <span class="item-name">Faktor Evaluasi</span>
                    <i class="dd-arrow i-Arrow-Down"></i>
                </a>

                <ul class="submenu">
                    <li>
                        <a href="{{ route('f1_kompetensi_teknis_table') }}">F1. Kompetensi Teknis</a>
                    </li>
                    <li>
                        <a href="{{ route('f2table') }}">F2. Manajerial</a>
                    </li>
                    <li>
                        <a href="{{ route('f3table') }}">F3. Komunikasi</a>
                    </li>
                    <li>
                        <a href="{{ route('f4table') }}">F4. Analisis Lingkungan Pekerjaan</a>
                    </li>
                    <li>
                        <a href="{{ route('f5table') }}">F5. Pedoman Keputusan</a>
                    </li>
                    <li>
                        <a href="{{ route('f6table') }}">F6. Kondisi Kerja</a>
                    </li>
                    <li>
                        <a href="{{ route('f78table') }}">F78. Wewenang Dan Nilai Kelola Harta</a>
                    </li>
                    <li>
                        <a href="{{ route('fi9table') }}">FI9. Peran Jabatan - Indeterminate</a>
                    </li>
                    <li>
                        <a href="{{ route('fd9table') }}">FD9. Peran Jabatan - Determinate</a>
                    </li>
                    <li>
                        <a href="{{ route('f10table') }}">F10. Probabilitas Resiko</a>
                    </li>
                </ul>
            </li>
        </ul> 

        <ul class="childNav" data-parent="sessions">
            <li class="nav-item">
                <a href="{{ route('pir') }}">
                    <i class="nav-icon i-File-Horizontal-Text"></i>
                    <span class="item-name">Master PIR</span>
                </a>
            </li>
        </ul>

    </div>
    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->
