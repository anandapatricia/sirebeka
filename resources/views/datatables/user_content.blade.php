<thead>
    <tr>
        <th>Nama</th>
        <th>Username</th>
        <th>Email</th>
        <th>Aksi</th>
    </tr>
</thead>
<tbody>
    <tr>
        <td>SUSILO,SE,MT</td>
        <td>susilo</td>
        <td>susilo@gmail.com</td>
        <td>
            <button class="btn btn-success btn-sm m-1" type="button">Edit</button>
            <button class="btn btn-danger btn-sm m-1" type="button">Delete</button>
        </td>
    </tr>
    <tr>
        <td>Ir. THEO JOHANNES FRANS KALANGI,MSTr</td>
        <td>johannes</td>
        <td>tjohannesf@gmail.com</td>
        <td>
            <button class="btn btn-success btn-sm m-1" type="button">Edit</button>
            <button class="btn btn-danger btn-sm m-1" type="button">Delete</button>
        </td>
    </tr>
        <td>Drs. WAHYU WIDAYAT, MM</td>
        <td>wahyu_widayat</td>
        <td>wahyuwidayat@gmail.com</td>
        <td>
            <button class="btn btn-success btn-sm m-1" type="button">Edit</button>
            <button class="btn btn-danger btn-sm m-1" type="button">Delete</button>
        </td>
    </tr>
        <td>ZULNASRI,SH</td>
        <td>zulnasri10</td>
        <td>zul@gmail.com</td>
        <td>
            <button class="btn btn-success btn-sm m-1" type="button">Edit</button>
            <button class="btn btn-danger btn-sm m-1" type="button">Delete</button>
        </td>
    </tr>
        <td>Drs. TIGOR SIAGIAAN,MM</td>
        <td>tigor77</td>
        <td>tigor@gmail.com</td>
        <td>
            <button class="btn btn-success btn-sm m-1" type="button">Edit</button>
            <button class="btn btn-danger btn-sm m-1" type="button">Delete</button>
        </td>
    </tr>
</tbody>
<tfoot>
    <tr>
        <th>Nama</th>
        <th>Username</th>
        <th>Email</th>
        <th>Aksi</th>
    </tr>
</tfoot>
