@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
            <div class="breadcrumb">
                <h1>Data Pegawai</h1>
                <ul>
                    <li><a href="">Master Data</a></li>
                    <li>Pegawai</li>
                </ul>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <!-- <div class="row mb-4">
                <div class="col-md-12">
                    <h4>Datatables</h4>
                    <p>DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, build upon the foundations of progressive enhancement, that adds all of these advanced features to any HTML table.</p>
                </div>
            </div> -->
            <!-- end of row -->

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <a href="{{ route('forms-basic') }}" class="btn btn-primary ripple m-1" type="button">Tambah Pegawai</a>
                            <br><br>
                            <div class="table-responsive">
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                 @include('datatables.table_content')
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end of col -->


                <!-- <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <h4 class="card-title mb-3">Alternative pagination</h4>

                                <p>The default page control presented by DataTables (forward and backward buttons with up to 7 page numbers in-between) is fine for most situations, but there are cases where you may wish to customise the options presented to
                                the end user. This is done through DataTables' extensible pagination mechanism, the <a href="//datatables.net/reference/option/pagingType"><code class="option"
                                title="DataTables initialisation option">pagingType</code></a> option.</p>

                            <div class="table-responsive">
                                <table id="alternative_pagination_table" class="display table table-striped table-bordered" style="width:100%">
                                 @include('datatables.table_content')
                                </table>
                            </div>

                        </div>
                    </div>
                </div> -->
                <!-- end of col -->

                <!-- <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <h4 class="card-title mb-3">Scroll - vertical</h4>

                            <p>This example shows the DataTables table body scrolling in the vertical direction. This can generally be seen as an alternative method to pagination for displaying a large table in a fairly small vertical area, and as such pagination
                                has been disabled here (note that this is not mandatory, it will work just fine with pagination enabled as well!).</p>

                            <div class="table-responsive">
                                <table id="scroll_vertical_table" class="display table table-striped table-bordered" style="width:100%">
                                @include('datatables.table_content')
                                </table>
                            </div>

                        </div>
                    </div>
                </div> -->
                <!-- end of col -->

                <!-- <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <h4 class="card-title mb-3">Scroll - vertical, dynamic height</h4>

                            <p>This example shows a vertically scrolling DataTable that makes use of the CSS3 <code>vh</code> unit in order to dynamically resize the viewport based on the browser window height. The <a href="https://developer.mozilla.org/en/docs/Web/CSS/length#Viewport-percentage_lengths"><code>vh</code>
                            unit</a> is effectively a percentage of the browser window height. So the <code>50vh</code> used in this example is 50% of the window height. The viewport size will update dynamically as the window is resized.</p>

                            <div class="table-responsive">
                                <table id="scroll_vertical_dynamic_height_table" class="display table table-striped table-bordered" style="width:100%">
                                  @include('datatables.table_content')
                                </table>
                            </div>

                        </div>
                    </div>
                </div> -->
                <!-- end of col -->


                <!-- <div class="col-md-12 mb-4">
                    <div class="card  text-left">
                        <div class="card-body">
                            <h4 class="card-title mb-3">Scroll - horizontal</h4>


                            <p>DataTables has the ability to show tables with horizontal scrolling, which is very useful for when you have a wide table, but want to constrain it to a limited horizontal display area. To enable x-scrolling simply set the
                                <a href="//datatables.net/reference/option/scrollX"><code class="option" title="DataTables initialisation option">scrollX</code></a> parameter to be whatever you want the container wrapper's width to be (this should be
                                100% in almost all cases with the width being constrained by the container element).</p>
                        </div>
                        <div class="card-body w-80 ml-auto mr-auto">


                            <div class="table-responsive">
                                <table id="scroll_horizontal_table" class="display nowrap table table-striped table-bordered" style="width:100%">
                                @include('datatables.table_content')
                                </table>
                            </div>

                        </div>
                    </div>
                </div> -->
                <!-- end of col -->

                <!-- <div class="col-md-12 mb-4">
                    <div class="card text-left">

                        <div class="card-body">
                            <h4 class="card-title mb-3">Language - Comma decimal place</h4>

                            <p>A dot (<code>.</code>) is used to mark the decimal place in Javascript, however, <a href="http://en.wikipedia.org/wiki/Decimal_mark">many
                            parts of the world
                            use a comma</a> (<code>,</code>) and other characters such as the Unicode decimal separator (<code>⎖</code>) or a dash (<code>-</code>) are often used to show the decimal place in a displayed number.</p>

                            <div class="table-responsive">
                                <table id="comma_decimal_table" class="display table table-striped table-bordered" style="width:100%">
                                 @include('datatables.table_content')
                                </table>
                            </div>

                        </div>
                    </div>
                </div> -->
                <!-- end of col -->

            </div>
            <!-- end of row -->

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
