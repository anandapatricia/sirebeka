@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
td{
    text-align : center;
}

th{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Master Data</h1>
            <ul>
                <li>Faktor Evaluasi</li>
                <li>Manajerial</li>
            </ul>
    </div>
        <div class="separator-breadcrumb border-top"></div>

            <body>
                <div class="row mb-4">
                    <div class="col-md-12 mb-4">
                        <div class="card text-left">
                            <div class="card-body">
                                <h2>MANAJERIAL</h2><br/>
                                <form method="post" action="{{ route('f2update', $f2['id'])}}">
                                @csrf
                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="jenis">Jenis:</label>
                                            <input id="jenis" type="jenis" value="{{$f2->jenis}}" class="form-control" name="jenis" >
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="kelas">Kelas:</label>
                                            <input id="kelas" value="{{$f2->kelas}}" type="kelas" class="form-control" name="kelas" >
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="tipe">Tipe:</label>
                                            <input id="tipe" value="{{$f2->tipe}}" class="form-control" name="tipe">
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="lingkup">Lingkup:</label>
                                            <input id="lingkup" type="lingkup" value="{{$f2->lingkup}}" class="form-control" name="lingkup" >
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="nilai">Nilai:</label>
                                            <input id="nilai" value="{{$f2->nilai}}" class="form-control" name="nilai">
                                        </div>
                                    </div> 

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4" style="margin-top:10px">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="{{ route('f2table') }}" class="btn btn-warning">Kembali</a>
                                        </div>
                                    </div>
                                </form>      
                            </div>
                        </div>
                    </div>
                </div>
            </body>


@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection