@extends('layouts.master')
@section('page-css')


<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')

	<div class="breadcrumb">
		<h1>Master Data Pegawai</h1>
	</div>
	<div class="separator-breadcrumb border-top"></div>
	<div class="row mb-4">
		<div class="col-md-12 mb-4">
			<div class="card text-left">
				<div class="card-body">
					<div class="table-responsive">
						<div class="row mb-4">
							<div class="col-md-2">
								<a href="{{ route('data') }}" class="btn btn-primary">Tambah Pegawai</a>
								<!-- <a href="{{ route('getSIK') }}" class="btn btn-primary">Get SIK</a> -->
								<br>
									<br>
									</div>
									<!-- <div class="col-md-6">
										<form action="{{ route('syncPegawai') }}" method="post">
                                            @csrf

											<input type="hidden" name="kodekantor" value="008017000000000">
												<button type="submit" class="btn btn-success" > Sync Data Pegawai</button>
											</form>
										</div> -->
									</div>
									<table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
										<thead>
											<tr>
												<th>Nama</th>
												<th>NIP</th>
												<th>Sub Kantor</th>
												<th>Jabatan</th>
												<th>Jabatan Link</th>
												<th>Golongan </th>
												<th width="120">Aksi</th>
											</tr>
										</thead>
										<tbody>
                                            @foreach($list as $row)

											<tr>
												<td>{{$row->nama}}</td>
												<td>{{$row->nip}}</td>
												<td>{{$row->subkantor}}</td>
												<td>
                                                    {{ !empty($row->jabatan_struktural) ? $row->jabatan_struktural : $row->jabatan_fungsional }}
												</td>
												<td>{{ !empty($row->id_jabatan) ? $row->getJabatan->jabatan_baru : ""}}</td>
												<td>{{$row->golongan_global}}</td>
												<td>
                                                    <a href="{{ route('detailPegawai',['id_users'=>$row->id_users])}}" class="btn btn-success btn-sm m-1" style="text-decoration:none;">EDIT</a>
                                                    <a href="{{ route('absensi',['nip'=>$row->nip])}}" class="btn btn-danger btn-sm m-1">Absensi</a>
												</td>
											</tr>
                                             @endforeach

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- end of col -->
				</div>
				<!-- end of row -->
@endsection
@section('page-js')
				<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
				<script src="{{asset('assets/js/datatables.script.js')}}"></script>
@endsection
