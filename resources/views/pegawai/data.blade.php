@extends('layouts.master')
@section('page-css')
<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection
@section('main-content')
<style>
   .form-group {
   height:100px;
   width:100%;
   }
   .kolom1 {
   height:100px;
   width:48%;
   float:left;
   }
   .kolom2 {
   height:100px;
   width:48%;
   float:right;
   }
</style>
<div class="breadcrumb">
   <h1>Input Data Pegawai</h1>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="col-md-12 mb-4">
<div class="card text-left">
   @if(Session::has('flash_message'))
   <div class="alert alert-success">
      {{ Session::get('flash_message') }}
   </div>
   @endif
   <div class="card-body">
      <div class="card-title mb-3">
         <form action="{{ route('pegawai.save') }}" method="post"  enctype="multipart/form-data">
            @csrf
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <label>NAMA</label>
                     <input type="text" class="form-control" id="nama"  name="nama" required>
                  </div>
                  <div class="form-group">
                     <label>JENIS KELAMIN</label>
                     <select class="form-control form-control-rounded" name="jenis_kelamin">
                        <option value="" disabled selected>-- PILIH JENIS KELAMIN ---</option>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>
                     </select>
                  </div>
                  <div class="form-group">
                     <label>TANGGAL LAHIR</label>
                     <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" required>
                  </div>
                  <div class="form-group">
                     <label>TEMPAT LAHIR</label>
                     <input type="text" class="form-control" id="tempat_lahir"   name="tempat_lahir" required>
                  </div>
                  <div class="form-group">
                     <label>PHONE</label>
                     <input type="text" class="form-control" id="telepon"  name="telepon" required>
                  </div>
                  <div class="form-group">
                     <label>EMAIL</label>
                     <input type="text" class="form-control" id="email"   name="email" required>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <div class="kolom1">
                        <label>KODE KANTOR LAMA</label>
                        <input type="text" class="form-control" id="kode_kantor_lama"   name="kode_kantor_lama" >
                     </div>
                     <div class="kolom2">
                        <label>SUBKANTOR</label>
                        <input type="text" class="form-control" id="subkantor"   name="subkantor" required>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="kolom1">
                        <label>KODE SUBKANTOR</label>
                        <input type="text" class="form-control" id="kode_subkantor"   name="kode_subkantor" >
                     </div>
                     <div class="kolom2">
                        <label>TMT GOLONGAN</label>
                        <input type="text" class="form-control" id="tmt_golongan"   name="tmt_golongan" >
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="kolom1">
                        <label>GOLONGAN</label>
                        <input type="text" class="form-control" id="golongan"   name="golongan" required>
                     </div>
                     <div class="kolom2">
                        <label>PANGKAT</label>
                        <input type="text" class="form-control" id="pangkat"   name="pangkat" required>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="kolom1">
                        <label>KODE ESELON</label>
                        <input type="text" class="form-control" id="kode_eselon"   name="kode_eselon" >
                     </div>
                     <div class="kolom2">
                        <label>ESELON</label>
                        <input type="text" class="form-control" id="eselon"  name="eselon" >
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="kolom1">
                        <label>JABATAN</label>
                        <select class="form-control form-control-rounded" name="id_jabatan">
                           <option value="" disabled selected>-- PILIH JABATAN ---</option>
                           @foreach($jabatan as $row)
                           <option value="{{$row->id}}">{{$row->jabatan_baru}}</option>
                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="form-group">
                     <label>EMAIL</label>
                     <input type="text" class="form-control" id="email"   name="email" required>
                  </div>
                  <div class="form-group">
                     <label>UNIT</label>
                     <input type="text" class="form-control" id="unit"   name="unit" required>
                  </div>
                  <div class="form-group">
                     <label>KODE UNIT</label>
                     <input type="text" class="form-control" id="kode_unit"   name="kode_unit" >
                  </div>
                  <div class="form-group">
                     <label>KODE UNIT LAMA</label>
                     <input type="text" class="form-control" id="kode_unit_lama"   name="kode_unit_lama" >
                  </div>
                  <div class="form-group">
                     <label>KANTOR</label>
                     <input type="text" class="form-control" id="kantor"   name="kantor" required>
                  </div>
                  <div class="form-group">
                     <div class="kolom1">
                        <label>TMT PNS</label>
                        <input type="text" class="form-control" id="tmt_pns"   name="tmt_pns" >
                     </div>
                     <div class="kolom2">
                        <label>PENDIDIKAN TERAKHIR</label>
                        <input type="text" class="form-control" id="pendidikan_terakhir"   name="pendidikan_terakhir" >
                     </div>
                  </div>
                  <div class="form-group">
                     <label>GOLONGAN</label>
                     <select class="form-control form-control-rounded" name="golongan_global">
                        <option value="" disabled selected>-- PILIH GOLONGAN ---</option>
                        @foreach($golongan as $row)
                        <option value="{{$row->id}}">{{$row->golongan}}</option>
                        @endforeach
                     </select>
                  </div>
                  <div class="form-group">
                     <label>PANGKAT</label>
                     <input type="text" class="form-control" id="pangkat"   name="pangkat" required>
                  </div>
                  <div class="form-group">
                     <label>KODE ESELON</label>
                     <input type="text" class="form-control" id="kode_eselon"   name="kode_eselon" >
                  </div>
                  <div class="form-group">
                     <label>ESELON</label>
                     <input type="text" class="form-control" id="eselon"  name="eselon" >
                  </div>
                  <div class="form-group">
                     <label>JABATAN</label>
                     <select class="form-control form-control-rounded" name="id_jabatan">
                        <option value="" disabled selected>-- PILIH JABATAN ---</option>
                        @foreach($jabatan as $row)
                        <option value="{{$row->id}}">{{$row->jabatan_baru}}</option>
                        @endforeach
                     </select>
                  </div>
                  <div class="form-group">
                     <label>TMT CPNS</label>
                     <input type="date" class="form-control" id="tmt_cpns"  name="tmt_cpns" >
                  </div>
                  <div class="form-group">
                     <label>TMT PNS</label>
                     <input type="date" class="form-control" id="tmt_pns"   name="tmt_pns" >
                  </div>
                  <div class="form-group">
                     <label>PENDIDIKAN TERAKHIR</label>
                     <input type="text" class="form-control" id="pendidikan_terakhir"   name="pendidikan_terakhir" >
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label>KODE KANTOR</label>
                     <input type="text" class="form-control" id="kode_kantor"  name="kode_kantor" >
                  </div>
                  <div class="form-group">
                     <label>KODE KANTOR LAMA</label>
                     <input type="text" class="form-control" id="kode_kantor_lama"   name="kode_kantor_lama" >
                  </div>
                  <div class="form-group">
                     <label>SUBKANTOR</label>
                     <input type="text" class="form-control" id="subkantor"   name="subkantor" required>
                  </div>
                  <div class="form-group">
                     <label>KODE SUBKANTOR</label>
                     <input type="text" class="form-control" id="kode_subkantor"   name="kode_subkantor" >
                  </div>
                  <div class="form-group">
                     <label>TMT GOLONGAN</label>
                     <input type="date" class="form-control" id="tmt_golongan"   name="tmt_golongan" >
                  </div>
                  <div class="form-group">
                     <label>STATUS</label>
                     <select class="form-control form-control-rounded" name="status">
                        <option value="" disabled selected>-- PILIH STATUS ---</option>
                        <option value="PNS">PNS</option>
                        <option value="BLU">BLU</option>
                        <option value="HONOR">Honorer</option>
                     </select>
                  </div>
                  <button type="submit" name="selesai" class="btn btn-primary">Simpan</button>
         </form>
         </div>
         <div id="showDetail">
         </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('page-js')
@endsection