@extends('layouts.master')
@section('page-css')


<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')

	<div class="breadcrumb">
		<h1>Data Absensi</h1>
	</div>
	<div class="separator-breadcrumb border-top"></div>
	<div class="row mb-4">
		<div class="col-md-12 mb-4">
			<div class="card text-left">
				<div class="card-body">
                    <div class="card-title">Data Absensi Pegawai</div>
					<div class="table-responsive">
                        <table id="" class="display table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Tanggal</th>
                                    <th>Jam Duty</th>
                                    <th>Jam Off Duty</th>
                                    <th>Jam Masuk</th>
                                    <th>Jam Pulang</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($list))
                                    @foreach($list as $row)
                                    <tr>
                                        <td>{{$row->nip}}</td>
                                        <td>{{$row->nama}}</td>
                                        <td>{{$row->tanggal}}</td>
                                        <td>{{$row->jam_masuk}}</td>
                                        <td>{{$row->jam_pulang}}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
					</div>
				</div>
            </div>
        </div>
    </div>
@endsection
@section('page-js')
    <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
@endsection
