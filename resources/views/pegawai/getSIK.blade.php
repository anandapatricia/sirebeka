
@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')


            <div class="breadcrumb">
                <h1>Data Pegawai</h1>
            </div>

            <div class="separator-breadcrumb border-top"></div>

                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="card-title mb-3">
                                <form action="{{ route('datadetail') }}" method="post">
                                @csrf
                                    <table class="table table-striped table-bordered">
                                        <tr>
                                            <td width="150">NIP</td>
                                            <td width="10">:</td>
                                            <td>
                                                <div class="input-group input-group-sm">
                                                    <input class="form-control" placeholder="Masukan NIP / NIK" id="nip" name="nosik" type="text" required>
                                                    <span class="input-group-append">
                                                        <button type="submit" onclick="show()" class="btn btn-primary">Cek!</button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
     
        
                                 </form>
                                 <div id="showDetail">
                                    {!! $html !!}
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>
    <script>
            $(function show(){
                var finish = function(data){
                    $("#showDetail").html(data);
                };
                $("#search").submit(function(){
                    $.get("{{ route('datadetail') }}" + '/' +
                            $("#nip").val(), finish);
                    return false;
            });
        });
    </script>

@endsection
