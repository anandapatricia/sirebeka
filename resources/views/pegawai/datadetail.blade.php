@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection

@section('main-content')
<div class="breadcrumb">
		<h1>Master Data Pegawai</h1>
	</div>
	<div class="separator-breadcrumb border-top"></div>
	<div class="row mb-4">
		<div class="col-md-12 mb-4">
			<div class="card text-left">
<div class="content-wrapper">
	<div class="panel-body">
	<div class="card-body">
	@foreach($detail as $detail)
	<form action="{{route('updatePegawai')}}" method="post" enctype="multipart/form-data">
	@csrf

          <div class="row">
		 	 <div class="col-md-2">
					<div class="form-group">
						<img src="{{url('/foto/'.$detail->foto)}}" class="img-thumbnail rounded d-block" width="200px" height="200px">
						<br>
						<input type="hidden" name="hidden_image" value="{{ $detail->foto}}" />
						<input type="hidden" name="id_users" value="{{ $detail->id_users}}" />
						<input type="file" class="form-control"  name="foto">
					</div>
				</div>
				<div class="col-md-10">
					<div class="form-group">
						<label>NAMA</label>
						<input type="text" class="form-control" id="nama" value="{{$detail->nama}}"  name="nama" >
					</div>

					<div class="form-group">
						<label>NIP</label>
						<input type="text" class="form-control" id="nip" value="{{$detail->nip}}"  name="nip" >
					</div>

					<div class="form-group">
						<label>NIK</label>
						<input type="text" class="form-control" id="nik" value="{{$detail->nik}}"  name="nik" >
					</div>
						<div class="form-group">
						<label>TANGGAL LAHIR</label>
						<input type="text" class="form-control" id="tanggal_lahir" value="{{$detail->tanggal_lahir}}"  name="tanggal_lahir" >
					</div>

					<div class="form-group">
						<label>TEMPAT LAHIR</label>
						<input type="text" class="form-control" id="tempat_lahir" value="{{$detail->tempat_lahir}}"   name="tempat_lahir" >
					</div>

				</div>
		  </div>

				<div class="row">
				<div class="col-md-6">
				<div class="form-group">
                     <label>JENIS KELAMIN</label>
                     <select class="form-control form-control-rounded" name="jenis_kelamin">
                        <option value="{{$detail->jenis_kelamin}}" >
						@if($detail->jenis_kelamin=='L')
						Laki-Laki

						@else
							Perempuan						
						@endif
						</option>
                        <option value="L">Laki-Laki</option>
                        <option value="P">Perempuan</option>
                     </select>
                  </div>

				<div class="form-group">
					<label>KARPEG</label>
					<input type="text" class="form-control" id="karpeg" value="{{$detail->tanggal_lahir}}"  name="karpeg" >
				</div>

				<div class="form-group">
					<label>UNIT</label>
					<input type="text" class="form-control" id="unit" value="{{$detail->unit}}"  name="unit" >
				</div>

				<div class="form-group">
					<label>KODE UNIT</label>
					<input type="text" class="form-control" id="kode_unit" value="{{$detail->kode_unit}}"  name="kode_unit" >
				</div>

				<div class="form-group">
					<label>KODE UNIT LAMA</label>
					<input type="text" class="form-control" id="kode_unit_lama" value="{{$detail->kode_unit_lama}}"   name="kode_unit_lama" >
				</div>

				<div class="form-group">
					<label>KANTOR</label>
					<input type="text" class="form-control" id="kantor" value="{{$detail->kantor}}"  name="kantor" >
				</div>

				<div class="form-group">
					<label>KODE KANTOR</label>
					<input type="text" class="form-control" id="kode_kantor" value="{{$detail->kode_kantor}}"  name="kode_kantor" >
				</div>

				<div class="form-group">
					<label>KODE KANTOR LAMA</label>
					<input type="text" class="form-control" id="kode_kantor_lama" value="{{$detail->kode_kantor_lama}}"  name="kode_kantor_lama" >
				</div>

				<div class="form-group">
					<label>SUBKANTOR</label>
					<input type="text" class="form-control" id="subkantor" value="{{$detail->subkantor}}"  name="subkantor" >
				</div>

				<div class="form-group">
					<label>KODE SUBKANTOR</label>
					<input type="text" class="form-control" id="kode_subkantor"  value="{{$detail->kode_subkantor}}" name="kode_subkantor" >
				</div>

				<div class="form-group">
					<label>TMT GOLONGAN</label>
					<input type="date" class="form-control" id="tmt_golongan" value="{{$detail->tmt_golongan}}"  name="tmt_golongan" >
				</div>

				<div class="form-group">
					<label>GOLONGAN</label>
					<input type="text" class="form-control" id="golongan" value="{{$detail->golongan}}" name="golongan" >
                </div>

                <div class="form-group">
                    <label>GOLONGAN</label>
                    <select class="form-control form-control-rounded" name="golongan_global">
                       <option value="" disabled selected>-- PILIH GOLONGAN ---</option>
                          @foreach($golongan as $row)
                          <option value="{{$row->id}}">{{$row->golongan}}</option>

                          @endforeach
                    </select>
                 </div>
            </div>

            <div class="col-md-6">
				<div class="form-group">
					<label>PANGKAT</label>
					<input type="text" class="form-control" id="pangkat" value="{{$detail->pangkat}}"  name="pangkat" >
				</div>

				<div class="form-group">
					<label>KODE ESELON</label>
					<input type="text" class="form-control" id="kode_eselon" value="{{$detail->kode_eselon}}"  name="kode_eselon" >
				</div>

				<div class="form-group">
					<label>ESELON</label>
					<input type="text" class="form-control" id="eselon" value="{{$detail->eselon}}"  name="eselon" >
				</div>

				<div class="form-group">
					<label>EMAIL</label>
					<input type="text" class="form-control" id="email" value="{{$detail->email}}" name="email" >
				</div>

				<div class="form-group">
					<label>PHONE</label>
					<input type="text" class="form-control" id="telepon" value="{{$detail->telepon}}" name="telepon" >
				</div>

				<div class="form-group">
					<label>TMT CPNS</label>
					<input type="date" class="form-control" id="tmt_cpns" value="{{$detail->tmt_cpns}}" name="tmt_cpns" >
				</div>

				<div class="form-group">
					<label>TMT PNS</label>
					<input type="text" class="form-control" id="tmt_pns" value="{{$detail->tmt_pns}}" name="tmt_pns" >
				</div>

				<div class="form-group">
					<label>PENDIDIKAN TERAKHIR</label>
					<input type="text" class="form-control" id="pendidikan_terakhir" value="{{$detail->pendidikan_terakhir}}" name="pendidikan_terakhir" >
				</div>

				<div class="form-group">
                  <label>JABATAN</label>
                  <select class="form-control form-control-rounded" name="id_jabatan">
                       <option value="{{$detail->id_jabatan}}"  selected>{{$detail->jabatan_baru}}</option>
				@endforeach
                        @foreach($jabatan as $row)
                        <option value="{{$row->id}}">{{$row->jabatan_baru}}</option>

                        @endforeach
                  </select>
               </div>
				</div>
				</div>

					<button type="submit" class="btn btn-primary">Simpan</button>
			</form>
	</div>
	</div>
</div>
</div>
</div>
</div>
@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
