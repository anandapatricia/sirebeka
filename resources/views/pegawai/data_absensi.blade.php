@extends('layouts.master')
@section('before-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">
@endsection
@section('main-content')
<div class="breadcrumb">
   <h1>Data Absensi </h1>
</div>
<div class="separator-breadcrumb border-top"></div>
<div class="row">
   <div class="col-lg-12 col-md-12">
               <table id="zero_configuration_table" class="display table table-striped table-bordered">
                  <thead>
                        <tr>
                           <th>No</th>
                           <th>NAMA</th>
                           <th>NIP</th>
                           <th>TANGGAL</th>
                           <th>MASUK</th>
                           <th>KELUAR</th>    
                        </tr>
                  </thead>
                  <tbody>
                 @foreach($absen_pegawai as $row)
                 <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$row->nama}}</td>
                    <td>{{$row->nip}}</td>
                    <td>{{$row->tanggal_absen}}</td>
                    <td>{{$row->clockin}}</td>
                    <td>{{$row->clockout}}</td>
                  </tr>
                 @endforeach
                  </tbody>
               </table>
           
   </div>
</div>


@endsection
@section('page-js')
<script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
				<script src="{{asset('assets/js/datatables.script.js')}}"></script>
@endsection
@section('bottom-js')
@endsection