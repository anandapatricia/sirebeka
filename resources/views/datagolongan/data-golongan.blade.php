
    <thead>
        <tr>
            <th>JENIS</th>
            <th>GOLONGAN</th>
            <th>PERSENTASE</th>
            <th>AKSI</th>
        </tr>
    </thead>
    <tbody>
        @foreach( $golongan as $golongan )
        <tr>
            <td>{{ $golongan['jenis'] }}</td>
            <td>{{ $golongan['golongan'] }}</td>
            <td>{{ $golongan['persen'] }}%</td>
            <td>
                <a href="{{ route('editgolongan', $golongan['id']) }}" class="btn btn-success btn-sm m-1" type="button">Edit</a>
            </td>
        </tr>
        @endforeach
    </tbody>
