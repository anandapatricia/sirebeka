@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
td{
    text-align : center;
}

th{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Master Data</h1>
            <ul>
                <li>Faktor Evaluasi</li>
                <li>Nilai Kelola Harta</li>
            </ul>
    </div>
        <div class="separator-breadcrumb border-top"></div>

            <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> 
                            <span class="nav-label">WEWENANG DAN NILAI KELOLA HARTA</span> 
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('f78table') }}">NILAI KELOLA HARTA</a></li>
                            <li><a href="{{ route('f78ktable') }}">NILAI KELOLA HARTA - KECIL</a></li>
                            <li><a href="{{ route('f78stable') }}">NILAI KELOLA HARTA - SEDANG</a></li>
                            <li><a href="{{ route('f78mtable') }}">NILAI KELOLA HARTA - MENENGAH</a></li>
                            <li><a href="{{ route('f78ttable') }}">NILAI KELOLA HARTA - TINGGI</a></li>
                        </ul>
                    </li>
                </ul>

            <!-- end of row -->

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="table-responsive">
                                <a href="{{ route('f78') }}" class="btn btn-primary">TAMBAH</a><br><br>
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th colspan="8">FAKTOR #7 dan #8 : WEWENANG DAN NILAI KELOLA HARTA</th>
                                        </tr>
                                        <tr>
                                            <th colspan="8">A.INDETERMINATE (TIDAK LANGSUNG)</th>
                                        </tr>
                                        <tr>
                                            <th colspan="2" rowspan="3">TINGKAT</th>
                                            <th colspan="5">NILAI KELOLA HARTA</th>
                                            <th colspan="2" rowspan="3">AKSI</th>
                                        </tr>
                                        <tr>
                                            <th>Umum</th>
                                            <th>Standar</th>
                                            <th>Menengah</th>
                                            <th>Menengah Tinggi</th>
                                            <th>Tinggi</th>
                                        </tr>
                                        <tr>
                                            <th>U</th>
                                            <th>S</th>
                                            <th>M</th>
                                            <th>MT</th>
                                            <th>T</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($f78 as $f78)
                                        <tr>
                                            <td>{{ strtoupper($f78['jenis']) }}</td>
                                            <td>{{ strtoupper($f78['kelas']) }}</td>
                                            <td>{{ strtoupper($f78['umum']) }}</td>
                                            <td>{{ strtoupper($f78['standar']) }}</td>
                                            <td>{{ strtoupper($f78['menengah']) }}</td>
                                            <td>{{ strtoupper($f78['menengah_tinggi']) }}</td>
                                            <td>{{ strtoupper($f78['tinggi']) }}</td>
                                            <td>
                                                <a href="{{ route('f78edit', $f78['id']) }}" class="btn btn-success btn-sm m-1" type="button">Edit</a>
                                                <!-- <button class="btn btn-danger btn-sm m-1" type="button">Delete</button> -->
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

            <!-- end of row

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
