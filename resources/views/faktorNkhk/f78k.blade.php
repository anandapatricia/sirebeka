@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
td{
    text-align : center;
}

th{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Master Data</h1>
            <ul>
                <li>Faktor Evaluasi</li>
                <li>Wewenang dan Nilai Kelola Harta</li>
            </ul>
    </div>

        <div class="separator-breadcrumb border-top"></div>
            <body>
                <div class="row mb-4">
                    <div class="col-md-12 mb-4">
                        <div class="card text-left">
                            <div class="card-body">
                                <h2>NILAI KELOLA HARTA - KECIL</h2><br/>
                                <form method="post" action="{{ route('f78k.save')}}">
                                @csrf
                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="jenis">JENIS:</label>
                                            <input id="jenis" type="jenis" class="form-control" name="jenis" >
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="kelas">KELAS:</label>
                                            <input id="kelas" class="form-control" name="kelas">
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="r">R :</label>
                                            <input id="r" class="form-control" name="r">
                                        </div>
                                    </div>

                                     <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="k">K :</label>
                                            <input id="k" class="form-control" name="k">
                                        </div>
                                    </div>

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="b">B :</label>
                                            <input id="b" class="form-control" name="b">
                                        </div>
                                    </div> 
                                    
                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4">
                                            <label for="u">U :</label>
                                            <input id="u" class="form-control" name="u">
                                        </div>
                                    </div> 

                                    <div class="row">
                                    <div class="col-md-12"></div>
                                        <div class="form-group col-md-4" style="margin-top:10px">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="{{ route('f78ktable') }}" class="btn btn-warning">Kembali</a>
                                        </div>
                                    </div>
                                </form>      
                            </div>
                        </div>
                    </div>
                </div>
            </body>


@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
