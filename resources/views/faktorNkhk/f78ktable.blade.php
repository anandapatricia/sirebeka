@extends('layouts.master')
@section('page-css')

<link rel="stylesheet" href="{{asset('assets/styles/vendor/datatables.min.css')}}">

<style>
td{
    text-align : center;
}

th{
    text-align : center;
}
</style>

@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Master Data</h1>
            <ul>
                <li>Faktor Evaluasi</li>
                <li>Nilai Kelola Harta - Kecil</li>
            </ul>
    </div>
    
        <div class="separator-breadcrumb border-top"></div>

            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> 
                        <span class="nav-label">WEWENANG DAN NILAI KELOLA HARTA</span> 
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('f78table') }}">NILAI KELOLA HARTA</a></li>
                        <li><a href="{{ route('f78ktable') }}">NILAI KELOLA HARTA - KECIL</a></li>
                        <li><a href="{{ route('f78stable') }}">NILAI KELOLA HARTA - SEDANG</a></li>
                        <li><a href="{{ route('f78mtable') }}">NILAI KELOLA HARTA - MENENGAH</a></li>
                        <li><a href="{{ route('f78ttable') }}">NILAI KELOLA HARTA - TINGGI</a></li>
                    </ul>
                </li>
            </ul>


            <!-- end of row -->

            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <div class="table-responsive">
                                <a href="{{ route('f78k') }}" class="btn btn-primary">TAMBAH</a><br><br>
                                <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th colspan="7">FAKTOR #7 dan #8 : WEWENANG DAN NILAI KELOLA HARTA</th>
                                        </tr>
                                        <tr>
                                            <th colspan="7">B. LANGSUNG</th>
                                        </tr>
                                        <tr>
                                            <th colspan="2" rowspan="3">TINGKAT</th>
                                            <th colspan="4">NILAI KELOLA HARTA - KECIL</th>
                                            <th colspan="2" rowspan="3">AKSI</th>
                                        </tr>
                                        <tr>
                                            <th colspan="4">PERAN JABATAN</th>
                                        </tr>
                                        <tr>
                                            <th>R</th>
                                            <th>K</th>
                                            <th>B</th>
                                            <th>U</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($f78k as $f78k)
                                        <tr>
                                            <td>{{ strtoupper($f78k['jenis']) }}</td>
                                            <td>{{ strtoupper($f78k['kelas']) }}</td>
                                            <td>{{ strtoupper($f78k['r']) }}</td>
                                            <td>{{ strtoupper($f78k['k']) }}</td>
                                            <td>{{ strtoupper($f78k['b']) }}</td>
                                            <td>{{ strtoupper($f78k['u']) }}</td>
                                            <td>
                                                <a href="{{ route('f78kedit', $f78k['id']) }}" class="btn btn-success btn-sm m-1" type="button">Edit</a>
                                                <!-- <button class="btn btn-danger btn-sm m-1" type="button">Delete</button> -->
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 

            <!-- end of row

@endsection

@section('page-js')

 <script src="{{asset('assets/js/vendor/datatables.min.js')}}"></script>
    <script src="{{asset('assets/js/datatables.script.js')}}"></script>

@endsection
