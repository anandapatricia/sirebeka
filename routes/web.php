<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('sessions.signIn');
// });

// Route::view('/', 'starter')->name('starter');
Route::get('large-compact-sidebar/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'compact']);
    return view('dashboard.dashboardv1');
})->name('compact');

Route::get('large-sidebar/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'normal']);
    return view('dashboard.dashboardv1');
})->name('normal');

Route::get('horizontal-bar/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'horizontal']);
    return view('dashboard.dashboardv1');
})->name('horizontal');

Route::get('vertical/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'vertical']);
    return view('dashboard.dashboardv1');
})->name('vertical');

Route::view('dashboard/dashboard1', 'dashboard.dashboardv1')->name('dashboard_version_1');
Route::view('dashboard/dashboard2', 'dashboard.dashboardv2')->name('dashboard_version_2');
Route::view('dashboard/dashboard3', 'dashboard.dashboardv3')->name('dashboard_version_3');
Route::view('dashboard/dashboard4', 'dashboard.dashboardv4')->name('dashboard_version_4');

// uiKits
Route::view('uikits/alerts', 'uiKits.alerts')->name('alerts');
Route::view('uikits/accordion', 'uiKits.accordion')->name('accordion');
Route::view('uikits/buttons', 'uiKits.buttons')->name('buttons');
Route::view('uikits/badges', 'uiKits.badges')->name('badges');
Route::view('uikits/bootstrap-tab', 'uiKits.bootstrap-tab')->name('bootstrap-tab');
Route::view('uikits/carousel', 'uiKits.carousel')->name('carousel');
Route::view('uikits/collapsible', 'uiKits.collapsible')->name('collapsible');
Route::view('uikits/lists', 'uiKits.lists')->name('lists');
Route::view('uikits/pagination', 'uiKits.pagination')->name('pagination');
Route::view('uikits/popover', 'uiKits.popover')->name('popover');
Route::view('uikits/progressbar', 'uiKits.progressbar')->name('progressbar');
Route::view('uikits/tables', 'uiKits.tables')->name('tables');
Route::view('uikits/tabs', 'uiKits.tabs')->name('tabs');
Route::view('uikits/tooltip', 'uiKits.tooltip')->name('tooltip');
Route::view('uikits/modals', 'uiKits.modals')->name('modals');
Route::view('uikits/NoUislider', 'uiKits.NoUislider')->name('NoUislider');
Route::view('uikits/cards', 'uiKits.cards')->name('cards');
Route::view('uikits/cards-metrics', 'uiKits.cards-metrics')->name('cards-metrics');
Route::view('uikits/typography', 'uiKits.typography')->name('typography');

// extra kits
Route::view('extrakits/dropDown', 'extraKits.dropDown')->name('dropDown');
Route::view('extrakits/imageCroper', 'extraKits.imageCroper')->name('imageCroper');
Route::view('extrakits/loader', 'extraKits.loader')->name('loader');
Route::view('extrakits/laddaButton', 'extraKits.laddaButton')->name('laddaButton');
Route::view('extrakits/toastr', 'extraKits.toastr')->name('toastr');
Route::view('extrakits/sweetAlert', 'extraKits.sweetAlert')->name('sweetAlert');
Route::view('extrakits/tour', 'extraKits.tour')->name('tour');
Route::view('extrakits/upload', 'extraKits.upload')->name('upload');


// Apps
Route::view('apps/invoice', 'apps.invoice')->name('invoice');
Route::view('apps/inbox', 'apps.inbox')->name('inbox');
Route::view('apps/chat', 'apps.chat')->name('chat');
Route::view('apps/calendar', 'apps.calendar')->name('calendar');
Route::view('apps/task-manager-list', 'apps.task-manager-list')->name('task-manager-list');
Route::view('apps/task-manager', 'apps.task-manager')->name('task-manager');
Route::view('apps/toDo', 'apps.toDo')->name('toDo');
Route::view('apps/ecommerce/products', 'apps.ecommerce.products')->name('ecommerce-products');
Route::view('apps/ecommerce/product-details', 'apps.ecommerce.product-details')->name('ecommerce-product-details');
Route::view('apps/ecommerce/cart', 'apps.ecommerce.cart')->name('ecommerce-cart');
Route::view('apps/ecommerce/checkout', 'apps.ecommerce.checkout')->name('ecommerce-checkout');


Route::view('apps/contacts/lists', 'apps.contacts.lists')->name('contacts-lists');
Route::view('apps/contacts/contact-details', 'apps.contacts.contact-details')->name('contact-details');
Route::view('apps/contacts/grid', 'apps.contacts.grid')->name('contacts-grid');
Route::view('apps/contacts/contact-list-table', 'apps.contacts.contact-list-table')->name('contact-list-table');

// forms
Route::view('forms/basic-action-bar', 'forms.basic-action-bar')->name('basic-action-bar');
Route::view('forms/multi-column-forms', 'forms.multi-column-forms')->name('multi-column-forms');
Route::view('forms/smartWizard', 'forms.smartWizard')->name('smartWizard');
Route::view('forms/tagInput', 'forms.tagInput')->name('tagInput');
Route::view('forms/forms-basic', 'forms.forms-basic')->name('forms-basic');
Route::view('forms/forms-user', 'forms.forms-user')->name('forms-user');
Route::view('forms/forms-user', 'forms.forms-user')->name('forms-user');
Route::view('forms/form-layouts', 'forms.form-layouts')->name('form-layouts');
Route::view('forms/form-input-group', 'forms.form-input-group')->name('form-input-group');
Route::view('forms/form-validation', 'forms.form-validation')->name('form-validation');
Route::view('forms/form-editor', 'forms.form-editor')->name('form-editor');

// Charts
Route::view('charts/echarts', 'charts.echarts')->name('echarts');
Route::view('charts/chartjs', 'charts.chartjs')->name('chartjs');
Route::view('charts/apexLineCharts', 'charts.apexLineCharts')->name('apexLineCharts');
Route::view('charts/apexAreaCharts', 'charts.apexAreaCharts')->name('apexAreaCharts');
Route::view('charts/apexBarCharts', 'charts.apexBarCharts')->name('apexBarCharts');
Route::view('charts/apexColumnCharts', 'charts.apexColumnCharts')->name('apexColumnCharts');
Route::view('charts/apexRadialBarCharts', 'charts.apexRadialBarCharts')->name('apexRadialBarCharts');
Route::view('charts/apexRadarCharts', 'charts.apexRadarCharts')->name('apexRadarCharts');
Route::view('charts/apexPieDonutCharts', 'charts.apexPieDonutCharts')->name('apexPieDonutCharts');
Route::view('charts/apexSparklineCharts', 'charts.apexSparklineCharts')->name('apexSparklineCharts');
Route::view('charts/apexScatterCharts', 'charts.apexScatterCharts')->name('apexScatterCharts');
Route::view('charts/apexBubbleCharts', 'charts.apexBubbleCharts')->name('apexBubbleCharts');
Route::view('charts/apexCandleStickCharts', 'charts.apexCandleStickCharts')->name('apexCandleStickCharts');
Route::view('charts/apexMixCharts', 'charts.apexMixCharts')->name('apexMixCharts');

// datatables
Route::view('datatables/basic-tables', 'datatables.basic-tables')->name('basic-tables');
Route::view('datatables/tabel-user', 'datatables.tabel-user')->name('tabel-user');

// sessions
Route::view('sessions/signIn', 'sessions.signIn')->name('signIn');;
Route::view('sessions/signUp', 'sessions.signUp')->name('signUp');
Route::view('sessions/forgot', 'sessions.forgot')->name('forgot');

// widgets
Route::view('widgets/card', 'widgets.card')->name('widget-card');
Route::view('widgets/statistics', 'widgets.statistics')->name('widget-statistics');
Route::view('widgets/list', 'widgets.list')->name('widget-list');
Route::view('widgets/app', 'widgets.app')->name('widget-app');
Route::view('widgets/weather-app', 'widgets.weather-app')->name('widget-weather-app');

// others
Route::view('others/notFound', 'others.notFound')->name('notFound');
Route::view('others/user-profile', 'others.user-profile')->name('user-profile');
Route::view('others/starter', 'starter')->name('starter');
Route::view('others/faq', 'others.faq')->name('faq');
Route::view('others/pricing-table', 'others.pricing-table')->name('pricing-table');
Route::view('others/search-result', 'others.search-result')->name('search-result');

use App\Http\Controllers\Controller;

use App\Http\Controllers\HomeController;
Route::get('pegawai/data', [HomeController::class,'datapegawai'])->name('data');
Route::get('pegawai', [HomeController::class,'index'])->name('data.index');
Route::post('pegawai/datadetail', [HomeController::class, 'datadetail'])->name('datadetail');
Route::get('pegawai/sik', [HomeController::class, 'datasik'])->name('data.sik');
Route::post('pegawai/syncPegawai', [HomeController::class,'syncPegawai'])->name('syncPegawai');
Route::post('pegawai/insertData', [HomeController::class,'insertData'])->name('pegawai.save');
Route::get('pegawai/detailPegawai/{id_users}',[HomeController::class,'detailPegawai'])->name('detailPegawai');
Route::post('pegawai/updatePegawai',[HomeController::class,'updatePegawai'])->name('updatePegawai');
Route::get('getSIK', [HomeController::class,'getSIK'])->name('getSIK');
Route::post('pegawai/save',[HomeController::class,'save'])->name('save');
Route::get('absensi/{nip}', [HomeController::class,'getAbsensiByID'])->name('absensi');

use App\Http\Controllers\RemunerasiController;
Route::get('remunerasi',[RemunerasiController::class,'index'])->name('remunerasi');
Route::get('remunerasi/hasil',[RemunerasiController::class,'hitung'])->name('remunerasi.data');
Route::post('remunerasi/generate',[RemunerasiController::class,'generate'])->name('remunerasi.generate');
Route::get('remunerasi/detail/{bulan}',[RemunerasiController::class,'show'])->name('remunerasi.detail');
Route::post('remunerasi/save',[RemunerasiController::class,'store'])->name('remunerasi.save');
Route::post('remunerasi/adjustment',[RemunerasiController::class,'adjustment'])->name('remunerasi.adjustment');

use App\Http\Controllers\PirController;
Route::get('pir',[PirController::class,'index'])->name('pir');
Route::post('pir/save',[PirController::class,'store'])->name('pir.save');
Route::get('pir/edit/{id}',[PirController::class,'show'])->name('pir.edit');
Route::get('pir/detail/{id}',[PirController::class,'edit'])->name('pir.detail');
Route::get('pir/delete/{id}',[PirController::class,'destroy'])->name('pir.delete');

use App\Http\Controllers\ImportAbsensiController;
Route::get('importabsensi',[ImportAbsensiController::class,'index'])->name('importabsensi');
Route::post('importabsensi/imports',[ImportAbsensiController::class,'import_excel'])->name('importabsensi.imports');
Route::get('importabsensi/getAbsensi',[ImportAbsensiController::class,'getAbsensi'])->name('importabsensi.data.absensi');

// Auth::routes();

Route::get('/', function () {return view('sessions.signIn');})->name('signIn');

use App\Http\Controllers\SigninController;

Route::post('signIn/proses', [SigninController::class, 'signIn'])->name('signIn.proses');

use App\Http\Controllers\SignupController;

Route::get('register', [SignupController::class, 'register'])->name('register');
Route::post('register/save', [SignupController::class, 'register_save'])->name('register.save');
Route::view('auth/verify', 'auth.verify')->name('verify');
Route::view('auth/passwords', 'auth.passwords')->name('passwords');

// Route::post('pegawai/save', [HomeController::class, 'pegawai_save'])->name('pegawai.save');

use App\Http\Controllers\MainController;
use App\Http\Controllers\DashboardController;

Route::group(['middleware' => 'auth'], function () {


// dashboard
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::get('/logout', [MainController::class, 'logout'])->name('logout');

});

use App\Http\Controllers\DatamasterController;

//faktor satu
Route::get('faktorsatu/f1_kompetensi_teknis_table', [DatamasterController::class, 'f1_kompetensi_teknis_table'])->name('f1_kompetensi_teknis_table');
Route::get('f1_kompetensi_teknis/jenis', [DatamasterController::class, 'jenis'])->name('jenis');
Route::get('f1_kompetensi_teknis/tipe', [DatamasterController::class, 'tipe'])->name('tipe');
Route::get('f1_kompetensi_teknis/nilai', [DatamasterController::class, 'nilai'])->name('nilai');
Route::get('faktorsatu/f1_kompetensi_teknis', [DatamasterController::class, 'f1_kompetensi_teknis'])->name('f1_kompetensi_teknis');
Route::post('f1_kompetensi_teknis/save', [DatamasterController::class, 'f1_kompetensi_teknis_save'])->name('f1_kompetensi_teknis.save');
Route::get('f1edit/{id}', [DatamasterController::class, 'f1edit'])->name('f1edit');
Route::post('f1update/{id}', [DatamasterController::class, 'f1update'])->name('f1update');

//faktordua
Route::get('faktordua/f2table', [DatamasterController::class, 'f2table'])->name('f2table');
Route::get('f2/jenis', [DatamasterController::class, 'jenis'])->name('jenis');
Route::get('f2/kelas', [DatamasterController::class, 'kelas'])->name('kelas');
Route::get('f2/tipe', [DatamasterController::class, 'tipe'])->name('tipe');
Route::get('f2/lingkup', [DatamasterController::class, 'lingkup'])->name('lingkup');
Route::get('f2/nilai', [DatamasterController::class, 'nilai'])->name('nilai');
Route::get('faktordua/f2', [DatamasterController::class, 'f2'])->name('f2');
Route::post('f2/save', [DatamasterController::class, 'f2_save'])->name('f2.save');
Route::get('f2edit/{id}', [DatamasterController::class, 'f2edit'])->name('f2edit');
Route::post('f2update/{id}', [DatamasterController::class, 'f2update'])->name('f2update');

//faktortiga
Route::get('faktortiga/f3table', [DatamasterController::class, 'f3table'])->name('f3table');
Route::get('f3/jenis', [DatamasterController::class, 'jenis'])->name('jenis');
Route::get('f3/kelas', [DatamasterController::class, 'kelas'])->name('kelas');
Route::get('f3/nilai', [DatamasterController::class, 'nilai'])->name('nilai');
Route::get('faktortiga/f3', [DatamasterController::class, 'f3'])->name('f3');
Route::post('f3/save', [DatamasterController::class, 'f3_save'])->name('f3.save');
Route::get('f3edit/{id}', [DatamasterController::class, 'f3edit'])->name('f3edit');
Route::post('f3update/{id}', [DatamasterController::class, 'f3update'])->name('f3update');

//faktorempat
Route::get('faktorempat/f4table', [DatamasterController::class, 'f4table'])->name('f4table');
Route::get('f4/jenis', [DatamasterController::class, 'jenis'])->name('jenis');
Route::get('f4/tipe', [DatamasterController::class, 'tipe'])->name('tipe');
Route::get('f4/nilai', [DatamasterController::class, 'nilai'])->name('nilai');
Route::get('faktorempat/f4', [DatamasterController::class, 'f4'])->name('f4');
Route::post('f4/save', [DatamasterController::class, 'f4_save'])->name('f4.save');
Route::get('f4edit/{id}', [DatamasterController::class, 'f4edit'])->name('f4edit');
Route::post('f4update/{id}', [DatamasterController::class, 'f4update'])->name('f4update');

//faktorlima
Route::get('faktorlima/f5table', [DatamasterController::class, 'f5table'])->name('f5table');
Route::get('f5/jenis', [DatamasterController::class, 'jenis'])->name('jenis');
Route::get('f5/tipe', [DatamasterController::class, 'tipe'])->name('tipe');
Route::get('f5/nilai', [DatamasterController::class, 'nilai'])->name('nilai');
Route::get('faktorlima/f5', [DatamasterController::class, 'f5'])->name('f5');
Route::post('f5/save', [DatamasterController::class, 'f5_save'])->name('f5.save');
Route::get('f5edit/{id}', [DatamasterController::class, 'f5edit'])->name('f5edit');
Route::post('f5update/{id}', [DatamasterController::class, 'f5update'])->name('f5update');

//faktorenam
Route::get('faktorenam/f6table', [DatamasterController::class, 'f6table'])->name('f6table');
Route::get('f6/jenis', [DatamasterController::class, 'jenis'])->name('jenis');
Route::get('f6/tipe', [DatamasterController::class, 'tipe'])->name('tipe');
Route::get('f6/kode', [DatamasterController::class, 'kode'])->name('kode');
Route::get('f6/kelas', [DatamasterController::class, 'kelas'])->name('kelas');
Route::get('f6/nilai', [DatamasterController::class, 'nilai'])->name('nilai');
Route::get('faktorenam/f6', [DatamasterController::class, 'f6'])->name('f6');
Route::post('f6/save', [DatamasterController::class, 'f6_save'])->name('f6.save');
Route::get('f6edit/{id}', [DatamasterController::class, 'f6edit'])->name('f6edit');
Route::post('f6update/{id}', [DatamasterController::class, 'f6update'])->name('f6update');

//faktorsepuluh
Route::get('faktorsepuluh/f10table', [DatamasterController::class, 'f10table'])->name('f10table');
Route::get('f10/jenis', [DatamasterController::class, 'jenis'])->name('jenis');
Route::get('f10/tipe', [DatamasterController::class, 'tipe'])->name('tipe');
Route::get('f10/nilai', [DatamasterController::class, 'nilai'])->name('nilai');
Route::get('faktorsepuluh/f10', [DatamasterController::class, 'f10'])->name('f10');
Route::post('f10/save', [DatamasterController::class, 'f10_save'])->name('f10.save');
Route::get('f10edit/{id}', [DatamasterController::class, 'f10edit'])->name('f10edit');
Route::post('f10update/{id}', [DatamasterController::class, 'f10update'])->name('f10update');

use App\Http\Controllers\FaktorNKHController;

//faktorNkh
Route::get('faktorNkh/f78table', [FaktorNKHController::class, 'f78table'])->name('f78table');
Route::get('f78/jenis', [FaktorNKHController::class, 'jenis'])->name('jenis');
Route::get('f78/kelas', [FaktorNKHController::class, 'kelas'])->name('kelas');
Route::get('f78/umum', [FaktorNKHController::class, 'umum'])->name('umum');
Route::get('f78/standar', [FaktorNKHController::class, 'standar'])->name('standar');
Route::get('f78/menengah', [FaktorNKHController::class, 'menengah'])->name('menengah');
Route::get('f78/menengah_tinggi', [FaktorNKHController::class, 'menengah_tinggi'])->name('menengah_tinggi');
Route::get('f78/tinggi', [FaktorNKHController::class, 'tinggi'])->name('tinggi');
Route::get('faktorNkh/f78', [FaktorNKHController::class, 'f78'])->name('f78');
Route::post('f78/save', [FaktorNKHController::class, 'f78_save'])->name('f78.save');
Route::get('f78edit/{id}', [FaktorNKHController::class, 'f78edit'])->name('f78edit');
Route::post('f78update/{id}', [FaktorNKHController::class, 'f78update'])->name('f78update');

//faktorNkhk
Route::get('faktorNkhk/f78ktable', [FaktorNKHController::class, 'f78ktable'])->name('f78ktable');
Route::get('f78k/jenis', [FaktorNKHController::class, 'jenis'])->name('jenis');
Route::get('f78k/kelas', [FaktorNKHController::class, 'kelas'])->name('kelas');
Route::get('f78k/r', [FaktorNKHController::class, 'r'])->name('r');
Route::get('f78k/k', [FaktorNKHController::class, 'k'])->name('k');
Route::get('f78k/b', [FaktorNKHController::class, 'b'])->name('b');
Route::get('f78k/u', [FaktorNKHController::class, 'u'])->name('u');
Route::get('faktorNkhk/f78k', [FaktorNKHController::class, 'f78k'])->name('f78k');
Route::post('f78k/save', [FaktorNKHController::class, 'f78k_save'])->name('f78k.save');
Route::get('f78kedit/{id}', [FaktorNKHController::class, 'f78kedit'])->name('f78kedit');
Route::post('f78kupdate/{id}', [FaktorNKHController::class, 'f78kupdate'])->name('f78kupdate');

//faktorNkhs
Route::get('faktorNkhs/f78stable', [FaktorNKHController::class, 'f78stable'])->name('f78stable');
Route::get('f78s/jenis', [FaktorNKHController::class, 'jenis'])->name('jenis');
Route::get('f78s/kelas', [FaktorNKHController::class, 'kelas'])->name('kelas');
Route::get('f78s/r', [FaktorNKHController::class, 'r'])->name('r');
Route::get('f78s/k', [FaktorNKHController::class, 'k'])->name('k');
Route::get('f78s/b', [FaktorNKHController::class, 'b'])->name('b');
Route::get('f78s/u', [FaktorNKHController::class, 'u'])->name('u');
Route::get('faktorNkhs/f78s', [FaktorNKHController::class, 'f78s'])->name('f78s');
Route::post('f78s/save', [FaktorNKHController::class, 'f78s_save'])->name('f78s.save');
Route::get('f78sedit/{id}', [FaktorNKHController::class, 'f78sedit'])->name('f78sedit');
Route::post('f78supdate/{id}', [FaktorNKHController::class, 'f78supdate'])->name('f78supdate');

//faktorNkhm
Route::get('faktorNkhm/f78mtable', [FaktorNKHController::class, 'f78mtable'])->name('f78mtable');
Route::get('f78m/jenis', [FaktorNKHController::class, 'jenis'])->name('jenis');
Route::get('f78m/kelas', [FaktorNKHController::class, 'kelas'])->name('kelas');
Route::get('f78m/r', [FaktorNKHController::class, 'r'])->name('r');
Route::get('f78m/k', [FaktorNKHController::class, 'k'])->name('k');
Route::get('f78m/b', [FaktorNKHController::class, 'b'])->name('b');
Route::get('f78m/u', [FaktorNKHController::class, 'u'])->name('u');
Route::get('faktorNkhm/f78m', [FaktorNKHController::class, 'f78m'])->name('f78m');
Route::post('f78m/save', [FaktorNKHController::class, 'f78m_save'])->name('f78m.save');
Route::get('f78medit/{id}', [FaktorNKHController::class, 'f78medit'])->name('f78medit');
Route::post('f78mupdate/{id}', [FaktorNKHController::class, 'f78mupdate'])->name('f78mupdate');

//faktorNkht
Route::get('faktorNkht/f78ttable', [FaktorNKHController::class, 'f78ttable'])->name('f78ttable');
Route::get('f78t/jenis', [FaktorNKHController::class, 'jenis'])->name('jenis');
Route::get('f78t/kelas', [FaktorNKHController::class, 'kelas'])->name('kelas');
Route::get('f78t/r', [FaktorNKHController::class, 'r'])->name('r');
Route::get('f78t/k', [FaktorNKHController::class, 'k'])->name('k');
Route::get('f78t/b', [FaktorNKHController::class, 'b'])->name('b');
Route::get('f78t/u', [FaktorNKHController::class, 'u'])->name('u');
Route::get('faktorNkht/f78t', [FaktorNKHController::class, 'f78t'])->name('f78t');
Route::post('f78t/save', [FaktorNKHController::class, 'f78t_save'])->name('f78t.save');
Route::get('f78tedit/{id}', [FaktorNKHController::class, 'f78tedit'])->name('f78tedit');
Route::post('f78tupdate/{id}', [FaktorNKHController::class, 'f78tupdate'])->name('f78tupdate');

use App\Http\Controllers\FaktorsembilanController;

//faktorsembilanI
Route::get('faktorsembilanI/fi9table', [FaktorsembilanController::class, 'fi9table'])->name('fi9table');
Route::get('fi9/jenis', [FaktorsembilanController::class, 'jenis'])->name('jenis');
Route::get('fi9/tipe', [FaktorsembilanController::class, 'tipe'])->name('tipe');
Route::get('fi9/nilai', [FaktorsembilanController::class, 'nilai'])->name('nilai');
Route::get('faktorsembilanI/fi9', [FaktorsembilanController::class, 'fi9'])->name('fi9');
Route::post('fi9/save', [FaktorsembilanController::class, 'fi9_save'])->name('fi9.save');
Route::get('fi9edit/{id}', [FaktorsembilanController::class, 'fi9edit'])->name('fi9edit');
Route::post('fi9update/{id}', [FaktorsembilanController::class, 'fi9update'])->name('fi9update');

//faktorsembilanD
Route::get('faktorsembilanD/fd9table', [FaktorsembilanController::class, 'fd9table'])->name('fd9table');
Route::get('fd9/jenis', [FaktorsembilanController::class, 'jenis'])->name('jenis');
Route::get('fd9/tipe', [FaktorsembilanController::class, 'tipe'])->name('tipe');
Route::get('fd9/nilai', [FaktorsembilanController::class, 'nilai'])->name('nilai');
Route::get('faktorsembilanD/fd9', [FaktorsembilanController::class, 'fd9'])->name('fd9');
Route::post('fd9/save', [FaktorsembilanController::class, 'fd9_save'])->name('fd9.save');
Route::get('fd9edit/{id}', [FaktorsembilanController::class, 'fd9edit'])->name('fd9edit');
Route::post('fd9update/{id}', [FaktorsembilanController::class, 'fd9update'])->name('fd9update');

use App\Http\Controllers\masterJabatanController;

Route::get('masterJabatan', [masterJabatanController::class, 'mjabatan'])->name('mjabatan');
Route::resource('jabatan','masterJabatanController');
Route::get('tambahJabatan', [masterJabatanController::class, 'formjabatan'])->name('formjabatan');
Route::post('saveJabatan', [masterJabatanController::class, 'savejabatan'])->name('savejabatan');
Route::get('editJabatan/{id}', [masterJabatanController::class, 'edit'])->name('editjabatan');
Route::post('updateJabatan/{id}', [masterJabatanController::class, 'update'])->name('updatejabatan');

use App\Http\Controllers\MasterMasaKerjaController;

Route::get('masterMasaKerja', [MasterMasaKerjaController::class, 'mmasakerja'])->name('mmasakerja');
Route::resource('masakerja','MasterMasaKerjaController');
Route::get('tambahMasaKerja', [MasterMasaKerjaController::class, 'formmasakerja'])->name('formmasakerja');
Route::post('saveMasaKErja', [MasterMasaKerjaController::class, 'savemasakerja'])->name('savemasakerja');
Route::get('editMasaKerja/{id}', [MasterMasaKerjaController::class, 'edit'])->name('editmasakerja');
Route::post('updateMasaKerja/{id}', [MasterMasaKerjaController::class, 'update'])->name('updatemasakerja');

use App\Http\Controllers\TugasJabatanController;

Route::get('tugasJabatan/{id}', [TugasJabatanController::class, 'tjabatan'])->name('tjabatan');
Route::get('dataTugasJabatan/{id}', [TugasJabatanController::class, 'tugasjabatan'])->name('tugasjabatan');
Route::post('saveTugasJabatan', [TugasJabatanController::class, 'savetugasjabatan'])->name('savetugasjabatan');
Route::get('editTugasJabatan/{id}', [TugasJabatanController::class, 'edittugasjabatan'])->name('edittugasjabatan');
Route::post('updateTugasJabatan/{id}', [TugasJabatanController::class, 'updatetugasjabatan'])->name('updatetugasjabatan');
Route::resource('tugasjabatan','TugasJabatanController');

use App\Http\Controllers\SkpController;
Route::get('SKP', [SkpController::class, 'index'])->name('skp');
Route::get('TambahSKP', [SkpController::class, 'formskp'])->name('tambahskp');
Route::get('TambahSKP/{id}', [SkpController::class, 'detailskp'])->name('tambahskp.detail');
Route::get('UbahSKP/{id}', [SkpController::class, 'detailskp'])->name('ubah.skp');
Route::get('TambahSKP/{id_header}/{id}', [SkpController::class, 'editskp'])->name('editskp.detail');
Route::post('saveSKP/{id_skp_header}', [SkpController::class, 'saveskp'])->name('saveskp');
Route::get('KirimSKP/{id}', [SkpController::class, 'kirimskp'])->name('skp.kirim');
Route::get('skp/buat', [SkpController::class, 'buatskp'])->name('buat.skp');
Route::post('skp/save', [SkpController::class, 'saveskp'])->name('skp.save');
Route::get('skp/iku', [SkpController::class, 'buat_iku'])->name('buat.iku');
Route::post('skp/iku/save', [SkpController::class, 'skp_iku_save'])->name('skp.iku.save');
Route::post('skp/iku/tugas_tambahan', [SkpController::class, 'skp_tugas_tambahan'])->name('skp.tugas.tambahan');
Route::get('skp/buat/realisasi/{id}', [SkpController::class, 'buat_realisasi'])->name('skp.buat.realisasi');
Route::post('skp/realisasi/save', [SkpController::class, 'save_realisasi_skp'])->name('skp.realisasi.save');
Route::get('skp/detail/realisasi/{tahun}', [SkpController::class, 'detail_realisasi'])->name('skp.detail.realisasi');
Route::get('skp/detail/realisasi/data/{id}', [SkpController::class, 'view_realisasi'])->name('skp.detail.realisasi.data');
Route::get('skp/detail/realisasi/data/kirim/{id}', [SkpController::class, 'kirim_realisasi'])->name('kirim.realisasi');

use App\Http\Controllers\masterGradeController;

Route::get('masterGrade', [masterGradeController::class, 'mgrade'])->name('mgrade');
Route::resource('grade','masterGradeController');
Route::get('tambahGrade', [masterGradeController::class, 'formgrade'])->name('formgrade');
Route::post('saveGrade', [masterGradeController::class, 'savegrade'])->name('savegrade');
Route::get('editGrade/{id}', [masterGradeController::class, 'edit'])->name('editgrade');
Route::post('updateGrade/{id}', [masterGradeController::class, 'update'])->name('updategrade');

use App\Http\Controllers\masterGolonganController;

Route::get('masterGolongan', [masterGolonganController::class, 'mgolongan'])->name('mgolongan');
Route::resource('golongan','masterGolonganController');
Route::get('tambahGolongan', [masterGolonganController::class, 'formgolongan'])->name('formgolongan');
Route::post('saveGolongan', [masterGolonganController::class, 'savegolongan'])->name('savegolongan');
Route::get('editGolongan/{id}', [masterGolonganController::class, 'edit'])->name('editgolongan');
Route::post('updateGolongan/{id}', [masterGolonganController::class, 'update'])->name('updategolongan');

use App\Http\Controllers\MasterIKUController;

Route::get('iku/ikutable', [MasterIKUController::class, 'ikutable'])->name('ikutable');
Route::get('m_iku/iku', [MasterIKUController::class, 'iku'])->name('iku');
Route::get('m_iku/tahun', [MasterIKUController::class, 'tahun'])->name('tahun');
Route::get('iku/iku', [MasterIKUController::class, 'iku'])->name('iku');
Route::post('iku/save', [MasterIKUController::class, 'iku_save'])->name('iku.save');
Route::get('ikuedit/{id}', [MasterIKUController::class, 'ikuedit'])->name('ikuedit');
Route::post('ikuupdate/{id}', [MasterIKUController::class, 'ikuupdate'])->name('ikuupdate');

use App\Http\Controllers\CutiController;

Route::get('cuti/form_pengajuan', [CutiController::class, 'form_pengajuan'])->name('form_pengajuan');
Route::get('cuti/table_pengajuan', [CutiController::class, 'table_pengajuan'])->name('table_pengajuan');
Route::get('cuti/nama', [CutiController::class, 'nama'])->name('nama');
Route::get('cuti/nip', [CutiController::class, 'nip'])->name('nip');
Route::get('cuti/jabatan', [CutiController::class, 'nama'])->name('jabatan');
Route::get('cuti/masakerja', [CutiController::class, 'masakerja'])->name('masakerja');
Route::get('cuti/unitkerja', [CutiController::class, 'unitkerja'])->name('unitkerja');
Route::get('cuti/jeniscuti', [CutiController::class, 'jeniscuti'])->name('jeniscuti');
Route::get('cuti/alasan', [CutiController::class, 'alasan'])->name('alasan');
Route::get('cuti/tanggalcuti', [CutiController::class, 'tanggalcuti'])->name('tanggalcuti');
Route::get('cuti/tanggal', [CutiController::class, 'tanggal'])->name('tanggal');
Route::get('cuti/alamat', [CutiController::class, 'alamat'])->name('alamat');
Route::post('cuti/save', [CutiController::class, 'pengajuan_save'])->name('pengajuan_save');
Route::get('cuti/view_pengajuan/{id}', [CutiController::class, 'view_pengajuan'])->name('view_pengajuan');
Route::get('edit_pengajuan/{id}', [CutiController::class, 'edit_pengajuan'])->name('edit_pengajuan');
Route::get('delete_pengajuan/{id}', [CutiController::class, 'delete_pengajuan'])->name('delete_pengajuan');
Route::post('pengajuan_update/{id}', [CutiController::class, 'pengajuan_update'])->name('pengajuan_update');
Route::get('cuti_kirim/{id}', [CutiController::class, 'cuti_kirim'])->name('cuti_kirim');
Route::get('cuti_setuju/{id}', [CutiController::class, 'cuti_setuju'])->name('cuti_setuju');
Route::get('cuti_tolak/{id}', [CutiController::class, 'cuti_tolak'])->name('cuti_tolak');

use App\Http\Controllers\SptController;

Route::get('spt/table_spt', [SptController::class, 'table_spt'])->name('table_spt');
Route::get('spt/detail_spt/{id}', [SptController::class, 'detail_spt'])->name('detail_spt');
Route::get('spt/form_spt', [SptController::class, 'form_spt'])->name('form_spt');
Route::get('spt/nama', [SptController::class, 'nama'])->name('nama');
Route::get('spt/nip', [SptController::class, 'nip'])->name('nip');
Route::get('spt/pangkat', [SptController::class, 'pangkat'])->name('pangkat');
Route::get('spt/golongan', [SptController::class, 'golongan'])->name('golongan');
Route::get('spt/jabatan', [SptController::class, 'jabatan'])->name('jabatan');
Route::get('spt/tanggal_surat', [SptController::class, 'tanggal_surat'])->name('tanggal_surat');
Route::get('spt/nomor_surat', [SptController::class, 'nomor_surat'])->name('nomor_surat');
Route::get('spt/tanggal_mulai', [SptController::class, 'tanggal_mulai'])->name('tanggal_mulai');
Route::get('spt/tanggal_selesai', [SptController::class, 'tanggal_selesai'])->name('tanggal_selesai');
Route::get('spt/lokasi_spt', [SptController::class, 'lokasi_spt'])->name('lokasi_spt');
Route::get('spt/uraian', [SptController::class, 'uraian'])->name('uraian');
Route::post('spt/save', [SptController::class, 'save_spt'])->name('save_spt');
Route::get('edit_spt/{id}', [SptController::class, 'edit_spt'])->name('edit_spt');
Route::get('delete_spt/{id}', [SptController::class, 'delete_spt'])->name('delete_spt');
Route::post('update_spt/{id}', [SptController::class, 'update_spt'])->name('update_spt');
Route::get('kirim_spt/{id}', [SptController::class, 'kirim_spt'])->name('kirim_spt');
Route::post('pengajuan_update/{id}', [CutiController::class, 'pengajuan_update'])->name('pengajuan_update');